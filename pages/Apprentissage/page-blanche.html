<blockquote>
	Il n'y a pas de moment idéal pour écrire. Il n'y a que l'instant présent.
	<cite>Barbara Kingsolver</cite>
</blockquote>
<nav>
	<ul>
		<li><a href="#redaction" title="Le contexte de la rédaction">Le contexte de la rédaction</a></li>
		<li><a href="#page-blanche" title="Qu'est-ce que le phénomène de la page blanche ?">Qu'est-ce que le phénomène de la page blanche ?</a></li>
		<li><a href="#freins" title="Les freins à la productivité">Les freins à la productivité</a></li>
		<li><a href="#strategies" title="Des stratégies pour rédiger de façon plus productive">Des stratégies pour rédiger de façon plus productive</a></li>
		<li><a href="#remedier" title="Ces conseils ne sont pas suffisants pour remédier à la situation ?">Ces conseils ne sont pas suffisants pour remédier à la situation ?</a></li>
		<li><a href="#conclusion" title="Conclusion">Conclusion</a></li>
		<li><a href="#references" title="Références">Références</a></li>
	</ul>
</nav>
<section id="redaction">
	<h1>Le contexte de la rédaction</h1>
	<p>
		La rédaction d'un essai, d'un mémoire ou d'une thèse représente souvent un projet d'écriture d'une envergure sans précédent. La tâche implique la capacité de gérer presque entièrement ses activités d'apprentissage, avec peu de pression sociale directe pour respecter les échéances. Enfin, l'atteinte de l'objectif peut être chargé d'une forte connotation émotionnelle (ex. : être le premier de la famille à terminer un doctorat). 
	</p>
	<p>
		L'étape de rédaction d'un mémoire ou d'une thèse peut donc s'avérer être une tâche intimidante. Vos limites et vos craintes les mieux enfouies refont parfois surface. Afin de bien compléter cette étape, il importe de bien comprendre ce qui peut venir entraver le processus d'écriture. Vous pouvez aussi consulter le texte <a href="{{LIEN}}obstacles-redaction" title="Obstacles à la rédaction aux cycles supérieurs en complément" target="_blank">Obstacles à la rédaction aux cycles supérieurs en complément</a>.
	</p>
</section>
<section id="page-blanche">
	<h1>Qu'est-ce que le phénomène de la page blanche ?</h1>
	<p>
		Le phénomène de la page blanche se produit lorsque l'étudiante ou l'étudiant a en main le contenu de son travail, mais qu'il n'arrive pas à mettre ses idées par écrit. Il entraîne une incapacité à organiser, planifier et initier la tâche de la rédaction, alors que les étapes préalables (recherche, lecture, connaissance du sujet, etc.) ont été réalisées. S'ensuit un blocage dans la rédaction. Cela peut aussi se produire lorsque l'étudiante ou l'étudiant a lu beaucoup sur un sujet, sans prendre de notes. Il peut alors entraîner un sentiment d'éparpillement et de la difficulté à savoir quoi sélectionner et par quoi commencer. 
	</p>
	<p>
		Le phénomène de la page blanche se manifeste par un inconfort, souvent composé de plusieurs états émotifs : anxiété, fatigue, impuissance, culpabilité, impression de piétinement et sentiment d'incompétence. Pour composer avec ces sentiments désagréables, l'étudiante ou l'étudiant aura alors tendance à se tourner vers d'autres tâches qu'il se sent plus à même de réaliser. Cependant, plus le temps passe, plus la tâche devient difficile à entreprendre. 
	</p>
</section>
<section id="freins">
	<h1>Les freins à la productivité</h1>
	<p>
		Le phénomène de la page blanche est rarement dû à une seule cause. De nombreux facteurs individuels peuvent l'occasionner et être interreliés. Bien qu'il puisse se produire chez des étudiantes ou des étudiants ayant déjà eu ce type de difficulté, il peut aussi se manifester pour la première fois lors des études supérieures. Avant de tenter quoi que ce soit, il importe d'abord de prendre le temps de vous arrêter et d'identifier quels facteurs sont susceptibles d'affecter la productivité. 
	</p>
	<h2>1) La peur de l'échec</h2>
	<p>
		<q>Je ne suis pas assez intelligent...</q><br>
		<q>Je n'ai pas les capacités, je suis nulle !</q><br>
		<q>Je n'y arriverai jamais, ce ne sera pas accepté...</q>
	</p>
	<p>
		La peur de l'échec peut déclencher une anxiété souvent diffuse, un sentiment d'incompétence ou d'infériorité. L'échec est souvent perçu de façon très globale : l'échec n'est pas uniquement attribuable à la tâche qui est réalisée, mais à la valeur de l'individu. La performance devient l'étalon de mesure de la valeur personnelle. Ce sont souvent les conséquences de l'échec qui sont anticipées (ex. : « je me m'en remettrais pas »). Une variante de la peur de l'échec est le « syndrome de l'imposteur », par exemple si vous doutez de votre potentiel pour les études supérieures et que vous craignez, en écrivant un texte, que votre directrice ou directeur vous démasque.
	</p>
	<h2>2) Le perfectionnisme</h2>
	<p>
		<q>Je dois produire quelque chose de supérieur à ce qui se fait habituellement.</q><br>
		<q>Je dois rédiger dix pages par jour et je ne prendrai pas plus qu'une journée de congé par semaine.</q><br>
		<q>Je dois écrire quelque chose de bon du premier coup !</q>
	</p>
	<p>
		Une personne perfectionniste se fixe des standards irréalistes et beaucoup trop élevés qui génèrent de l'anxiété (ex. : révolutionner le domaine). La pression pour atteindre ces standards peut l'amener à s'imposer des règles rigides ou à attendre d'être dans des dispositions idéales pour accomplir un travail parfait. En complément à ce texte, vous pouvez lire <a href="{{LIEN}}perfectionnisme" title="Le perfectionnisme : quand le mieux devient l'ennemi du bien" target="_blank">Le perfectionnisme : quand le mieux devient l'ennemi du bien</a>.
	</p>
	<h2>3) L'autocensure</h2>
	<p>
		<q>Mon directeur ne trouvera pas ça bon...</q><br>
		<q>Je n'ai pas assez progressé, ça n'avance pas !</q><br>
		<q>Ce que j'ai écrit n'est pas assez bon, mieux vaut recommencer...</q>
	</p>
	<p>
		L'autocensure prend souvent racine dans des expériences passées négatives (ex. : mauvais résultats). Elle peut être une forme d'intériorisation des critiques, provenant généralement des figures d'autorité (ex. : parents), ce qui vous amène à censurer et à rejeter d'emblée les idées qui vous viennent en tête, sans leur laisser une chance.
	</p>
	<h2>4) La procrastination</h2>
	<p>
		<q>Ça va être plate et long !</q><br>
		<q>e vais faire telle chose avant de commencer...</q><br>
		<q>Il me reste encore du temps...</q>
	</p>
	<p>
		La procrastination est la difficulté à se mettre à la tâche, à se discipliner. Elle se manifeste par un besoin irrésistible de fuir le travail à accomplir. Elle peut découler des attitudes décrites précédemment, mais peut aussi être reliée à d'autres facteurs. Elle implique, entre autres, une difficulté de gestion du temps et d'organisation. La fuite procure un soulagement temporaire : avec le retard qui s'accumule, la pression pour rattraper le temps perdu s'amplifie. Vous pouvez aussi consulter à ce sujet le texte <a href="{{LIEN}}procrastination" title="La procrastination" target="_blank">La procrastination</a>. 
	</p>
	<h2>5) Les mythes reliés à l'écriture</h2>
	<p>
		<q>Bien écrire est un talent inné.</q><br>
		<q>L'écriture n'est pas vraiment importante en soi...</q><br>
		<q>Ça prend un vocabulaire recherché pour produire un bon texte.</q><br>
		<q>Un texte long est meilleur qu'un court.</q>
	</p>
	<p>
		L'écriture est une habileté qui s'apprend et se développe par la pratique. La communication des résultats d'un travail scientifique est une compétence importante à développer aux études supérieures. Posséder un vocabulaire recherché peut être un atout mais l'utilisation d'un vocabulaire simple et précis permet aussi de véhiculer clairement ses idées. Enfin, même si le nombre de pages peut à première vue laisser l'impression que le travail a nécessité plus d'effort et d'investissement, un texte court et concis a souvent plus d'impact qu'un texte long, sinueux et redondant. 
	</p>
</section>
<section id="strategies">
	<h1>Des stratégies pour rédiger de façon plus productive</h1>
	<h2>1) Se fixer des objectifs spécifiques et atteignables</h2>
	<p>
		Si vous vous sentez bloqué, bloquée, la première chose à faire est de diminuer vos objectifs d'écriture. Pourtant, avec le temps qui passe, le sentiment de devoir « vous rattraper » vous poussera probablement à vouloir faire l'inverse, mais cela a généralement pour effet d'augmenter la pression et de perpétuer le blocage. Fixez-vous au départ des objectifs de rédaction très petits et faciles, une partie bien spécifique à la fois : faites attention de ne pas trop vous en demander, vous devez vous désensibiliser graduellement. 
	</p>
	<p>
		Vous ne pouvez vous attendre à rédiger une bonne partie de votre mémoire ou de votre thèse en quelques jours, comme cela pouvait être le cas pour les travaux écrits faits dans le cadre de vos cours (ex. : écrire intensivement la majeure partie du travail pendant une fin de semaine). La tâche est de trop grande envergure pour être approchée de cette façon. 
	</p>
	<p>
		Privilégiez des périodes d'une durée de deux ou trois heures pour la rédaction, et complétez votre journée avec d'autres tâches. Cette approche est souvent plus productive à long terme. Le texte Les défis de la gestion du temps aux cycles supérieurs pourrait vous être utile. 
	</p>
	<h2>2) L'écriture libre et la méthode par brouillons successifs</h2>
	<p>
		Cette méthode consiste à écrire ce qui vous vient à l'esprit sans vous arrêter et sans vous préoccuper de la cohérence des idées ou de la syntaxe. Même si les idées générées semblent confuses, elles deviendront de plus en plus claires en écrivant. La tendance à relire et réécrire sans cesse les mêmes paragraphes est antiproductive.
	</p>
	<p>
		Lorsque vous débutez l'écriture, ayez en tête que vous rédigez d'abord pour vous-même, afin de résumer ce que vous savez et les informations obtenues suite à vos lectures et vos recherches. En écrivant votre première version, vous verrez à ce que vos idées soient mieux consolidées et bien présentées, et à ce que votre lecteur puisse bien vous comprendre. Écrivez plus que moins, sans critiquer votre travail à ce stade-ci, jusqu'à ce que vous ayez atteint votre objectif d'écriture pour cette journée.
	</p>
	<p>
		Voici à titre d'exemple les étapes qui pourraient être suivies en utilisant cette méthode : 
	</p>
	<ol>
		<li>
			Écrivez librement : générez les idées principales, écrivez tout ce qui vous vient à l'esprit pour une section donnée 
		</li>
		<li>
			Triez et organisez les points selon une structure 
		</li>
		<li>
			Formulez les idées en phrases complètes et en paragraphes, améliorez les transitions (rédaction de votre première version) 
		</li>
		<li>
			Relisez et corrigez votre première version 
		</li>
		<li>
			Révisez et améliorez vos versions subséquentes (jusqu'à votre version finale) 
		</li>
	</ol>
	<h2>3) Établir un horaire de travail structuré</h2>
	<p>
		Il importe de vous créer des habitudes d'écriture constantes (ex. : quatre demi-journées par semaine). Visez la régularité dans la rédaction, peu importe que vous sentiez l'inspiration ou non. La continuité dans la tâche d'écriture est importante puisqu'elle permet de conserver vos idées actives dans votre esprit et de diminuer le degré d'anticipation et d'anxiété face à la tâche. Cela vous évite d'osciller entre des périodes de surproductivité (pouvant conduire à l'épuisement) et à l'inverse, des périodes d'aversion pour l'écriture. Écrire régulièrement et lentement favorise la construction des idées : écrire produit des idées, qui favorisent l'écriture, qui entraîne l'émergence d'autres idées... La rédaction ne doit pas occuper toute la place, mais votre travail ne doit idéalement pas non plus être laissé de côté pendant de longues semaines.
	</p>
	<p>
		Commencez votre journée par la période d'écriture. Plutôt que d'attendre un état d'inspiration pour écrire, tentez de vous placer dans des conditions favorables (ex. : endroit adéquat, où le risque d'interruption est limité). Préparez votre esprit au travail intellectuel, en vous laissant une période de réchauffement (ex. : relire la partie rédigée la veille). 
	</p>
	<p>
		En outre, l'écriture est une tâche où le rejet et la critique sont courants. Elle offre peu de récompenses immédiates. Assurez-vous de vous offrir des récompenses pour ce qui a été accompli plutôt que des punitions pour les objectifs non atteints. Revoyez les objectifs fixés si ceux-ci ne sont que rarement atteints. Les loisirs ou activités qui vous font plaisir sont une bonne source de récompense, si vous vous les accordez après un effort (et non avant !). 
	</p>
	<h2>4) Rechercher les exceptions</h2>
	<p>
		Observez-vous et posez-vous ces questions : quels sont les moments où le blocage ne se manifeste pas ? Où êtes-vous naturellement plus productive, productif ? Identifiez ce qui fonctionne pour vous, comment vous avez procédé lors de ces moments. Tentez de reproduire les conditions dans lesquelles vous avez le sentiment d'avoir avancé et ressenti de la satisfaction face à votre travail.
	</p>
	<h2>5) Essayer quelque chose de différent</h2>
	<p>
		Si les solutions tentées jusqu'à présent s'avèrent peu efficaces, essayez quelque chose de différent de ce que vous avez l'habitude de faire. Changez d'environnement (ex. : travailler dans un bureau plutôt qu'à la maison). Écrivez à la main si vous avez l'habitude d'écrire à l'ordinateur, enregistrez-vos idées en parlant sur un dictaphone. Changez l'ordre dans lequel vous rédigez vos sections (ex. : commencer par une section plus facile, ne pas s'acharner à rédiger l'introduction en premier). 
	</p>
	<h2>6) Porter attention à votre discours interne</h2>
	<p>
		Les pensées qui vous habitent avant, pendant et après vos périodes de rédaction ont un impact sur votre motivation et votre satisfaction. Essayez d'entretenir un discours encourageant face à vos efforts, parlez-vous comme le ferait un entraîneur sportif, qui serait derrière vous pour vous soutenir dans l'atteinte de votre objectif. Pour une meilleure compréhension du lien entre votre mode de pensée et vos états émotifs, lisez le texte <a href="{{LIEN}}perceptions" title="Stress, anxiété et perceptions : maîtriser les pensées toxiques" target="_blank">Stress, anxiété et perceptions : maîtriser les pensées toxiques</a>. 
	</p>
</section>
<section id="remedier">
	<h1>Ces conseils ne sont pas suffisants pour remédier à la situation ?</h1>
	<p>
		Des facteurs de motivation sont peut-être en cause dans votre difficulté à progresser. Votre projet d'essai, de mémoire ou de thèse était-il vraiment votre choix ? Sur quelle base avez-vous pris votre décision d'entreprendre des études supérieures ? Entretenez-vous des doutes ou de l'ambivalence à l'égard de ce choix ? Quels sont les enjeux qui y sont rattachés ? Avez-vous des craintes quant à l'idée de terminer votre projet (ex. : peur de « l'après », du marché du travail, peur d'avoir à maintenir un haut niveau de performance). 
	</p>
	<p>
		Vous arrêter et prendre un peu de recul afin d'explorer les motifs de vos choix, vos questionnements et la résistance ressentie à l'égard de la tâche pourrait vous permettre de mieux comprendre ce que vous vivez.
	</p>
	<p>
		Il est également possible que vous viviez des difficultés personnelles qui accaparent votre esprit et affectent votre fonctionnement. N'hésitez pas à demander une aide professionnelle pour clarifier votre situation.
	</p>
</section>
<section id="conclusion">
	<h1>Conclusion</h1>
	<p>
		Ce texte visait à vous aider à mieux comprendre le phénomène de la page blanche. Bien qu'il puisse avoir plusieurs sources, dans tous les cas, ce blocage vous permet d'éviter de vous confronter à l'échec, à la critique et... au succès. Le plus difficile est de commencer. Les sentiments négatifs s'estomperont à mesure que vous commencerez à rédiger, à constater vos progrès, et les sentiments positifs pourront alors émerger. En fait, le sentiment de vous rapprocher de votre objectif est probablement la meilleure manière de surmonter vos peurs. 
	</p>
	<p>
		Lien vers le site de la <a href="http://www.fesp.ulaval.ca" title="Faculté des études supérieures">Faculté des études supérieures</a>.
	</p>
</section>
<section id="references">
	<h1>Références</h1>
	<p><cite>
		Apps, J. W. (1982), Improving your writing skills: a learning plan for adults, Chicago, Follett Pub. Co., 239 p. 
	</cite></p>
	<p><cite>
		Bégin, C. (1999), « Mieux vivre sa période de rédaction », Étudier à l'UQAM, vol. 6, no 2 (février). 
	</cite></p>
	<p><cite>
		Boice, R. (1993), « Writing Blocks and Tacit Knowledge », Journal of Higher Education, vol. 64, no 1, p. 19-54. 
	</cite></p>
	<p><cite>
		Bolker, J. (1998), Writing your dissertation in fifteen minutes a day, a guide to starting, revising and finishing your doctoral thesis, New York: H. Holt, Owl Books, 184 p. 
	</cite></p>
	<p><cite>
		Cambra, A. E., N. S. Schluntz et S. A. Cardoza (1984), Graduate students' survival guide, Jefferson, N. C.: McFarland &amp; Co., 210 p. 
	</cite></p>
	<p><cite>
		Cohen, M.-R. (1999), « The pause that represses: The experience of working through writer's block during the dissertation writing process », Dissertation Abstracts International Section A: Humanities and Social Sciences, vol. 60 (5-A, December), p. 1448.
	</cite></p>
	<p><cite>
		Phillips, E. M., et D. S. Pugh (1987), How to get a Ph. D., a handbook for students and their supervisors, Milton Keynes (England) - Philadelphia (USA), Open University Press, 161 p. 
	</cite></p>
	<p><cite>
		Talbot, F. (2003), « Les défis du contexte de rédaction », Vies-à-vies. Bulletin du Service d'orientation et de consultation psychologique, Université de Montréal, vol. 15, no 3 (janvier). 
	</cite></p>
</section>
<footer>
	<h2>Rédiger par :</h2>
	<p>
		Véronique Mimeault, psychologue
	</p>
</footer>
