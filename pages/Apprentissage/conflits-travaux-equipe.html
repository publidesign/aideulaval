<blockquote>
	Recherchez d’abord à comprendre, puis à être compris.
	<cite>S. Covey.</cite>
</blockquote>
<p>
	Travailler en équipe offre de multiples occasions d’apprentissage et exige des habiletés spécifiques, telles qu’exprimer ses idées, négocier, déléguer et interagir avec divers types de personnalité. L’expérience peut s’avérer stimulante et enrichissante. Cependant, le travail en équipe est souvent une source de tensions et de désaccords. Le climat au sein d’une équipe dépend entre autres de la façon dont les membres s’y prennent pour exprimer leurs points de vue à l’égard des sujets abordés. Les difficultés vécues dans le cadre d’un travail en équipe sont souvent de nature relationnelle. Toutefois, il est possible d’adopter une attitude constructive pour prévenir l’émergence de conflits ou en favoriser la résolution.
</p>
<nav>
	<ul>
		<li><a href="#conflit" title="Qu’est-ce qu’un conflit?">Qu’est-ce qu’un conflit?</a></li>
		<li><a href="#types" title="Les types de conflit">Les types de conflit</a></li>
		<li><a href="#reactions" title="Les réactions par rapport aux conflits">Les réactions par rapport aux conflits</a></li>
		<li><a href="#situation" title="Que faire en situation de conflit?">Que faire en situation de conflit?</a></li>
		<li><a href="#collaboration" title="Comment favoriser une bonne collaboration et prévenir les conflits?">Comment favoriser une bonne collaboration et prévenir les conflits?</a></li>
		<li><a href="#degenere" title="Quand ça dégénère...">Quand ça dégénère...</a></li>
		<li><a href="#references" title="Références">Références</a></li>
	</ul>
</nav>
<section id="conflit">
	<h1>Qu’est-ce qu’un conflit?</h1>
	<p>
		Un conflit est causé par un désaccord entre des personnes. Il est souvent issu d’un affrontement d’opinions, d’intérêts ou de valeurs. Il se développe à la suite de la perception négative d’un comportement, d’une parole ou d’une intention de l’autre. Parfois, il est en réaction à un comportement ou à une attitude inappropriée. Le conflit peut se manifester par des tensions dans les relations, dégénérer en altercations verbales ou même, dans les cas plus graves, physiques. Lorsqu’un conflit éclate, il peut faire perdre beaucoup de temps et d’énergie. La progression et la qualité du travail risquent également d’être compromises, de même que les relations entre les coéquipiers. Les conflits sont parfois inévitables, mais ils peuvent offrir la possibilité de mieux se connaître et de faire évoluer ses compétences pour gérer des situations délicates. En effet, ces habiletés pourront être mises à profit au-delà du contexte scolaire, en intégrant le marché du travail ou lors des interactions avec d’autres professionnels.
	</p>
</section>
<section id="types">
	<h1>Les types de conflit</h1>
	<ul>
		<li>
			<strong>Conflits reliés au fonctionnement de l’équipe</strong> : retards ou absences aux rencontres, investissement ou efforts insuffisants, rôles mal définis, difficulté à planifier l’horaire des rencontres, lieu de rencontre inadéquat.
		</li>
		<li>
			<strong>Conflits concernant la réalisation du travail</strong> : points de vue divergents, mésententes sur les objectifs et le contenu, répartition inéquitable des tâches, piétinement.
		</li>
		<li>
			<strong>Conflits en raison de traits de personnalité incompatibles</strong> : réactivité et susceptibilité par rapport aux critiques, passivité, démotivation, perfectionnisme et contrôle.
		</li>
		<li>
			<strong>Conflits de générations</strong> : divergences en raison des écarts d’âge, de façons différentes de réaliser le travail ou décalage en raison de la diversité des expériences antérieures (scolaires ou professionnelles).
		</li>
		<li>
			<strong>Conflits culturels</strong> : confrontations sur le plan des valeurs, des croyances religieuses ou des opinions politiques, mise à l’écart, préjugés, attitude discriminatoire.
		</li>
	</ul>
</section>
<section id="reactions">
	<h1>Les réactions par rapport aux conflits</h1>
	<p>
		La réalisation d’un travail en équipe nécessite la participation de tous. Il devient difficile de poursuivre le travail si un conflit éclate et perdure. Divers rôles peuvent être joués à différents moments par les membres d’une équipe. Si vous êtes confronté à des difficultés au sein de votre équipe, prenez conscience de vos réactions et observez aussi la manière dont les autres membres de l’équipe interagissent. Cela vous permettra de mieux cerner la dynamique de l’équipe et d’assumer votre responsabilité dans l’établissement d’un climat favorable. Voici les réactions les plus courantes par rapport aux conflits :
	</p>
	<ul>
		<li>
			<strong>Évitement</strong> : Certaines personnes se retirent systématiquement en situation de conflit; elles minimisent ou nient le problème. C’est souvent dans le but de ne pas nuire au groupe que certains évitent d’exprimer leurs réserves, leurs émotions ou leurs désaccords. Bien qu’il soit plus facile à court terme d’éviter les sujets litigieux, les insatisfactions non résolues sont susceptibles de compromettre la bonne entente. Il en résulte la plupart du temps un cumul de frustrations qui incite certains coéquipiers à moins s’investir, ce qui peut nuire au bon déroulement du travail ou provoquer un conflit. Même s’il peut arriver que certains conflits se résolvent d’eux-mêmes avec le temps, fuir toute forme de confrontation contribue au maintien du problème et freine l’acquisition d’habiletés pour le résoudre.
		</li>
		<li>
			<strong>Conciliation</strong> : Certaines personnes sont d’un naturel conciliant et cèdent devant l’insistance de l’autre afin de ne pas provoquer un conflit. Souvent, ces personnes craignent de déplaire ou de blesser en s’affirmant. Leur mécontentement peut alors s’exprimer de manière détournée, sous forme de bouderie ou de commérages. Céder implique en quelque sorte de sacrifier son opinion. Le fait de demeurer vigilant avant de faire le choix de tout concéder à l’autre en disant « comme tu voudras » peut aider à ne pas accumuler les frustrations ou à se sentir amer.
		</li>
		<li>
			<strong>Rivalité</strong> : D’autres cherchent à tout prix à faire valoir leur point de vue et à avoir raison. Ils sont prêts à tout pour l’emporter sur l’autre, y compris à utiliser le sarcasme, la dévalorisation, l’agressivité et l’humiliation. Cette attitude a des effets négatifs sur le climat de travail au sein de l’équipe et contribue à envenimer le conflit.
		</li>
		<li>
			<strong>Collaboration</strong> : La collaboration permet de trouver une solution satisfaisante pour les parties. Chacun travaille dans un esprit d’équipe plutôt qu’en fonction de ses intérêts personnels. La collaboration demande une bonne dose de confiance envers les autres, de l’ouverture ainsi que de bonnes habiletés d’affirmation de soi et de résolution de problèmes. Elle exige du temps et de la délicatesse, mais préserve la qualité des relations.
		</li>
		<li>
			<strong>Compromis</strong> : Afin d’arriver à un compromis, chacune des parties concède quelque chose, mais gagne sur un autre plan, ce qui permet de trouver une solution acceptable, même si elle n’est pas totalement satisfaisante. Les personnes concernées doivent faire preuve de flexibilité. Tout comme la collaboration, cette attitude permet l’implication de tous et stimule la motivation, la productivité et la satisfaction.
		</li>
	</ul>
</section>
<section id="situation">
	<h1>Que faire en situation de conflit?</h1>
	<p>
		Retarder la résolution d’un conflit complique habituellement la situation et risque de compromettre la réussite du travail. Il importe ainsi de réagir le plus rapidement possible, dès la prise de conscience des premiers signes de tension au sein de l’équipe. Le type de réaction adoptée par les coéquipiers (éviter, laisser planer des non-dits) détermine souvent si ces tensions entraîneront une escalade du conflit ou sa résolution. Passé un certain seuil, il deviendra plus difficile de régler les difficultés de façon efficace et harmonieuse. Les stratégies de résolution de conflits peuvent varier selon la situation, il n’existe pas de recette miracle. Cependant, les éléments suivants constituent des pistes d’action.
	</p>
	<h2>&#9632; Précisez et cherchez à comprendre la source du désaccord</h2>
	<p>
		Lorsqu’une situation vous contrarie, essayez d’identifier dès que possible ce que vous éprouvez (irritation, frustration, impatience, déception, impression de ridicule, stress, inquiétude, culpabilité, honte). Les émotions ressenties sont des indices que quelque chose vous dérange ou ne vous convient pas. Soyez à leur écoute et précisez ce qui pose problème pour vous dans la situation. Décrivez la situation en termes de comportements observables sur lesquels vous pouvez agir, plutôt que d’étiqueter négativement les personnes impliquées.
	</p>
	<blockquote>
		Marie travaille en équipe avec Simon et Jacinthe. Ils se sont réparti les tâches. Marie s’investit consciencieusement pour réaliser sa partie du travail. Elle remarque que Simon n’est pas très ouvert à écouter son point de vue et dénigre ses idées, mais elle a d’abord préféré ne pas lui en parler, de crainte qu’il réagisse mal. Au moment de mettre en commun leurs parties respectives, Simon adopte une attitude critique à l’égard du contenu élaboré par ses coéquipières et modifie sans leur autorisation les idées qui ne lui conviennent pas. Marie se sent frustrée et a l’impression de ne pas être respectée. Elle aurait souhaité que Simon discute au préalable avec elle des modifications à apporter plutôt que de décider sans elle. Pour sa part, Jacinthe réagit de manière plutôt détachée.
	</blockquote>
	<h2>&#9632; Exprimez votre point de vue et vos sentiments</h2>
	<p>
		Après avoir pris le recul nécessaire pour clarifier la source de votre malaise, choisissez l’endroit et le moment opportun pour aborder la situation conflictuelle avec la personne concernée. Chacun a avantage à communiquer ses insatisfactions et ses réticences plutôt que de les garder pour lui. Exprimez le plus directement et brièvement possible ce qui vous a dérangé et comment vous vous êtes senti, en vous appuyant sur les faits. Dites ce que vous ressentez en privilégiant l’emploi du « je », en évitant de critiquer, de juger ou d’attaquer. Évaluez s’il est pertinent de le faire devant les autres membres du groupe ou non, mais il est souvent plus approprié de s’adresser à la personne concernée seulement.
	</p>
	<p>
		Ensuite, laissez l’autre personne réagir à ce que vous lui communiquez et soyez à l’écoute sans l’interrompre. Faites preuve d’ouverture en essayant de comprendre la situation selon sa perspective. Laissez l’autre personne exprimer ses émotions. Il est possible que vous trouviez difficile de bien écouter sans être sur la défensive, surtout si vous vous sentez attaqué personnellement. Malgré cela, maintenez un contact visuel et faites un effort sincère pour vous intéresser à ce qu’elle essaie de vous dire. Laissez-la terminer ses phrases et clarifiez ses propos pour vous assurer que vous les avez bien compris. Afin de mieux saisir ce qu’elle ressent, portez aussi attention aux indices non verbaux tels que le ton de sa voix, son regard, ses gestes, ses expressions faciales et sa posture.
	</p>
	<p>
		Le conflit est souvent le résultat d’une accumulation d’irritants, de désaccords ou d’insatisfactions. Faites attention à ne pas tomber dans le piège de l’interprétation (généraliser, sauter aux conclusions, juger catégoriquement), mais visez plutôt à clarifier les intentions de l’autre. Restez calme et à l’écoute de vos émotions afin de ne pas vous laisser emporter. Un climat « explosif » risque de vous mener vers des solutions improvisées plutôt qu’à une résolution durable.
	</p>
	<p>
		Oser dire ce qui dérange nécessite de bonnes habiletés d’affirmation de soi. En situation de conflit, les conditions sont rarement idéales (pression, stress) pour exercer ces habiletés. Beaucoup de personnes sont maladroites dans ces situations, souvent parce qu’à force d’éviter ou de laisser la situation s’envenimer, elles n’ont pas saisi la possibilité de développer ce savoir-faire. Plusieurs peuvent ainsi être tentés de manifester leur insatisfaction par courriel ou sur les réseaux sociaux. Les nouvelles technologies offrent l’avantage de favoriser des communications rapides et efficaces. Cependant, les modes de communication virtuelle ne devraient jamais être utilisés pour résoudre des conflits. En effet, le mode virtuel ouvre la porte à des excès dans l’expression des opinions notamment parce que la personne n’est pas présente. Le risque de dérapage est d’autant plus grand si certains commentaires sont lus par un ensemble d’internautes qui ne sont pas concernés par la situation. Pour poursuivre la réflexion sur les conséquences possibles de ce type de communication, consultez le texte suivant : Quand les réseaux sociaux font mal. 
	</p>
	<blockquote>
		Marie a décidé d’exprimer ses sentiments à Simon par rapport à cette situation. Jacinthe n’a pas senti le besoin de s’en mêler. Marie a fait part de son impression à Simon : « lorsque tu modifies ainsi ma partie du travail sans m’en parler, il me semble que tu ne me fais pas confiance et que tu ne reconnais pas mes compétences. Cela me frustre et me rend mal à l’aise avec toi ». Simon s’est défendu en répondant qu’il a simplement voulu gagner du temps, car il a beaucoup d’étude et qu’il est stressé quant à ses résultats scolaires. Marie conçoit que Simon ne souhaitait pas lui manquer de respect, mais il voulait s’assurer d’obtenir un bon résultat dans le travail.
	</blockquote>
	<h2>&#9632; Explorez une zone de compromis</h2>
	<p>
		En tant que membre de l’équipe, vous devez vous sentir responsable de l’ensemble du travail. Vos efforts ne fonctionneront pas si l’une des personnes est engagée dans une confrontation. Le défi consiste à garder votre calme et à prendre conscience de votre rôle dans la dynamique de l’équipe. Ne vous acharnez pas à convaincre l’autre, mais adoptez plutôt une approche de résolution de problèmes. Idéalement, recherchez une solution ensemble, en explorant les possibilités. Soyez créatif et suggérez le maximum d’idées. N’insistez pas trop sur vos divergences, mais plutôt sur vos objectifs et intérêts communs. Admettez toutefois qu’un travail en équipe est nécessairement différent que si vous l’aviez fait seul.
	</p>
	<blockquote>
		Marie se sent soulagée d’avoir exprimé sa frustration à Simon. Elle comprend mieux ce qui l’a poussé à procéder ainsi. Cependant, à l’avenir, Marie propose à Simon de planifier une rencontre pour discuter ensemble des modifications à apporter au travail avant qu’il ne procède aux corrections. Jacinthe approuve. Simon est réticent, car il est peu disponible avant la date de remise pour modifier à nouveau le travail. Il semble toutefois avoir compris qu’il devait prendre en considération l’effort fourni par les autres membres de l’équipe plutôt que de s’approprier le travail. Pour cette fois, Marie fait un compromis, mais se félicite de lui en avoir parlé.
	</blockquote>
	<p>
		Il peut arriver que les solutions envisagées ne permettent pas d’améliorer la situation. De même, certaines personnes ne veulent pas changer ou se montrent résistantes au changement. Parfois, il existe trop de divergences entre les membres d’une équipe ou une personne manque de motivation et d’engagement à l’égard du travail, ce qui empêche de parvenir à un compromis. Il faut peut-être envisager de quitter l’équipe ou de faire appel au professeur pour considérer une correction équitable. Dans le cas où des motifs sérieux (absence systématique aux rencontres, non-respect des délais établis, plagiat) amènent à exclure une personne du groupe de travail, il est préférable de l’en aviser clairement dans les meilleurs délais. L’exclusion de la personne peut être la seule solution possible, mais il faut bien mesurer les conséquences de ce geste et avoir tenté d’autres solutions auparavant (avertissement, clarification des attentes, etc.).
	</p>
</section>
<section id="collaboration">
	<h1>Comment favoriser une bonne collaboration et prévenir les conflits?</h1>
	<p>
		Plusieurs comportements et attitudes peuvent contribuer à l’installation d’un climat de travail agréable et de solidarité plutôt que de compétition et ainsi aider à prévenir les conflits.
	</p>
	<h2>Minimisez les problèmes de logistique</h2>
	<ul>
		<li>
			Prenez de l’avance. Réunissez-vous le plus tôt possible pour discuter du sujet et des objectifs du travail. Réservez déjà à l’agenda de tous des périodes pour les rencontres;
		</li>
		<li>
			Trouvez un lieu de réunion adéquat et propice au travail; 
		</li>
		<li>
			Établissez des règles de fonctionnement claires (ordre du jour et compte-rendu pour les réunions, animation des réunions, façons de faire circuler l’information, rôles et responsabilités de chacun, répartition des tâches, ententes en cas d’absence ou de changement à l’horaire); 
		</li>
		<li>
			 Faites un bilan périodique de la progression du travail et de la satisfaction des participants.
		</li>
	</ul>
	<h2>Reconnaissez et respectez les différences</h2>
	<ul>
		<li>
			Consacrez du temps pour que les coéquipiers puissent s’apprivoiser mutuellement ; cela favorisera une meilleure utilisation des compétences de chacun dans la répartition des tâches et créera une cohésion dans le groupe. 
		</li>
		<li>
			Réajustez la répartition des tâches et de la charge de travail au besoin (ajoutez, retranchez ou échangez une tâche). Si une personne ne respecte pas ses engagements, ce n’est pas toujours une question de mauvaise volonté; peut-être que la tâche lui semble trop lourde, mal définie ou en dehors de ses capacités, ce qui l’empêche de bien la réaliser. 
		</li>
		<li>
			Développez vos habiletés d’affirmation. Apprendre à dire non, à mettre ses limites et à être à l’écoute de ses insatisfactions avant qu’elles ne s’amplifient peut aider à prévenir les conflits. Expliquez ce que vous voulez et ce dont vous avez besoin et assurez-vous que votre message a bien été reçu et compris. Demandez des précisions : posez des questions et ne vous contentez pas d’ententes vagues.
		</li>
		<li>
			Favorisez l’échange d’information. Adoptez une attitude de confiance et partagez librement l’information. 
		</li>
		<li>
			Attaquez-vous aux problèmes et non aux individus. Déterminez les comportements qui vous agacent plutôt que de vous attarder à vos préjugés vis-à-vis de cette personne. Si vous êtes à l’aise, privilégiez une touche d’humour. 
		</li>
		<li>
			Souvenez-vous qu’une opinion ou une autre approche que la vôtre n’est pas mauvaise en soi. Elle est le reflet d’une perspective ou d’un bagage d’expériences distinct, qui peuvent être enrichissants tant pour les membres de l’équipe que pour le travail. Au besoin, n’hésitez pas à mentionner tout élément pertinent qui vous permettrait d’être mieux compris; cela favorise une meilleure acceptation de la part des autres. Laissez-vous également le droit d’être en désaccord sans animosité.
		</li>
		<li>
			Soyez flexible. Lorsque la majorité l’emporte, acceptez les décisions du groupe.
		</li>
		<li>
			Respectez la diversité individuelle. Même s’il peut être confrontant de travailler avec des personnes qui adoptent des valeurs opposées aux vôtres, faites preuve d’ouverture et de curiosité. Vous pourrez en apprendre énormément sur les autres et sur vous. N’essayez pas de rallier ou de convaincre une personne d’épouser d’autres valeurs. En effet, les valeurs d’un individu sont souvent bien ancrées et peu modifiables. Prévenez ce type de conflit plutôt que de vous engager dans des débats émotifs qui ne mènent nulle part (ex. : opinions politiques ou religieuses). Respectez ces différences (culturelles, générationnelles), quitte à éviter certains sujets, afin de demeurer centré sur la poursuite de la tâche et la recherche de solutions. Considérez les gens pour ce qu’ils sont plutôt que de les associer aux caractéristiques de leur culture, de leur race ou de leur âge. 
		</li>
		<li>
			Limitez les discussions si le climat devient trop émotif ou si vous vous sentez mal préparé. Vous pouvez ainsi éviter d’aggraver le conflit à court terme. Laissez-vous un peu de temps et attendez que les émotions (colère, frustration, soupçons) se calment de part et d’autre. Armez-vous de patience et démontrez votre bonne volonté.
		</li>
	</ul>
</section>
<section id="degenere">
	<h1>Quand ça dégénère...</h1>
	<p>
		Si les difficultés avec vos coéquipiers sont associées à des comportements inappropriés (isolement, humiliation, discrédit, harcèlement sexuel, menaces), réagissez sans tarder. Manifestez votre désapprobation directement à la personne en cause. S’il n’y a pas de changement, cherchez de l’aide auprès des ressources appropriées (<a href="http://www2.ulaval.ca/services-de-a-a-z/harcelement/accueil.html" title="Centre de prévention et d’intervention en matière de harcèlement" target="_blank">Centre de prévention et d’intervention en matière de harcèlement</a>).
	</p>
</section>
<section id="references">
	<h1>Références</h1>
	<p><cite>
		Apprendre en collaboration avec d’autres… le travail en équipe. Faculté des Sciences de l’éducation, Université Laval. (1996).
	</cite></p>
	<p><cite>
		CONSEIL DES RESSOURCES HUMAINES DU SECTEUR CULTUREL (CRHSC). Gestion des ressources humaines : gérer les problèmes et les conflits.
	</cite></p>
	<p><cite>
		La gestion des conflits. Atelier donné par Pascale Poudrette dans le cadre de la CACPUQ. (2003).
	</cite></p>
	<p><cite>
		Motoi, I. et Villeneuve, L. Guide de résolution de conflits dans le travail en équipe. Université du Québec en Abitibi-Témiscamingue. Presses de l’Université du Québec. (2010).
	</cite></p>
	<p><cite>
		Scholtes, P., Joiner, B., Streibel, B., et Lalanne, J. Réussir en équipe. Éditeur Actualisation. (2002).
	</cite></p>
	<p><cite>
		TACT, Téléapprentissage Communautaire et Transformatif. Le travail en équipe. URL: <a href="http://www.tact.fse.ulaval.ca/fr/html/sites/guide2.html" title="http://www.tact.fse.ulaval.ca/fr/html/sites/guide2.html" target="_blank">http://www.tact.fse.ulaval.ca/fr/html/sites/guide2.html</a> (Consulté le 4 février 2013).
	</cite></p>
</section>
<footer>
	<h2>Rédiger par :</h2>
	<p>
		Véronique Mimeault, psychologue<br>
		Caroline Sylvain, psychologue
	</p>
</footer>
