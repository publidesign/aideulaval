<blockquote>
	La question n'est pas tant de bien faire les choses que de faire les bonnes choses
	<cite>S. R. Covey</cite>
</blockquote>
<nav>
	<ul>
		<li><a href="#introduction" title="Introduction">Introduction</a></li>
		<li><a href="#pieges" title="Les principaux pièges">Les principaux pièges</a></li>
		<li><a href="#priorites" title="Priorité aux priorités !">Priorité aux priorités !</a></li>
		<li><a href="#long-terme" title="Planification à long terme : l'échéancier à rebours">Planification à long terme : l'échéancier à rebours</a></li>
		<li><a href="#court-terme" title="Planification à court terme">Planification à court terme</a></li>
		<li><a href="#organiser-temps" title="Du temps pour organiser votre temps">Du temps pour organiser votre temps</a></li>
		<li><a href="#conclusion" title="Conclusion">Conclusion</a></li>
		<li><a href="#references" title="Références">Références</a></li>
	</ul>
</nav>
<section id="introduction">
	<h1>Introduction</h1>
	<p>
		Au moment de la rédaction de l'essai, du mémoire ou de la thèse, le rapport au temps est différent. Les repères du premier cycle (ex. : plans de cours, échéances des examens de fin de session et de remise des travaux) ne sont plus présents. En l'absence de l'encadrement habituel, l'étudiante ou l'étudiant devient ainsi le principal responsable de son emploi du temps, et l'artisan de son horaire, ce qui peut affecter ses habitudes de travail, mais aussi l'organisation de sa vie entière. Le défi est ainsi d'être efficace dans son travail, sans délaisser les autres sphères de sa vie. 
	</p>
	<p>
		La gestion du temps est donc un aspect indissociable de la réussite aux études supérieures. Les recherches démontrent que plus la durée des études augmente, plus la motivation diminue, ce qui est associé à un abandon définitif du projet dans certains cas. En complément à ce texte et pour une révision plus générale des principes de base de la gestion du temps, vous pouvez consulter le texte <a href="{{LIEN}}gestion-temps" title="La gestion du temps" target="_blank">La gestion du temps</a>.
	</p>
</section>
<section id="pieges">
	<h1>Les principaux pièges</h1>
	<p>
		Au départ, l'autonomie que procure le fait d'être « son propre patron » peut être stimulante, mais certains pièges sont à surveiller.
	</p>
	<p>
		Tout d'abord, le surinvestissement dans le travail de recherche et de rédaction peut mener à l'oubli de soi et à l'isolement. Les moments pour briser le rythme au baccalauréat (ex. : semaine de lecture, vacances de Noël) n'étant plus prévus à l'agenda, il peut être plus difficile de se les accorder. Le manque d'activités sociales et de loisirs ainsi que la difficulté à « décrocher » de l'essai, du mémoire ou de la thèse sans sentiment de culpabilité peuvent entraîner un état de démotivation et conduire à l'épuisement. 
	</p>
	<p>
		À l'inverse, un manque de discipline personnelle et un manque de balises dans l'horaire de travail peuvent entraîner une difficulté à se mettre à la tâche et un sentiment de tourner en rond. En outre, les étudiantes et étudiants des cycles supérieurs sont souvent sollicités pour régler toutes sortes de situations problématiques pouvant survenir (ex. : utilisation d'un logiciel, analyses statistiques). Les interruptions provenant de l'entourage sont fréquentes au sein d'un laboratoire de recherche et les heures de bénévolat ne sont souvent pas calculées. Il peut être gratifiant d'être une personne-ressource sur qui tout le monde peut compter, mais il est important d'apprendre à reconnaître et à respecter ses limites. Il est possible d'offrir son aide et de demeurer accessible en cas de difficulté (ex. : donner une information) sans faire le travail à la place de l'autre. De même, vous aussi pouvez demander de l'aide. 
	</p>
	<p>
		Enfin, les étudiantes et les étudiants aux études supérieures ont souvent plus de responsabilités (ex. : financières, familiales), ce qui surcharge davantage leur emploi du temps. Afin de pouvoir subvenir à leurs besoins, ils acceptent fréquemment des emplois exigeant un grand investissement de temps. En retour, cela a pour conséquence de retarder la progression dans leur échéancier. 
	</p>
</section>
<section id="priorites">
	<h1>Priorité aux priorités !</h1>
	<p>
		Bien utiliser votre temps, c'est vous consacrer aux tâches qui reflètent vos priorités, tant sur les plans professionnel que personnel. Il est possible de répartir vos priorités de la façon suivante : 
	</p>
	<ul>
		<li>
			Priorités associées à des responsabilités (ex. : cours, enfants, réunions, paiement de factures, échéance de remise d'un travail, tâches administratives)
		</li>
		<li>
			Priorités associées aux tâches professionnelles (ex. : lectures, recherche en bibliothèque, rédaction, heures de travail rémunérées) 
		</li>
		<li>
			Priorités personnelles, souvent laissées de côté ou non considérées comme de véritables priorités (ex. : sport, repos, alimentation saine, loisirs, relations sociales)
		</li>
	</ul>
	<p>
		Prenez le temps de réfléchir et d'identifier vos priorités pour chacune de ces catégories, pour une période donnée (ex. : le prochain mois). Qu'est-ce qui est le plus important pour vous cette session-ci ? Placez cette liste de façon à l'avoir sous les yeux quand viendra le temps de faire votre planification à long terme et à court terme. 
	</p>
	<p>
		Au quotidien, les deux principaux facteurs qui déterminent la manière dont vous choisissez d'employer votre temps sont l'urgence et l'importance. Ce n'est pas parce qu'une tâche est urgente qu'elle est importante, tout comme ce n'est pas parce que c'est important que c'est urgent. Le tableau qui suit résume les quatre façons dont le temps peut être utilisé. Il peut vous aider à situer dans quel cadre (I, II, III, IV) vous passez le plus de temps. Vous constaterez que plus l'urgence domine, plus l'importance est négligée. Les problèmes de gestion du temps proviennent souvent d'un déséquilibre entre ces différents niveaux de priorités (ex. : procrastination). Les chiffres mis entre parenthèses représentent une estimation de la proportion de temps qui devrait idéalement être accordée à ce type de priorité. 
	</p>
	<table>
		<thead>
		<tr>
			<th></th>
			<th>Urgent</th>
			<th>Non urgent</th>
		</tr>
		</thead>
		<tbody>
			<tr>
				<th>Important</th>
				<td>
					<strong>I (20-25 %)</strong>
					<ul>
						<li>
							Gérer les crises
						</li>
						<li>
							Travailler à la dernière minute
						</li>
						<li>
							Problèmes pressants
						</li>
						<li>
							Toute tâche qui requière une action immédiate
						</li>
					</ul>
				</td>
				<td>
					<strong>II (65-80 %)</strong>
					<ul>
						<li>
							Planifier ses activités
						</li>
						<li>
							Travailler à l’atteinte de ses objectifs, au développement de compétences
						</li>
						<li>
							Entretenir ses relations
						</li>
						<li>
							Ressourcement (ex. : loisirs, sport)
						</li>
					</ul>
				</td>
			</tr>
			<tr>
				<th>Pas important</th>
				<td>
					<strong>I (15 %)</strong>
					<ul>
						<li>
							Répondre aux demandes des autres
						</li>
						<li>
							Courrier, téléphone, courriels
						</li>
						<li>
							Interruptions
						</li>
					</ul>
				</td>
				<td>
					<strong>I (1 %)</strong>
					<ul>
						<li>
							Pertes de temps
						</li>
						<li>
							Échappatoires
						</li>
						<li>
							Ne rien faire d’agréable ou d’utile
						</li>
					</ul>
				</td>
			</tr>
		</tbody>
	</table>
	<p>
		La condition première pour donner la priorité aux priorités est d'établir ce qui est important (cadres I et II) au lieu de ne réagir qu'aux événements et aux urgences (cadres I et III). La décharge d'adrénaline qui accompagne une gestion du temps basée sur l'urgence peut être stimulante et excitante. Mais elle est aussi étroitement liée aux sensations de stress, de pression et de fatigue. À la longue, cela peut entraîner un sentiment de vide et l'impression que vos besoins ne sont pas satisfaits. Évidemment, l'urgence fait partie de la vie, mais le problème apparaît lorsqu'elle devient votre mode de gestion habituelle et, surtout, lorsqu'elle vous fait reléguer au second plan ce qui est important.
	</p>
	<p>
		Plusieurs activités importantes ne sont pas de nature à exercer une pression sur vous (ex. : entretien des relations, loisirs). Elles sont liées à un sentiment de satisfaction (cadre II). Puisqu'elles ne sont pas urgentes, qu'elles ne requièrent pas une intervention immédiate, c'est donc à vous de les maîtriser (ex. : vous réserver un moment dans votre agenda).
	</p>
	<p>
		Avoir des priorités, c'est orienter vos efforts dans les tâches du cadre II, en tentant de vous retrouver le moins possible dans les cadres I et III. Les cadres III et IV ne devraient idéalement pas occuper une trop grande proportion de votre temps. Avoir des priorités, c'est aussi faire des choix. Vouloir tout faire est une illusion, car il y aura toujours plus de choses à faire que de temps disponible. Réfléchissez à ce qui est vraiment important pour vous, mais aussi, acceptez vos limites. La clé de la qualité de vie réside dans ces choix, effectués chaque jour.
	</p>
</section>
<section id="long-terme">
	<h1>Planification à long terme : l'échéancier à rebours</h1>
	<p>
		La clarification de vos priorités est la base de la planification de votre temps. Cela vous aidera à faire les bons choix face aux nombreux dilemmes que comporte la vie de tous les jours. Pour planifier, le rapport au temps s'opère à deux niveaux : un niveau plus large, où les tâches principales requises par un essai, un mémoire ou une thèse doivent être identifiées et ordonnées de façon réaliste afin qu'elles soient accomplies dans les délais prévus, et un niveau plus circonscrit, où les tâches spécifiques de chacune des parties constituantes doivent être intégrées à une planification hebdomadaire. 
	</p>
	<p>
		Afin de vérifier si votre projet est réalisable ou non dans les délais prévus et de ne pas succomber à la tentation de toujours ajuster le temps, la méthode de l'échéancier à rebours peut s'avérer fort utile.
	</p>
	<ol>
		<li>
			Dressez d'abord la liste des étapes à franchir, de la dernière à la première;
		</li>
		<li>
			Estimez le temps requis pour chacune de ces étapes; 
		</li>
		<li>
			Créez un échéancier, en commençant par la dernière étape à réaliser et en terminant par la première. 
		</li>
	</ol>
	<table>
		<caption>Exemple : mémoire de maîtrise débutant en septembre 2005</caption>
		<thead>
			<tr>
				<th>Étapes</th>
				<th>Temps requis (approximatif)</th>
				<th>Échéances</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th>Lectures, définition de la problèmatique</th>
				<td>1 session</td>
				<td>15 décembre 2005</td>
			</tr>
			<tr>
				<th>Élaboration et rédaction du cadre théorique</th>
				<td>8 semaines</td>
				<td>1<sup>er</sup> mars 2006</td>
			</tr>
			<tr>
				<th>Formulation des hypothèses</th>
				<td>2 semaines</td>
				<td>15 mars 2006</td>
			</tr>
			<tr>
				<th>Expérimentation et rédaction de la méthodologie</th>
				<td>8 semaines</td>
				<td>15 mai 2006</td>
			</tr>
			<tr>
				<th>Analyse des résultats</th>
				<td>6 semaines</td>
				<td>30 juin 2006</td>
			</tr>
			<tr>
				<th>Interprétation et rédaction des résultats</th>
				<td>4 semaines</td>
				<td>30 juillet 2006</td>
			</tr>
			<tr>
				<th>Arrêt : période de vacances</th>
				<td>4 semaines</td>
				<td>30 août 2006</td>
			</tr>
			<tr>
				<th>Rédaction : discussion</th>
				<td>2 semaines</td>
				<td>15 septembre 2006</td>
			</tr>
			<tr>
				<th>Rédaction : introduction, conclusion, synthèse et pistes de recherche futures</th>
				<td>2 semaines</td>
				<td>30 septembre 2006</td>
			</tr>
			<tr>
				<th>Liste des tableaux et figures, appendices et références</th>
				<td>2 semaines</td>
				<td>15 octobre 2006</td>
			</tr>
			<tr>
				<th>Relecture (dépôt initial)</th>
				<td>3 semaines</td>
				<td>5 novembre 2006</td>
			</tr>
			<tr>
				<th>Corrections</th>
				<td>2 semaines</td>
				<td>15 décembre 2006</td>
			</tr>
			<tr>
				<th>Dépôt final</th>
				<td></td>
				<td>décembre 2006</td>
			</tr>
		</tbody>
	</table>
	<p>
		Bien entendu, votre directrice ou directeur peut vous donner un coup de main en vous indiquant les différentes étapes à venir et en vous aidant à estimer le temps requis pour chacune de ces étapes. 
	</p>
	<p>
		Si vous constatez, en faisant cet exercice, que vous n'arriverez pas à respecter la date que vous vous étiez fixée, il est alors nécessaire d'évaluer à nouveau le temps requis pour chacune des étapes à franchir mais surtout de réviser cette date. Inutile de préciser que votre planification doit être envisagée avec souplesse et peut être réajustée, selon le déroulement des événements. 
	</p>
</section>
<section id="court-terme">
	<h1>Planification à court terme</h1>
	<p>
		Une fois que vous avez planifié les grandes étapes de votre projet de recherche, il est conseillé de planifier les différentes tâches spécifiques à l'accomplissement de ces grandes étapes, et ce, semaine après semaine. Voici ici des suggestions quant à la façon de procéder. 
	</p>
	<ol>
		<li>
			À chaque semaine, faites la liste de toutes les tâches à compléter, divisez-les en petites tâches précises. 
		</li>
		<li>
			Notez dans votre agenda les activités que vous devez faire à une heure fixe déjà prévue (ex. : cours, réunions, rendez-vous, repas, emploi, etc.) et, sur une liste à part, celles que vous désirez accomplir cette semaine, mais au moment qui sera le plus opportun de la journée ou de la semaine. Utilisez un agenda où vous pourrez visualiser la semaine entière sur une page. 
		</li>
		<li>
			Répartissez vos autres tâches, professionnelles et personnelles, dans les périodes disponibles. Estimez et prévoyez des plages pour chacune, de durée variable. Par exemple, des tâches telles que la recherche en bibliothèque et la rédaction requièrent des périodes plus longues (deux heures et plus). Ne faites pas plus de trois heures de travail intellectuel continu et accordez-vous des pauses afin de préserver et stimuler votre concentration. 
		</li>
		<li>
			Planifiez exactement ce que vous ferez dans la période de travail (ex. : lire tel article, exécuter tel test statistique, rédiger telle section). Procédez par ordre de priorité, en vous assurant de garder du temps en premier pour ce qui est le plus important, et en complétant par ce qui l'est moins. N'oubliez pas vos priorités personnelles ! Cela vous aidera à consacrer les périodes les plus productives à ce qui est vraiment important (cadres I et II), plutôt que de laisser votre temps s'écouler lors d'activités qui détournent votre attention (cadres III et IV). Même si vous n'avez pas accompli l'ensemble des tâches que vous aviez prévu pour une journée, vous aurez quand même la satisfaction d'avoir travaillé à une tâche importante.
		</li>
		<li>
			Prévoyez des périodes « tampons » et des périodes libres qui vous procureront une certaine marge de manoeuvre en cas d'imprévus. Deux périodes de deux heures chacune dans la semaine peuvent s'avérer très bénéfiques. Les plages sont interchangeables, ce qui permet de conserver une flexibilité dans votre horaire. Cette flexibilité est essentielle. La vie ne suit pas automatiquement le cours d'une page d'agenda, et c'est une chance, puisque cela conduirait à une existence dépourvue de toute spontanéité ! Tenez compte de l'imprévu, car la planification n'est pas destinée à ce que votre emploi du temps soit coulé dans le béton.
		</li>
		<li>
			Respectez la tâche qui a été planifiée. N'ajoutez jamais plus d'une demi-heure au temps initialement prévu, sinon vous n'arriverez jamais à prendre votre planification au sérieux. Replanifiez immédiatement ce qui n'a pas pu être fait. À l'inverse, si la tâche a requis moins de temps que prévu, n'en faites pas plus, cela viendrait éliminer l'effet motivant. Réorganisez, agissez et réévaluez votre horaire si des choses réellement plus importantes que celles que vous aviez prévues se présentent. Et surtout, passez en revue votre planification au début de la journée, puis au cours de celle-ci. 
		</li>
	</ol>
</section>
<section id="organiser-temps">
	<h1>Du temps pour organiser votre temps</h1>
	<p>
		Ce conseil peut paraître simple. Mais pour beaucoup d'étudiantes et d'étudiants, organiser son temps se limite à remplir son agenda des différents rendez-vous qui se présentent et à tenter de travailler dans les intervalles qui restent. Pour gagner du temps, il faut d'abord en prendre un peu afin de réfléchir à votre emploi du temps. À la fin de chaque semaine, gardez-vous un moment pour faire le bilan de ce qui a été accompli et de ce qui a été laissé en plan, et cherchez à identifier quelles en sont les raisons. Réévaluez : 
	</p>
	<ul>
		<li>
			le réalisme de vos objectifs
		</li>
		<li>
			la difficulté à dire non 
		</li>
		<li>
			la possibilité de déléguer certaines tâches
		</li>
		<li>
			la présence d'interruptions (qui auraient pu être évitées ?)
		</li>
		<li>
			la procrastination
		</li>
		<li>
			le perfectionnisme
		</li>
		<li>
			le temps consacré aux loisirs
		</li>
	</ul>
	<p>
		Basez-vous sur l'expérience de votre semaine pour améliorer votre planification de la suivante. C'est en vous y prenant de la sorte que vous vous connaîtrez davantage et arriverez, par exemple, à mieux respecter votre rythme de travail, les moments où vous êtes le plus efficace et les combinaisons de tâches qui vous conviennent le mieux. Puisque c'est vous qui avez choisi de faire ce que vous avez planifié, votre emploi du temps ne devrait pas être perçu comme une contrainte mais comme un moyen d'atteindre vos buts.
	</p>
</section>
<section id="conclusion">
	<h1>Conclusion</h1>
	<p>
		Vous l'avez tous et toutes probablement expérimenté, ce n'est pas du premier coup que vous arriverez à établir et à respecter votre planification. C'est par essais et erreurs que vous apprendrez à planifier de façon réaliste et efficace. La gestion du temps est un processus dynamique, qui se construit au fur et à mesure que le temps avance, et que vous devez avoir constamment en tête afin de faire les ajustements nécessaires au gré des événements ! 
	</p>
</section>
<section id="references">
	<h1>Références</h1>
	<p><cite>
		Bégin, C. (1995), Apprivoiser l'isolement. Le contexte (atelier III). Contenu de présentation pour les ateliers sur l'encadrement aux études supérieures [Document inédit], Université du Québec à Montréal.
	</cite></p>
	<p><cite>
		Covey, S. R. (1995), Priorité aux priorités : vivre, aimer, apprendre et transmettre. Paris, First Éditions. 
	</cite></p>
	<p><cite>
		Lelord F. (2001), Bien vivre avec son stress, Paris, Contre la montre. 
	</cite></p>
	<p><cite>
		Talbot, F. (2003), « Les défis du contexte de rédaction », Vies-à-vies. Bulletin du Service d'orientation et de consultation psychologique, Université de Montréal, vol. 15, no 3 (janvier).
	</cite></p>
</section>
<footer>
	<h2>Rédiger par :</h2>
	<p>
		Louise Careau, psychologue<br>
		Véronique Mimeault, psychologue
	</p>
</footer>
