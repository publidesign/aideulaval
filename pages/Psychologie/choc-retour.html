<p>
	Avec les moyens de communication et de transport qui se sont considérablement développés au cours des dernières décennies, les frontières entre les pays se sont estompées et les cultures du monde sont devenues accessibles. De plus en plus de gens quittent momentanément leur milieu pour des séjours à l'étranger, allant de quelques jours à plusieurs années : voyages d'affaires, tourisme, échanges culturels, apprentissage de langues, développement international, pèlerinages, etc. Un long voyage n'est plus l'apanage de quelques idéalistes, mais une expérience recherchée et à la mode. L'Université Laval mise sur l'ouverture sur le monde avec des stages à l'étranger, le programme Profil international, sans oublier les organismes et agences présents sur le campus. Si, pour partir, la panoplie de ressources et de moyens disponibles semble aussi variée qu'accessible (à l'université comme ailleurs), qu'en est-il du retour ? Certes, un départ pour l'étranger demande une longue préparation, un peu d'audace et de l'argent. Mais suffit-il d'avoir son billet d'avion pour bien rentrer ? Quelles sont les implications d'un retour chez-soi après un long séjour à l'étranger ?
</p>
<nav>
	<ul>
		<li><a href="#pourquoi" title="Pourquoi un choc ?">Pourquoi un choc ?</a></li>
		<li><a href="#symptomes" title="Les symptômes du choc culturel">Les symptômes du choc culturel</a></li>
		<li><a href="#stades" title="Les stades du retour">Les stades du retour</a></li>
		<li><a href="#croyances" title="Les croyances reliées au choc du retour">Les croyances reliées au choc du retour</a></li>
		<li><a href="#facteurs" title="Les facteurs affectant l'intensité et la durée">Les facteurs affectant l'intensité et la durée</a></li>
		<li><a href="#reintegration" title="Des moyens de faciliter la réintégration">Des moyens de faciliter la réintégration</a></li>
		<li><a href="#references" title="Références">Références</a></li>
	</ul>
</nav>
<section id="pourquoi">
	<h1>Pourquoi un choc ?</h1>
	<p>
		Les gens qui partent pour un long séjour à l'étranger vivent une véritable expérience de découvertes extérieures et intérieures. Les implications sont telles que, souvent, elles peuvent provoquer des conséquences plus ou moins prévues au retour. Les voyageurs savent qu'une adaptation est nécessaire quand ils partent à l'étranger, et c'est ce dépaysement qu'ils recherchent. Cependant, plusieurs présument que le retour se fait facilement, alors que la réintégration demande aussi une période d'adaptation. Ce décalage entre les attentes au retour et la réalité peut provoquer un ensemble de symptômes qui font partie de ce qui est appelé le choc du retour. Il est très important de mentionner que pour environ la moitié des voyageurs, le retour se fait sans problème et la réadaptation est plutôt facile. Pour l'autre moitié, la réinsertion est plus difficile, ce qui entraîne parfois une certaine détresse. Cette transition peut donner lieu à des changements significatifs. Par exemple, les valeurs, les relations et les choix de vie peuvent avoir été modifiés durant le voyage et doivent être intégrés à la vie « ordinaire ». Toutefois, les difficultés auxquelles on s'expose lors de cette période de réadaptation ne constituent presque jamais une source de regrets, surtout devant la fabuleuse expérience que représente un voyage. 
	</p>
</section>
<section id="symptomes">
	<h1>Les symptômes du choc culturel</h1>
	<p>
		Le choc culturel est vécu par les gens qui ont de la difficulté à s'adapter à un nouveau milieu, mais aussi par ceux qui se replongent difficilement dans leur milieu d'origine après un long séjour à l'extérieur. Les symptômes qui composent le choc culturel peuvent varier en intensité, en durée et en ordre d'apparition.
	</p>
	<p>
		Les voyageurs et voyageuses qui éprouvent un choc culturel peuvent avoir la <strong>nostalgie</strong> du pays visité. Ils porteront systématiquement un regard négatif sur l'endroit où ils sont et un regard positif sur les autres endroits, en particulier celui d'où ils viennent. Par exemple, l'étudiant ou l'étudiante revenant d'un pays pauvre remarquera seulement le gaspillage et la consommation comme valeur de sa société et repensera avec nostalgie au type de partage dont il ou elle a été témoin durant son voyage. En plus de regretter l'endroit d'où ils arrivent, ceux et celles qui éprouvent un choc culturel seront <strong>tristes</strong> et mettront <strong>peu d'énergie</strong> dans leurs activités habituelles. Ils auront <strong>tendance à s'isoler</strong>, à éviter les contacts avec les habitantes et habitants de leur région, au profit d'une ouverture pour ceux qui viennent d'ailleurs. Comme si des gens d'une culture étaient mieux ou plus intéressants que ceux d'une autre. En plus d'exagérer certains problèmes et d'en négliger d'autres qui pourtant les préoccupaient auparavant, ils éprouvent de la <strong>colère</strong> et de la <strong>frustration</strong>. Ils montrent de <strong>l'ambivalence</strong> quant à leurs positions et à leur statut. Leur <strong>sommeil</strong> et leur <strong>appétit</strong> peuvent être <strong>perturbés</strong>, avec des variations de poids, un sommeil excessif ou de l'insomnie. <strong>Irritables</strong>, leur habituel sens de l'humour est obstrué par des inquiétudes et des réactions excessives. Cette émotivité exacerbée amène des tensions dans leurs relations avec leur conjoint ou conjointe, leurs amies, amis ou famille. Ces symptômes sont vécus durant la phase du choc du retour et s'estompent graduellement durant la réintégration ou la réadaptation.
	</p>
</section>
<section id="stades">
	<h1>Les stades du retour</h1>
	<p>
		La structure suivante n'est pas absolue et ne correspond pas à ce qui est vécu par tous les voyageurs et voyageuses à leur retour. Elle aide cependant à comprendre le processus encouru et à rendre l'expérience du retour moins mystérieuse. Ce processus implique des pertes, des gains et d'autres changements qui surviennent souvent selon l'ordre proposé dans ces étapes. Le voyageur ou la voyageuse qui a dû déployer des efforts d'adaptation et de contrôle pendant son séjour à l'étranger espère qu'en revenant à la maison il ou elle pourra enfin faire relâche et se laisser aller à être soi-même. En général, plus la perception d'une réintégration facile et automatique est ancrée, plus longue et difficile sera la réadaptation. Voyons comment cela pourrait se passer pour vous, voyageur ou voyageuse.
	</p>
	<h2>1. La préparation du retour</h2>
	<p>
		Il est facile de présumer que les étapes du retour débutent à l'aéroport. C'est exact au sens pratique, mais aux niveaux psychologique et émotionnel, elles commencent durant les semaines qui précèdent votre départ du pays étranger. Avant de le quitter, vous êtes occupé ou occupée à saluer des gens que vous ne reverrez peut-être jamais et à visiter des endroits que vous n'aurez plus l'occasion de revoir. Ceci demande une préparation et la coordination de plusieurs agendas dans un laps de temps restreint. Vous pensez peu à votre chez-vous et, quand vous le faites, vous anticipez très positivement les moments qui suivront votre retour. Vous imaginez de joyeuses retrouvailles avec votre famille, vos amis et amies, des repas à vos restaurants préférés, des sports que vous n'avez pu pratiquer depuis des mois, etc. Au plan émotionnel, vos sentiments peuvent être ambivalents. D'une part, vous exultez de revoir bientôt vos proches ; d'autre part, vous vous désolez de quitter définitivement les gens qui ont fait partie de votre vie outre-mer.
	</p>
	<h2>2. La lune de miel</h2>
	<p>
		La lune de miel, ce sont les semaines qui suivent votre arrivée chez vous. Ces semaines sont près de la perfection : les choses se déroulent en général comme vous l'aviez souhaité. Les gens sont contents de renouer avec vous et vous êtes heureux, heureuse de les rencontrer. C'est une période euphorique. D'un endroit à l'autre, vous êtes le centre d'attention, une sorte de visiteur-vedette. Vous engendrez des réactions partout sur votre passage, les gens s'intéressent à vous, veulent vous entendre, vous posent des questions et regardent vos photos. Ces contacts sont si chargés d'émotions et de candeur que vous ne remarquez pas les changements qui ont pu se produire chez les autres, dans votre milieu, ou en vous-même. Vous n'avez pas le temps de penser à ces changements, ni de songer à ceux et celles que vous avez laissés au loin et que vous ne reverrez peut-être jamais. Vous en profitez plutôt pour faire tout ce qui vous manquait durant votre voyage : voir des films, manger des mets québécois, pratiquer des sports et parler enfin dans votre propre langue. Les gens s'ouvrent à vous sans attente, sans rien demander. Vous êtes exempt ou exempte de préoccupations financières, ménagères ou scolaires. Dans vos pensées, vous êtes encore très loin des études, des prêts et bourses ou des tâches comme le ménage de la salle de bain ! Votre vie ressemble alors à des vacances. N'étant pas installé ou installée de façon stable à aucun endroit, vous n'êtes pas confrontée ou confronté à certaines réalités de la vie et les décisions sont remises à plus tard. En un sens, pour vous, le voyage se poursuit.
	</p>
	<h2>3. Le choc du retour</h2>
	<p>
		La lune de miel ne peut se prolonger indéfiniment. C'est quand la réalité de votre vie   « habituelle » vous rattrape que survient le véritable choc du retour. Ce contact avec la réalité advient habituellement quand la tournée des visites est complétée et que les gens sont de nouveau habitués à votre présence. Pour votre famille, vos amies et amis, la magie est passée. Ils ont vu vos photos de voyage et s'intéressent moins à votre expérience. Ils considèrent que le temps «régulier» est revenu et que vous devez faire face à vos obligations. Pour vous, ce n'est pas terminé. Vous n'êtes peut-être pas convaincu ou convaincue que le chemin de vie que vous vous étiez tracé avant votre départ vous convient encore. Par exemple, vous pouvez remettre en question vos choix amoureux, votre plan de carrière et hésiter entre commencer une nouvelle vie ou continuer celle que vous aviez avant. Pendant que vous souffrez et que vous vous questionnez, les gens croient que vous allez bien.
	</p>
	<p>
		Alors commence la période des jugements. Vous portez un regard systématiquement critique sur ce qui vous entoure et pensez avec nostalgie au pays où vous avez vécu. Cela dépasse les immanquables comparaisons entre deux cultures. Il semble y avoir une détermination à trouver la vie à l'étranger meilleure que celle de sa propre culture (voir les mythes dans l'encadré). Vos souvenirs de voyage, même les moments les plus difficiles, deviennent magiques, sans doute parce qu'ils sont inatteignables, alors que ce qui est accessible ici perd sa valeur. Des souvenirs, parfois anodins, sont idéalisés et ce qui est vécu ici est déprécié. Vous vous rappelez avec nostalgie les yeux d'un pauvre croisé là-bas et méprisez la jovialité du commis au dépanneur du coin. Ceci semble paradoxal devant le statut de personne ouverte, patiente et tolérante que vous vous donnez. Ce manque d'objectivité peut devenir agaçant pour vos proches, surtout quand ils sont visés par vos jugements. En fait, ces petits procès ne sont pas attribuables à des éléments de votre culture, mais à l'état émotionnel instable dans lequel vous vous trouvez, car vous réagissez plus à votre tristesse qu'à ce qui vous entoure. Le pays visité vous manque et vous êtes ambivalente, ambivalent face à votre avenir ici. Cette distance permet un regard critique sur son milieu, ce qui implique un réaménagement des valeurs ou une consolidation de l'identité. Par exemple, après avoir vu d'autres habitudes de vie ailleurs, certaines personnes vont diminuer leur consommation et inciter leurs proches à recycler davantage.
	</p>
	<p>
		Cette période de transformation au cours de laquelle vous réalisez à quel point vous avez changé est marquée par la perception de <strong>vivre en marge</strong>. Vous ne vous identifiez plus comme avant à votre culture d'origine et vous savez très bien que vous n'appartenez pas à celle du pays visité, ce qui vous donne la perception d'être apatride. Pendant cette redéfinition de valeurs qui marqueront votre identité, vous vous sentez comme un hybride culturel : divisé entre deux cultures, sans vraiment vous reconnaître dans l'une ou dans l'autre, vous vous complaisez à ne pas trouver votre place. Vous aimez la présence d'étrangers, vous vous percevez comme un être à part, alors que vous êtes perçu ou perçue par vos proches comme étant des leurs.
	</p>
	<p>
		Alors survient le <strong>doute</strong>. Empreint ou empreinte de tristesse et de déception, vous remettez en question vos choix et vos décisions de revenir chez vous. Vous vous cherchez et songez à retourner vous trouver ailleurs. Ces doutes peuvent être paralysants, et l'idée de repartir, bien que tentante, n'est pas réaliste. Au mieux, c'est une solution temporaire. Ce cul-de-sac survient à un moment où plusieurs choses doivent être faites si vous désirez vraiment vous établir ici à nouveau, ce qui crée une pression.
	</p>
	<p>
		À vivre en marge et dans le doute, vous faites face à des symptômes propres au choc du retour. Cette <strong>lutte</strong> devant le quotidien se traduit par un manque de concentration, par de la tristesse ou une lourdeur émotionnelle, par un sommeil difficile ou excessif, par un manque de motivation pour des tâches ordinaires, par une ambivalence affective ou par de la peur. Entamer une session universitaire de plein fouet dès votre retour d'un long séjour peut être laborieux et réussir à vous plonger dans une matière abstraite relève parfois de l'exploit. Comment se concentrer sur un document de droit ou d'informatique quand on a l'impression d'arriver d'une autre planète ? Le combat pour vous réorganiser malgré tous ces symptômes peut renforcer l'idée que vous n'êtes pas normal ou normale et que quelque chose ne va pas. 
	</p>
	<p>
		Paradoxalement, même si vous ne vous sentez pas bien, vous ne souhaitez pas nécessairement aller mieux ! Cette <strong>résistance</strong> est due au fait que vous avez l'impression qu'en vous intégrant ici, vous renoncez à votre expérience. Et cette pensée vous paraît répugnante. Pour vous, redevenir à l'aise dans une vie qui vous semble ordinaire équivaut à la fin de la personne excitante que vous croyez être devenue. En réalité, aimer à nouveau sa vie chez soi ne signifie pas nier son vécu d'outre-mer. En relâchant vos résistances, la réintégration est possible et l'idée de repartir peut être reportée à plus tard.
	</p>
	<blockquote>
		L'aventure n'est pas un endroit mais un moment.
	</blockquote>
	<h2>4. La réintégration</h2>
	<p>
		Habituellement, c'est lentement et sans que vous ne vous en aperceviez que la réintégration s'installe. Le stress, la résistance, le doute et l'isolement propres à l'étape du choc du retour ont une fin. Tranquillement, vous arrivez par vous sentir chez vous. Sans renier votre fabuleuse expérience, vous avez perdu l'envie de repartir. Vous êtes moins nostalgique et admettez plus facilement que plusieurs choses sont très bien ici. Comment ce changement est survenu ?
	</p>
	<p>
		Avec le temps. Probablement plus de temps que prévu, ce qui cause une bonne partie des difficultés. Vous et vos proches croyiez que vous alliez être bien dès votre arrivée et que vous pourriez reprendre rapidement vos vieilles habitudes. Mais ça ne va plus parce que vous avez changé, ou plutôt parce que vous ne voulez pas redevenir cet ancien vous. Vous devez donc vous donner le temps de trouver ce qui vous convient vraiment. Avec les semaines qui ont passé, le monde qui vous entoure est redevenu familier et vous vous êtes organisé une vie qui contient ce que vous avez rapporté de votre voyage. Une routine sécurisante s'est installée, avec les réflexes de la vie courante. La critique, le stress et l'irritabilité ont fait place à une paix et à une confiance en vous différente. Votre attitude est plus objective et dégagée face aux souvenirs de votre périple et vous retrouvez un intérêt pour des sujets qui n'ont pas de lien avec la vie à l'étranger. Vous ne vous sentez plus coincée, coincé entre deux cultures. Sans entêtement, vous sélectionnez des valeurs acquises au loin et vous les adaptez à la vie nord-américaine. Le deuil à traverser est accompli dans un choix libre de valeurs, de souvenirs et de relations des deux mondes connus. Par exemple, la patience remarquée dans certaines grandes villes de pays orientaux pourrait être une qualité à intégrer avec l'efficace gestion du temps occidentale. 
	</p>
	<p>
		Avec cette stabilité émotive et ce discernement, les relations reprennent leur cours, parfois avec des changements. La distance, le temps, les nouvelles valeurs et les intérêts ont des répercussions dans les relations, ce qui n'empêche pas votre famille et vos bons amis et amies de pouvoir enfin vous reconnaître.
	</p>
	<p>
		Le temps nécessaire avant de se sentir «revenu» ou «revenue» est très variable et peut aller de un à dix-huit mois (Dumas, 2000). Certaines personnes doivent s'ajuster dans certains secteurs de leur vie, soit dans leur travail, leurs études ou leur vie personnelle. Au bout de tout ce processus, le résultat est le même. En tant que voyageur ou voyageuse, vous ne vous sentez plus hors du présent et vivant dans cet ailleurs, mais en harmonie avec le fait de penser parfois à un autre monde alors que votre vie est ici. Vous êtes passée ou passé de « là-bas OU ici » à « là-bas ET ici ». C'est donc avec un décalage important, de façon subtile et inattendue que vous retrouvez le sentiment d'être enfin revenu ou revenue chez vous.
	</p>
	<blockquote>
		Maintenant, je dois avancer, créer des choses, vivre une aventure, mon aventure, ici-même, pleinement. Mais je ne m'endors jamais sans rêver à l'Arabie.
		<cite>R. Blythe, Akenfield</cite>
	</blockquote>
</section>
<section id="croyances">
	<h1>Les croyances reliées au choc du retour</h1>
	<div class="encadre">
		<p>
			Là-bas je vivais, ici je meurs.
		</p>
		<p>
			Les gens d'ici qui ne sont pas sortis sont "plates", les étrangers et ceux qui ont voyagé sont intéressants et ouverts.
		</p>
		<p>
			Je n'avais pas de problèmes à l'étranger. Ici, il y a plein d'affaires qui ne marchent pas.
		</p>
		<p>
			J'ai vécu à... des choses que je ne pourrai jamais vivre ici. 
		</p>
		<p>
			C'est ennuyeux au (à) Québec.
		</p>
		<p>
			J'ai développé là-bas des capacités qui dorment ici.
		</p>
		<p>
			Ici, il faut toujours penser à l'argent.
		</p>
		<p>
			Personne ici ne peut me comprendre. Les gens ne s'intéressent pas vraiment à ce que j'ai vécu.
		</p>
		<p>
			Je n'ai plus de chez-moi. Je ne suis chez moi nulle part.
		</p>
		<p>
			Là-bas, j'étais libre et je ne dépendais de personne. Ici, on est pris dans un système. J'ai plein d'obligations.
		</p>
		<p>
			Quand j'étais là-bas, chaque minute était intense et pleine de vie. Depuis que je suis de retour, c'est long et plate.
		</p>
	</div>
</section>
<section id="facteurs">
	<h1>Les facteurs affectant l'intensité et la durée</h1>
	<div class="encadre">
	<ol>
		<li>
			La décision. Le retour est-il souhaité ou non ? Moins le retour au pays d'origine est choisi, plus il est difficile.
		</li>
		<li>
			L'âge. Le retour peut être plus facile pour des personnes plus âgées, qui ont vécu plus de transitions.
		</li>
		<li>
			Le degré de différences entre la culture hôte et la culture d'origine. Plus il y a de différences entre les deux cultures, plus la réintégration est longue (ex. : la France ressemble plus au Québec que la Thaïlande).
		</li>
		<li>
			Les autres expériences de retour. La première fois serait plus difficile.
		</li>
		<li>
			La durée du séjour à l'étranger. Bien sûr, plus le voyage est long, plus le pays hôte peut être difficile à quitter. La réintégration au pays d'origine peut-être plus ardue.
		</li>
		<li>
			Le degré d'interaction avec la culture étrangère. Plus on était impliqué dans cette culture, plus il est difficile de la quitter.
		</li>
		<li>
			L'environnement au retour. Le support de la famille et des amis facilite la réintégration.
		</li>
		<li>
			Les interactions avec les proches de son pays d'origine durant le séjour à l'étranger. Les échanges par lettres et par téléphone permettent d'entretenir un lien durant son absence.
		</li>
	</ol>
	</div>
</section>
<section id="reintegration">
	<h1>Des moyens de faciliter la réintégration</h1>
	<h2>1. Prévoir une période d'adaptation au retour.</h2>
	<p>
		Informez-vous d'avance pour être conscient ou consciente de la possibilité d'avoir à vivre un réajustement au retour. Aussi, préparez-vous mentalement à la différence de température. Arriver dans la grisaille de novembre ou le dégel d'avril après des mois de soleil et de ciel bleu peut affecter l'humeur.
	</p>
	<h2>2. Se donner du temps.</h2>
	<p>
		Il est normal qu'un processus de changement soit long. Le rapport au temps a peut-être changé durant le voyage, ce qui peut être une belle valeur à garder.
	</p>
	<h2>3. Avoir des plans.</h2>
	<p>
		Revenir avec rien devant soi augmente l'impression de vide et de déconnexion. Prévoyez d'abord une période de repos : vous ne revenez pas de vacances mais d'une expérience exigeante. Après ce répit, un plan pré-établi vous donnera une direction pratique, surtout si vous vous sentez désorienté ou désorientée. Engagez-vous dans un travail, dans la poursuite de vos études ou dans une autre expérience, de préférence locale. Ce type de projet n'est pas une fuite mais représente un but auquel vous pouvez vous accrocher, tout en retombant graduellement sur vos pieds. Il est préférable que ces plans soient malléables. Par exemple, explorez la possibilité de prendre moins de crédits de cours, mais de tout de même vous inscrire à quelques-uns.
	</p>
	<h2>4. Avoir de l'argent.</h2>
	<p>
		Les voyageurs et voyageuses sont souvent fauchés à leur retour. Certains dépensent toutes leurs ressources durant le voyage et reviennent quand il ne leur reste plus rien. Dans notre société, la vie est basée sur la consommation et elle coûte cher. Presque toutes les activités coûtent inévitablement quelque chose. Ne pas pouvoir suivre vos amis et amies dans leurs sorties ajoute au choc et au sentiment d'isolement.
	</p>
	<h2>5. Adapter les nouvelles valeurs.</h2>
	<p>
		Il est possible de transposer des valeurs acquises à l'étranger à la vie d'ici, concernant par exemple l'alimentation (manger des produits plus naturels), la gestion du temps (profiter de la vie) et les priorités (éviter la performance à tout prix).
	</p>
	<h2>6. Parler à des gens qui s'intéressent à votre expérience.</h2>
	<p>
		Quelques semaines après votre retour, votre famille, vos amies et amis en ont assez d'entendre parler de ce pays qu'ils n'ont jamais vu. Ils ne peuvent partager pleinement votre expérience et ont leurs propres préoccupations. Il est préférable alors d'assouvir votre besoin, bien légitime, d'échanger sur ce que vous avez vécu avec des gens qui s'y intéressent, comme d'autres Québécois et Québécoises qui ont voyagé, des étrangères ou étrangers en cours de séjour ici, ou encore des immigrants ou immigrantes.
	</p>
	<h2>7. Redécouvrir sa région comme touriste.</h2>
	<p>
		Puisque, effectivement, vous vous sentez étranger, étrangère chez vous, profitez de votre statut  de «rapatrié» ou de «rapatriée» pour voir votre région avec de nouveaux yeux ! Avec votre nouvelle perspective, votre intérêt aiguisé pour mieux connaître l'histoire et observer la nature, ça ne peut qu'être intéressant.
	</p>
	<h2>8. Se définir par autre chose que le voyage.</h2>
	<p>
		Une tendance réductrice est de se présenter, ou d'être présentée ou présenté par les autres, comme « celui qui est allé en Afrique » ou « l'amie qui a étudié en Asie ». Vous êtes autre chose que votre voyage. Vous avez tout un passé et tout un avenir qui existent hors de cette expérience. C'est à vous de vous brancher sur d'autres parties de ce que vous êtes ou sur d'autres morceaux de votre vécu.
	</p>
	<h2>9. Consulter.</h2>
	<p>
		Un voyage n'est pas une thérapie et partir ne règle pas les problèmes qui, de toute façon, réapparaissent au retour. Les difficultés qui peuvent être encourues par la réadaptation constituent un motif valable pour consulter. Il existe des ressources pour consulter, seul ou en groupe. Plusieurs des organismes qui envoient des gens à l'étranger, tels que l'Agence canadienne de développement international (ACDI) ou Jeunesse Canada Monde, fournissent un appui sous forme de rencontres de groupe au retour.
	</p>
	<h2>10. Écrire dans son journal de bord.</h2>
	<p>
		Plusieurs voyageurs et voyageuses ont la bonne idée de consigner leurs pensées et leurs expériences dans un journal personnel durant leur périple à l'étranger. Malheureusement, ils ou elles le mettent de côté au retour dans leur milieu. Pourtant, à la lumière de tout ce qui est mentionné dans ce texte sur le choc du retour, il est évident qu'en quelque sorte, l'aventure se poursuit mais à l'intérieur de soi !
	</p>
</section>
<section id="references">
	<h1>Références</h1>
	<p><cite>
		Blythe, R. (1972), Akenfield, Pantheon, Allen Lane Editions.
	</cite></p>
	<p><cite>
		Dumas, B. (2000), Comment se passe la réinsertion sociale et professionnelle des jeunes après une expérience à l'étranger, Chicoutimi.
	</cite></p>
	<p><cite>
		Storti, C. (1997), The art of coming home, Yarmouth (Me.), Intercultural Press.
	</cite></p>
	<p><cite>
		Collectif (2001), Le guide du routard humanitaire, Paris, Hachette.
	</cite></p>
	<p>
		Plusieurs articles sont accessibles sur Internet, en faisant une recherche avec les termes « reverse culture shock ».
	</p>
</section>
<footer>
	<h2>Rédiger par :</h2>
	<p>
		Marcel Bernier, psychologue
	</p>
</footer>
