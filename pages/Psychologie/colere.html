<blockquote>
	Si vous êtes patient un jour, vous échapperez à cent jours de chagrin. 
	<cite>Proverbe chinois</cite>
</blockquote>
<p>
	Nous savons tous ce qu'est la colère et nous l'avons tous déjà ressentie, que ce soit sous forme de léger agacement ou de rage fulminante. Bien qu'elle soit perçue comme une émotion négative et laide et que, dans notre culture, on nous ait souvent appris à la réprimer, la colère est une émotion humaine tout à fait naturelle, habituellement saine. Elle peut nous être bien utile car elle nous motive et nous donne de l'énergie pour surmonter les obstacles, résoudre des problèmes et atteindre des buts. Cependant, lorsque la colère devient hors de contrôle et destructive, elle peut entraîner des problèmes à l'école, au travail, dans nos relations interpersonnelles et nuire à notre qualité de vie. La maîtrise de nos émotions négatives est la clé du bien-être affectif. On ne peut pas éviter les sentiments pénibles car ils font partie de la vie, mais pour se sentir bien, il importe de contenir ces orages qui occupent toute la place dans notre esprit. Mais attention : facile à dire mais difficile à faire ! De toutes les émotions, la colère est la plus rebelle et la plus difficile à maîtriser. Nous vous présentons, dans ce texte, ce qui caractérise la colère et suggérons quelques moyens qui vous aideront à mieux la circonscrire et la contrôler.
</p>
<nav>
	<ul>
		<li><a href="#pourquoi" title="Pourquoi nous mettons-nous en colère ?">Pourquoi nous mettons-nous en colère ?</a></li>
		<li><a href="#mecanismes" title="Les mécanismes de la colère">Les mécanismes de la colère</a></li>
		<li><a href="#que-faire" title="Que pouvons nous faire pour contrôler et surmonter notre colère ?">Que pouvons nous faire pour contrôler et surmonter notre colère ?</a></li>
		<li><a href="#conclusion" title="Conclusion">Conclusion</a></li>
		<li><a href="#references" title="Références">Références</a></li>
		<li><a href="#complementaire" title="Contenu complémentaire">Contenu complémentaire</a></li>
	</ul>
</nav>
<section id="pourquoi">
	<h1>Pourquoi nous mettons-nous en colère ?</h1>
	<p>
		La colère nous envoie le signal qu'un obstacle s'oppose à notre satisfaction et que nos désirs et nos besoins ne sont pas comblés. La <strong>frustration</strong> apparaît lorsque quelque chose ou quelqu'un nous empêche de faire ce que nous voulons faire ou nous empêche d'aller là où nous désirons aller. Nous ressentons alors de l'impuissance et nous avons l'impression de perdre le contrôle de la situation. Pour illustrer ceci, pensons à l'étudiant ou l'étudiante qui a un exposé oral à faire le lendemain, qui désire se coucher tôt afin d'être en forme mais dont le ou la colocataire a invité amis et amies pour fêter à l'appartement. Nous pouvons très bien l'imaginer sortir de sa chambre et enguirlander les gens présents.
	</p>
	<p>
		Nous cherchons tous à atteindre certains objectifs, certains standards. Toutefois, si des événements ou des personnes, incluant vous-même, vous empêchent de les atteindre, vous pouvez être déçu ou déçue. Cette <strong>déception</strong> peut alors se transformer en colère. L'athlète qui s'entraîne depuis des années et qui, lors d'une compétition, fait une contre-performance peut être furieux ou furieuse et réagir agressivement.
	</p>
	<p>
		Si notre <strong>sécurité</strong> est menacée, nous nous sentons vulnérables et nous risquons également de nous mettre en colère. Un étudiant ou une étudiante qui obtient un mauvais résultat à un examen peut craindre l'exclusion et peut rager contre le professeur ou la professeure qui a eu l'audace de lui présenter un tel examen.
	</p>
	<p>
		Parfois, c'est notre <strong>environnement</strong> immédiat qui nous met en colère ou nous occasionne de la frustration. Les devoirs et les responsabilités peuvent peser lourd sur vos épaules et vous pouvez alors vous fâcher contre le fardeau qui vous incombe et contre toutes les personnes et toutes les choses qui constituent ce fardeau. Aussi, certains <strong>états</strong> comme la tension et le stress peuvent contribuer à l'émergence d'une telle émotion.
	</p>
	<p>
		Le style de <strong>communication</strong> adopté peut également influencer l'apparition de la colère. Si quelqu'un adopte un ton hostile et s'il fait preuve d'un manque d'écoute, il y a fort à parier que la personne ressentira de la colère à son tour.
	</p>
	<p>
		La colère peut donc être causée par des événements internes comme externes. Les gens, les situations, les inquiétudes, les ruminations au sujet de difficultés personnelles, des souvenirs d'événements traumatisants ou exaspérants peuvent faire naître la colère.
	</p>
	<p>
		Vous avez sûrement déjà constaté qu'il y a des gens plus enclins que d'autres à la colère. Les gens qui n'ont pas appris à s'exprimer avec des mots ou qui ont appris à tout garder en dedans sont plus vulnérables à l'explosion de la colère. Les personnes colériques ont un faible seuil de tolérance à la frustration et sont portées à entretenir des attentes élevées envers elles-mêmes, les autres et la vie en général.
	</p>
	<p>
		Qu'est-ce qui rend les gens ainsi ? Une des causes pourrait être d'ordre génétique. Il y a des faits et des évidences qui démontrent que, dès la naissance, des enfants deviennent facilement irritables, susceptibles et exaspérés. Une autre cause pourrait être d'ordre culturel. La colère est souvent perçue et regardée négativement. Nous enseignons qu'il est correct d'exprimer de la tristesse, de l'anxiété et bien d'autres émotions, mais pas la colère. Nous sommes donc nombreux et nombreuses à ne pas avoir appris à la gérer et à la canaliser de manière constructive.
	</p>
</section>
<section id="mecanismes">
	<h1>Les mécanismes de la colère</h1>
	<p>
		Comme toute émotion, la colère est accompagnée de changements physiologiques et biologiques : le rythme cardiaque et la pression sanguine augmentent, tout comme le taux d'adrénaline qui envoie alors à notre cerveau le message qu'il y a menace et que nous devons réagir. Bien qu'il puisse s'agir d'une menace physique, le plus souvent c'est lorsque notre amour-propre ou notre dignité est menacée que nous réagissons avec colère. Le fait d'être traité injustement, d'être bafoué, ridiculisé ou humilié entraîne souvent de l'agressivité. S'ensuivent des ruminations qui, à leur tour, attisent notre colère. La colère se nourrit de la colère. On observe alors un <strong>processus d'escalade</strong> où une pensée en entraîne une autre, qui nous met davantage en furie et qui peut dégénérer rapidement en violence. Cette violence peut être refoulée et retournée contre soi ou elle peut exploser et être tournée contre les autres. Dans un cas comme dans l'autre, ces deux extrêmes peuvent entraîner des conséquences très fâcheuses.
	</p>
	<p>
		Ce phénomène d'escalade explique, entre autres, pourquoi un événement bénin peut provoquer chez une personne, déjà énervée et irritée, une réaction violente et disproportionnée.
	</p>
</section>
<section id="que-faire">
	<h1>Que pouvons nous faire pour contrôler et surmonter notre colère ?</h1>
	<p>
		Plus la tension monte, moins nous trouvons de moyens pour la relâcher et plus nous risquons d'atteindre un niveau de colère dangereux et difficile à contrôler. Nos pensées sont alors irrationnelles et négatives et il devient de plus en plus difficile de nous calmer. Il existe toutefois des façons de surmonter notre colère.
	</p>
	<h2>Reconnaître la colère et remettre en question notre façon de voir les choses</h2>
	<p>
		Lorsque ce sentiment vous assaille, vous devez, en premier lieu, en <strong>prendre conscience</strong>. La tension musculaire, l'augmentation du rythme cardiaque et de la respiration, la migraine, la fatigue, l'emploi d'un ton agressif, les gestes impulsifs sont des signes qui vous permettent de reconnaître la colère. Vous pouvez porter attention au moment où la colère fait son apparition, à sa durée et à son intensité.
	</p>
	<p>
		Prenez ensuite du <strong>recul</strong> vis-à-vis la situation et tentez d'identifier les pensées à la source de votre colère. Plus vous identifierez rapidement ces pensées, plus vous aurez de chances de mettre un frein à cet engrenage. Des pensées plus apaisantes permettent de réévaluer la situation à l'origine de la colère. Un automobiliste, par exemple, prend la place de stationnement qu'un autre automobiliste attendait déjà. Ce dernier sent la moutarde lui monter au nez et se dit que l'autre se moque de lui et qu'il se fout du fait qu'il était là avant lui. Il tente toutefois de se calmer en remettant en question son interprétation et en émettant l'hypothèse que l'autre automobiliste n'a peut-être pas vu son clignotant. Il se sent alors plus détendu et il arrive à lui dire calmement que cette place lui revient. La modification des pensées est une stratégie efficace lorsque le niveau de colère est modéré. Toutefois, si vous êtes en furie, cette stratégie ne vous sera guère utile puisque vous n'êtes plus en mesure de réfléchir normalement.
	</p>
	<h2>Se retirer et relaxer</h2>
	<p>
		Lorsque vous n'arrivez pas à couper court à l'enchaînement des pensées négatives, il est préférable de vous retirer, de vous distraire et de relaxer. Vous pouvez alors prendre une longue marche, faire de l'exercice physique, respirer profondément tout en utilisant une méthode de relaxation, aller au cinéma, faire de la lecture ou écouter la télévision. Ce type de distractions permet à la personne en colère d'interrompre ses pensées et de retrouver sa bonne humeur. Pensez-y, il est difficile de demeurer frustré ou frustrée lorsque nous faisons quelque chose d'agréable. Une fois la colère estompée, vous serez plus en mesure d'évaluer de façon posée la situation et de réagir de manière adéquate.
	</p>
	<h2>Communiquer de façon adéquate</h2>
	<p>
		Les gens en colère ont tendance à sauter aux conclusions et à agir en conformité avec celles-ci. La première chose à faire, si vous êtes dans une discussion très vive et très animée, est de vous calmer et de bien considérer vos réponses. Le bon vieux proverbe « il faut tourner sept fois sa langue dans sa bouche avant de parler » prend ici tout son sens. Il est préférable de prendre un certain recul et de songer sérieusement à ce que vous voulez dire. De plus, <strong>écoutez attentivement ce que l'autre vous dit et prenez votre temps avant de répondre</strong>. Il est tout à fait naturel d'être sur la défensive lorsque l'on vous critique, mais il est inutile de contre-attaquer. Vous ne feriez qu'alimenter votre propre colère et celle de l'autre.
	</p>
	<p>
		À l'inverse, il y a des gens qui n'expriment pas leur colère et n'avouent pas leur mécontentement. Ces gens confondent très souvent l'affirmation de soi et l'agressivité. <strong>L'affirmation de soi fait référence à l'habileté d'exprimer clairement et directement ses idées, ses sentiments, ses besoins et ses goûts tout en respectant ses droits et ceux des autres</strong>. Une jeune femme est frustrée car son copain sort tous les vendredis soirs avec ses amis. Elle n'ose pas lui dire car elle craint de lui déplaire. Cependant elle boude à son retour. Elle pourrait s'affirmer en lui disant que, bien qu'elle comprenne qu'il ait besoin de voir ses amis, elle se sent mise à l'écart et qu'elle aimerait qu'il passe un vendredi sur deux avec elle. En vous affirmant, vous évitez non seulement les conflits, mais vous courez davantage la chance d'obtenir ce que vous souhaitez.
	</p>
</section>
<section id="conclusion">
	<h1>Conclusion</h1>
	<p>
		Nous ne pouvons rayer la colère de notre vie, et ce n'est d'ailleurs pas souhaitable. Malgré tous nos efforts, des choses qui suscitent la colère continueront toujours de se produire. La vie sera toujours remplie de frustrations, de douleurs, de pertes et d'actions imprévisibles de la part des autres. Nous ne pouvons rien changer à cela, mais nous pouvons changer la façon dont nous laissons ces évènements nous atteindre. Contrôler nos réactions de colère peut nous permettre d'être plus heureux ou heureuse, à plus long terme.
	</p>
	<p>
		Si vous sentez que votre colère est vraiment hors de votre contrôle, si elle a un impact majeur sur vos relations interpersonnelles et sur les aspects importants de votre vie, vous devriez considérer l'idée de consulter afin d'apprendre à mieux la maîtriser. Différentes ressources existent (psychologue, professionnelle ou professionnel  de la santé mentale reconnu, groupes de soutien ou d'entraide) afin de vous aider à développer une gamme de techniques qui vous permettront de modifier votre façon de percevoir les choses et de vous comporter dans de telles situations.
	</p>
</section>
<section id="references">
	<h1>Références</h1>
	<p><cite>
		Fanget, F. (2000) Affirmez-vous ! Pour mieux vivre avec les autres, Paris, Éditions Odile Jacob.
	</cite></p>
	<p><cite>
		Goleman, D. (1997), L'intelligence émotionnelle, Paris, Robert Laffont.
	</cite></p>
	<p><cite>
		Lelord, F. et  André, C. (2001), La force des émotions, Paris, Éditions Odile Jacob.
	</cite></p>
</section>
<footer>
	<h2>Rédiger par :</h2>
	<p>
		Louise Careau, psychologue
	</p>
</footer>
