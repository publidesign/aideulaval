<p>
	Vous avez sûrement déjà entendu parler de la dépression, mais pourriez-vous la reconnaître si elle vous touchait ou si elle affectait quelqu’un de votre entourage ? Le texte qui suit vise à vous informer afin de vous aider à la reconnaître et à en comprendre les causes. De plus, quelques stratégies vous sont proposées afin de mieux vous outiller pour la surmonter ou pour aider une ou un proche à le faire.
</p>
<nav>
	<ul>
		<li><a href="#definition" title="Définition">Définition</a></li>
		<li><a href="#reconnaitre" title="Comment reconnaître les manifestations de la dépression ?">Comment reconnaître les manifestations de la dépression ?</a></li>
		<li><a href="#causes" title="Causes de la dépression">Causes de la dépression</a></li>
		<li><a href="#consequences" title="Conséquences de la dépression">Conséquences de la dépression</a></li>
		<li><a href="#traitements" title="Traitements de la dépression">Traitements de la dépression</a></li>
		<li><a href="#strategies" title="Stratégies pour surmonter un état dépressif">Stratégies pour surmonter un état dépressif</a></li>
		<li><a href="#prevenir" title="Comment prévenir la dépression ?">Comment prévenir la dépression ?</a></li>
		<li><a href="#aider" title="Comment aider un ami ?">Comment aider un ami ?</a></li>
		<li><a href="#conclusion" title="Conclusion">Conclusion</a></li>
		<li><a href="#references" title="Références">Références</a></li>
	</ul>
</nav>
<section id="definition">
	<h1>Définition</h1>
	<p>
		La dépression est l’un des troubles psychologiques les plus répandus dans la population générale. Les données indiquent qu’environ 10 % à 25 % des femmes et 5 % à 12 % des hommes risquent de faire une dépression majeure au cours de leur vie (DSM-IV, 1994). La dysthymie <sup>[<a id="foot_1" href="#dysthymie" title="dysthymie">1</a>]</sup>, le trouble bipolaire <sup>[<a id="foot_2" href="#bipolaire" title="dysthymie">2</a>]</sup> et la dépression saisonnière <sup>[<a id="foot_3" href="#depression-saisonniere" title="dépression saisonnière">3</a>]</sup> sont d’autres troubles de l’humeur, cependant le présent texte porte plus particulièrement sur la dépression.
	</p>
	<p>
		La dépression se traduit par une profonde tristesse, une perte d’intérêt marquée, ainsi qu’une certaine altération du fonctionnement général de la personne (p. ex. perte d’appétit, insomnie ou hypersomnie, ralentissement psychomoteur, difficultés de concentration). Également, la présence de pensées de mort peut être remarquée, ainsi que l’émergence de fausses croyances affectant l’estime personnelle (p. ex. dévalorisation, culpabilité excessive). 
	</p>
</section>
<section id="reconnaitre">
	<h1>Comment reconnaître les manifestations de la dépression ?</h1>
	<p>
		Une personne qui ressent de la tristesse ou un mal-être à un moment ou à un autre de sa vie ne souffre pas nécessairement de dépression. Un malaise peut survenir lorsque l’individu doit s’adapter à une nouvelle situation (p. ex. entrée à l’université), lorsqu’il est confronté à un choix important dans sa vie (p. ex. projet de carrière) ou encore lorsqu’il vit une perte ou une rupture amoureuse. Cette tristesse permet parfois à la personne de se questionner sur elle-même, ce qui peut l’amener par la suite à mieux se connaître et évoluer. Toutefois, si la tristesse persiste et que le fonctionnement général de la personne est affecté, il est possible que la dépression soit en train de s’installer. Il est donc important de reconnaître les principaux signes indiquant la présence d’une dépression.
	</p>
	<p>
		Parmi les symptômes qui devraient vous alerter, le premier consiste en un changement de votre façon d’être et de percevoir les événements extérieurs. En effet, les gens rapportent souvent ne plus se reconnaître dans leur façon d’agir et de penser. Quatre types de manifestations peuvent vous indiquer la présence d’un état dépressif. Il est à noter que ces symptômes doivent être présents depuis <strong>au moins deux semaines</strong> et qu’il en faut plus d’un pour diagnostiquer une dépression. De plus, la sévérité d’un état dépressif peut varier d’une personne à l’autre. Si vous reconnaissez plusieurs de ces symptômes, il est important d’y porter attention. Toutefois,  ne tirez pas de conclusion diagnostique car vous devez consulter un intervenant en santé mentale qui a l’expertise nécessaire (p. ex. psychologue, médecin) pour faire une évaluation plus approfondie.
	</p>
	<table>
		<caption>Manifestations d'un état dépressif</caption>
		<tbody>
			<tr>
				<th>Manifestations physiques et cognitives</th>
				<td>
					<ul>
						<li>
							Perte d’énergie
						</li>
						<li>
							Fatigue
						</li>
						<li>
							Ralentissement dans les activités
						</li>
						<li>
							Perte ou gain de poids
						</li>
						<li>
							Démarche lourde
						</li>
						<li>
							Insomnie ou hypersomnie
						</li>
						<li>
							Douleur (p. ex. maux de tête)
						</li>
						<li>
							Baisse de libido
						</li>
						<li>
							Difficulté de mémorisation
						</li>
						<li>
							Difficulté de concentration
						</li>
					</ul>
				</td>
			</tr>
			<tr>
				<th>Manifestations psychologiques</th>
				<td>
					<ul>
						<li>
							Vision négative de soi, de l’existence en général et de l’avenir (p. ex. « Rien de ce que je fais ne fonctionne, alors je ne vois pas comment cela pourrait fonctionner un jour. Je suis un raté…»)
						</li>
						<li>
							Difficulté à se projeter dans l’avenir (p. ex. « L’avenir est sans issue, pourquoi je ferais des études puisque je ne serai pas capable de me trouver un travail dans mon domaine »)
						</li>
						<li>
							Difficulté à prendre des décisions
						</li>
						<li>
							Crainte d’agir, de peur de se tromper 
						</li>
						<li>
							Pensées reliées à la mort (p. ex. « J’arrêterais de souffrir si j’étais mort. Je deviens sûrement un poids pour les autres, cela serait mieux pour tout le monde si je n’étais plus là ») 
						</li>
					</ul>
				</td>
			</tr>
			<tr>
				<th>Manifestations émotionnelles</th>
				<td>
					<ul>
						<li>
							Perte d’intérêt et de plaisir dans les activités
						</li>
						<li>
							Sentiment de tristesse constant
						</li>
						<li>
							Envie de pleurer fréquente
						</li>
						<li>
							Sentiment de culpabilité ou regret de ses choix (p. ex. « Pourquoi me    suis-je éloigné de mes proches pour étudier alors qu’ils ont besoin de moi. Si j’avais fait… »)
						</li>
						<li>
							Irritabilité
						</li>
					</ul>
				</td>
			</tr>
			<tr>
				<th>Manifestations comportementales</th>
				<td>
					<ul>
						<li>
							Difficulté à agir, à s’activer
						</li>
						<li>
							Difficulté à communiquer
						</li>
						<li>
							Repli sur soi
						</li>
						<li>
							Isolement
						</li>
					</ul>
				</td>
			</tr>
		</tbody>
	</table>
</section>
<section id="causes">
	<h1>Causes de la dépression</h1>
	<p>
		Plusieurs études ont été menées afin d’identifier la ou les causes possibles de la dépression. Jusqu’à maintenant, la littérature explique la dépression comme étant la résultante de l’interaction d’un ensemble de facteurs biologiques, psychologiques et sociaux.
	</p>
	<h2>Facteurs biologiques</h2>
	<p>
		Les chercheurs du domaine médical se penchent depuis plus de cinquante ans sur l’identification des causes biologiques possibles de la dépression. Les études ont porté surtout sur la dysfonction de la production de certains neurotransmetteurs (p. ex. la sérotonine et la noradrénaline). Une baisse du niveau de sérotonine serait responsable de l’irritabilité, alors qu’une faible quantité de noradrénaline serait liée au ralentissement psychique et psychomoteur que l’on remarque chez les personnes déprimées. Toutefois, il est à noter que le dysfonctionnement du système sérotoninergique ne serait pas spécifique à la dépression, puisqu’il est également associé à l’anxiété.
	</p>
	<p>
		En plus des recherches portant sur les neurotransmetteurs, le rôle de la vulnérabilité héréditaire est également étudié depuis plusieurs années. Les études ont démontré un lien entre le risque de développer une dépression et la présence de troubles de l’humeur dans la famille. Or, ces études n’ont pas mis en lumière une cause précise, car il est difficile d’identifier un gène responsable de l’ensemble des maladies psychiatriques, puisque d’autres facteurs sont liés à leur développement. Ainsi, ce n’est pas parce qu’un membre de votre famille vit ou a vécu un trouble de l’humeur que vous allez automatiquement présenter un tel trouble. La vulnérabilité héréditaire constitue un facteur prédisposant à la dépression et non un facteur la déclenchant.
	</p>
	<h2>Facteurs personnels</h2>
	<p>
		Plusieurs événements de la vie ont été identifiés comme favorisant l’apparition et le développement de la dépression. Une rupture amoureuse, une déception scolaire, la perte d’un emploi, des difficultés financières, le décès d’un proche et la maladie sont identifiés comme étant des facteurs déclencheurs. Toutefois, ces événements de la vie ne peuvent expliquer à eux seuls le développement d’un état dépressif, puisque deux individus peuvent réagir différemment devant ces mêmes événements. Ainsi, ce n’est pas spécifiquement l’événement qui entraîne l’état dépressif, mais plutôt ce qu’il représente pour la personne. C’est pourquoi les facteurs psychologiques doivent aussi être considérés dans l’explication d’un état dépressif.
	</p>
	<h2>Facteurs psychologiques</h2>
	<p>
		Tel que mentionné précédemment, certains événements peuvent fragiliser une personne sur le plan des émotions. Cependant, la perception que chacune ou chacun entretient de ces événements l’amènera à vivre diverses émotions. Chaque personne perçoit la réalité au travers d’un filtre qui la modifie ; celle vivant une dépression a tendance à entretenir un mode de pensée négatif et pessimiste face à elle-même et face à la vie en générale (p. ex. « Je ne suis pas intelligent »). Plus une personne entretient un discours négatif, plus elle risque de se maintenir dans un état dépressif.
	</p>
</section>
<section id="consequences">
	<h1>Conséquences de la dépression</h1>
	<h2>Au quotidien</h2>
	<p>
		La dépression ralentit la personne dans ses activités quotidiennes. Des actes simples tels que se laver, s’habiller, se préparer un repas semblent complexes étant donné le peu d’énergie dont elle dispose. Assister aux cours et étudier peut alors devenir une tâche extrêmement exigeante, voire parfois impossible. La personne, en se comparant à ce qu’elle était autrefois, risque de se déprécier encore davantage.
	</p>
	<h2>Dans les relations interpersonnelles</h2>
	<p>
		L’état dépressif entraîne un repli sur soi, car l’individu n’a plus envie de parler à son entourage, par peur de ne pas être compris, par honte ou par crainte d’être un poids pour les autres. De plus, retirant de moins en moins de plaisir à participer à des activités, la personne refuse peu à peu les invitations et s’isole de plus en plus. Ce comportement d’isolement entraîne souvent une incompréhension de la part de l’entourage et même des frustrations, car les proches peuvent se sentir délaissés et comprendre difficilement les motifs de cet éloignement. Lorsque  les relations interpersonnelles se compliquent, des conflits risquent d’émerger et ceux-ci peuvent être interprétés de façon erronée par la personne dépressive comme étant un rejet de son entourage.
	</p>
	<h2>Dans la vie de couple</h2>
	<p>
		La dépression entraîne parfois des difficultés au sein du couple car la ou le partenaire peut se questionner sur le comportement de l’autre. Il peut être difficile de comprendre pourquoi l’autre s’isole, manque d’énergie et vit une baisse de désir sexuel. D’une part, la ou le partenaire se demande si elle ou il a un rôle à jouer dans la détérioration de l’état psychologique de l’autre. D’autre part, elle ou il peut éprouver un sentiment d’impuissance, car malgré ses tentatives pour aider l’autre, la situation ne s’améliore pas. Également, certaines paroles peuvent renforcer la personne déprimée dans son sentiment d’incompréhension, voire d’abandon (p. ex. « Allez, secoue-toi un peu ; moi aussi j’ai déjà fait face à des échecs universitaires et j’ai survécu »).
	</p>
	<h2>Le suicide</h2>
	<p>
		Le suicide est malheureusement une conséquence possible de la dépression. Les études montrent que 45 % à 70 % des tentatives de suicide sont associées à une dépression et que 10% à 20 % des personnes déprimées meurent par suicide (Mirabel-Sarron, 2002). Ainsi, le risque de suicide pour la personne déprimée par rapport à celui d’un individu de la population générale est 30 fois plus élevé. Afin d’en connaître davantage à ce sujet, vous pouvez consulter la section <a href="{{LIEN}}prevention-suicide" title="Prévention du suicide" target="_blank"></a>Prévention du suicide.
	</p>
</section>
<section id="traitements">
	<h1>Traitements de la dépression</h1>
	<p>
		Il est important de traiter la dépression pour ne pas qu’elle s’aggrave. Il existe divers traitements psychologiques et pharmacologiques pour la dépression. Parmi ceux existants, il est démontré qu’un traitement pharmacologique combiné à un traitement psychologique présente une meilleure efficacité (Gotlib et Hammen, 2002).
	</p>
	<h2>Traitements psychologiques</h2>
	<p>
		Depuis plusieurs années, l’importance et l’efficacité des traitements psychologiques de la dépression sont étudiées (Barlow, 1993 ; Cuijpers et al., 2005). Divers types de psychothérapies existent dans le traitement de la dépression, comme la thérapie humaniste existentielle, psychodynamique, psychothérapie brève ou cognitivo-comportementale. Cette dernière fût l’objet de plusieurs études empiriques qui ont démontré son efficacité (Barlow, 1993 ; Scott, 2001). Elle vise principalement à modifier les pensées négatives présentes lors de la dépression et favorise le retour à un niveau d’activité qui convient à la personne.
	</p>
	<h2>Traitements pharmacologiques</h2>
	<p>
		L’efficacité des traitements pharmacologiques dans la dépression a été largement démontrée. Les médicaments de types antidépresseurs sont apparus à la fin des années 1950. Aujourd’hui, ils ont moins d’effets secondaires qu’auparavant, mais il peut s’écouler quelques semaines avant de noter une amélioration significative de l’humeur. Les antidépresseurs agissent sur la chimie du cerveau, notamment en corrigeant les taux de sérotonine et de noradrénaline. La médication permet de retrouver en quelques semaines le sommeil, l’appétit, un regain d’énergie, le plaisir, une meilleure concentration et de diminuer l’anxiété (Gotlib et Hammen, 2002).
	</p>
</section>
<section id="strategies">
	<h1>Stratégies pour surmonter un état dépressif</h1>
	<p>
		Briser l’isolement en parlant à quelqu’un de ce que vous vivez constitue un pas vers la guérison, cependant aller chercher de l’aide auprès d’une personne-ressource spécialisée comme un psychologue ou un médecin est très important. Si vous soulagez les symptômes de la dépression, votre humeur s’améliorera et vous retrouverez vos capacités habituelles. Voici donc quelques stratégies pour vous aider :
	</p>
	<h2>Agir sur les symptômes physiques</h2>
	<ul>
		<li>
			Consultez un <strong>médecin</strong> afin d’évaluer si la fatigue que vous ressentez est attribuable à un problème de santé physique (p. ex. hypothyroïdie).
		</li>
		<li>
			Une <strong>médication</strong> peut aussi s’avérer efficace si votre fonctionnement général est grandement altéré.
		</li>
		<li>
			Diminuez votre charge de travail car vous avez besoin de <strong>repos</strong>.
		</li>
		<li>
			Faites de <strong>l’exercice</strong>. En effet, des études indiquent que l’exercice physique permet de diminuer certains symptômes dépressifs comme la perte d’appétit et la dévalorisation. Une courte période de quinze à vingt-cinq minutes d’exercices modérés peut être suffisante pour apporter un changement. Il est démontré que l’exercice physique augmente le taux de sérotonine, un neurotransmetteur impliqué dans la dépression.
		</li>
		<li>
			Faites preuve de patience et de tolérance envers vous-même et donnez-vous du <strong>temps</strong> pour retrouver votre énergie.
		</li>
		<h2>Agir sur les comportements</h2>
		<ul>
			<li>
				Fixez-vous de petits <strong>objectifs</strong> (p. ex. prendre un repas complet, sortir prendre l’air dix minutes dans la journée, appeler un ami une fois par semaine).
			</li>
			<li>
				<strong>Notez</strong> à chaque jour les activités que vous avez faites afin de prendre conscience de vos progrès et d’augmenter votre motivation à l’action.
			</li>
			<li>
				<strong>Reprenez progressivement vos activités</strong> et n’essayez pas de comparer votre efficacité actuelle à celle que vous aviez antérieurement. Vous n’avez pas le même niveau d’énergie.
			</li>
			<li>
				Ajoutez à votre horaire des activités qui peuvent vous procurer du <strong>plaisir</strong>. De petites choses toutes simples comme allez vous promener, cuisiner un bon repas, lire, faire du sport, etc. L’augmentation d’activités plaisantes contribue à diminuer les pensées négatives. 
			</li>
		</ul>
	</ul>
	<h2>Combattre ses pensées négatives pour améliorer ses émotions</h2>
	<p>
		Comme il est mentionné à la section portant sur les facteurs psychologiques, vos pensées influencent vos émotions. Une personne en dépression entretient une perception plus négative d’elle-même, du monde extérieur, ainsi que des événements auxquels elle est confrontée. Cette interprétation négative de la réalité maintient l’humeur dépressive et la détériore. Il est donc important d’apprendre à modifier son discours intérieur par des pensées plus constructives qui représentent plus fidèlement la réalité. Voici brièvement comment agir sur vos pensées (pour plus de détails, vous pouvez consulter la section <a href="{{LIEN}}perceptions" title="Stress, anxiété et perceptions : maîtriser les pensées toxiques" target="_blank">Stress, anxiété et perceptions : maîtriser les pensées toxiques</a> !) :
	</p>
	<ul>
		<li>
			Identifiez <strong>l’émotion</strong> (p. ex. tristesse, colère, anxiété
		</li>
		<li>
			Identifiez le <strong>contexte</strong> dans lequel elle est apparue (p. ex. « Je me sens triste parce que mon ami ne peut pas faire quelque chose avec moi ce soir »).
		</li>
		<li>
			Notez les <strong>pensées</strong> qui vous sont venues à l’esprit suite à cet événement (p. ex. « Je ne compte pour personne… Dans le fond, ils ont raison, je suis un raté »). Ces pensées ne reflètent pas la réalité, mais bien l’image que vous vous en faites. Elles vous semblent peut-être réalistes, mais il est important de les analyser attentivement. En effet, il peut être normal d’être déçu de passer une soirée seul, mais croire que vous n’êtes pas une personne aimable et vous dénigrer au point d’avoir l’impression d’être un raté est sûrement une conclusion erronée.
		</li>
		<li>
			<strong>Vérifiez</strong> si vos pensées correspondent à la réalité en évaluant les faits qui confirment vos pensées erronées et ceux qui vont à l’encontre de celles-ci (p. ex. « C’est vrai que personne ne m’aime ? », « Mon ami m’a dit non pour ce soir, mais est-ce que ça signifie réellement que je ne compte pas pour lui ? », « Si quelqu’un d’autre était dans ma situation, qu’est ce qu’il pourrait penser ? Qu’est-ce que j’aurais pensé il y a quelques années dans la même situation ? »).
		</li>
		<li>
			Trouvez une <strong>autre explication</strong> possible afin d’évaluer la situation de façon plus réaliste (p. ex. « Même si mon ami m’a dit non pour ce soir, ça ne veut pas dire qu’il ne m’aime pas. Il est très occupé et ça n’a rien à voir avec ses sentiments envers moi »).
		</li>
	</ul>
	<h2>Diminuer son sentiment de culpabilité</h2>
	<p>
		Le sentiment de culpabilité est un élément à travailler lors de la dépression, car il est fréquent que la personne déprimée maintienne un tel sentiment. L’entretenir ne fait que nuire à l’amélioration de l’humeur.
	</p>
	<p>
		Afin de vous aider à diminuer votre sentiment de culpabilité, vous pouvez vous demander si votre sentiment constitue un <strong>regret raisonnable face à la situation</strong> ou s’il est exagéré. Pour ce faire, vous pouvez vous demander si la situation est réellement aussi grave que vous le croyez. Aussi, interrogez-vous sur ce que vous diriez à quelqu’un qui se trouverait dans la même situation. Vous serez ainsi en mesure de mieux déterminer si votre sentiment de culpabilité est exagéré.
	</p>
	<p>
		Également, l’élimination de toutes les <strong>obligations fictives</strong> (p. ex. « Je devrais… ») s’avère un moyen efficace pour diminuer le sentiment de culpabilité. Voici quelques petits trucs pour enlever les « je devrais » de votre vocabulaire :
	</p>
	<ul>
		<li>
			Qui dit cela ? Où est-ce écrit ? Quels sont les avantages et les inconvénients de penser ainsi ?
		</li>
		<li>
			Remplacez les « je devrais » par d’autres mots (p. ex. « Il serait bien agréable… », « J’aimerais… », « Je veux… »).
		</li>
		<li>
			Prenez conscience que le « je devrais » ne correspond pas à la réalité, mais plutôt à une habitude de vous imposer des règles et des obligations.
		</li>
		<li>
			Identifiez vos limites et apprenez à dire <strong>non</strong>.
		</li>
		<li>
			<strong>Vous pouvez tout simplement vous demander : « Pourquoi devrais-je m’en vouloir autant ? »</strong>
		</li>
	</ul>
	<h2>Améliorer son estime personnelle</h2>
	<p>
		Lors d’une dépression, l’estime personnelle de l’individu peut être affectée. Un travail doit être fait afin de l’améliorer et de reprendre confiance en soi. Pour en savoir davantage à ce sujet, vous pouvez lire le texte intitulé <a href="{{LIEN}}estime-de-soi" title="L’estime de soi" target="_blank">L’estime de soi</a>.
	</p>
	<h2>Rechercher les exceptions</h2>
	<p>
		Essayez d’identifier les moments où vous ne vous sentez pas déprimée ou déprimé, ou encore les périodes de votre vie où vous vous sentiez bien. Une fois ces moments identifiés, questionnez-vous sur ce qui les caractérise (p. ex. Étiez-vous avec quelqu’un en particulier ? Faisiez-vous quelque chose de spécial ? Aviez-vous plus confiance en vous ?). Si vous arrivez à identifier les éléments qui sont associés aux moments un peu plus heureux, vous pouvez essayer soit de les insérer à nouveau dans votre quotidien ou de faire en sorte de recréer cet état de mieux-être dans votre vie actuelle (p. ex. « Je me sentais bien quand j’allais marcher dans le bois » ; ici, la personne peut essayer de s’accorder du temps pour aller dehors même s’il n’y a pas de bois à proximité).
	</p>
	<h2>Chercher l'origine de son état dépressif</h2>
	<p>
		Il peut aussi être de mise de se questionner sur l’origine de l’état dépressif. En effet, la dépression est souvent un signal d’alarme. Elle vous indique que quelque chose ne va pas (p. ex. trouble d’anxiété, difficulté dans les relations interpersonnelles, difficulté à faire face à un traumatisme, deuil, horaire de travail trop chargé, difficulté à mettre ses limites). Connaître l’origine de votre état dépressif ne permet pas de surmonter entièrement le problème, mais permet de cibler quelques pistes d’intervention dans le but d’un traitement psychologique.
	</p>
</section>
<section id="prevenir">
	<h1>Comment prévenir la dépression ?</h1>
	<ul>
		<li>
			Maintenez un équilibre de vie (p. ex. gérez le stress, planifiez des horaires de travail adéquats et des moments de détente, évitez le surmenage).
		</li>
		<li>
			Faites de l’exercice
		</li>
		<li>
			Développez vos habiletés sociales afin de vous affirmer de manière plus adéquate.
		</li>
		<li>
			Améliorez votre estime personnelle.
		</li>
		<li>
			Exposez-vous suffisamment à la lumière du jour (surtout pour les personnes susceptibles de souffrir de dépression saisonnière).
		</li>
		<li>
			Allez chercher de l’aide sans hésitation.
		</li>
	</ul>
</section>
<section id="aider">
	<h1>Comment aider un ami ?</h1>
	<ul>
		<li>
			Offrez une écoute attentive.
		</li>
		<li>
			Renseignez-vous davantage afin de mieux comprendre la personne qui souffre de dépression.
		</li>
		<li>
			Démontrez que vous comprenez les difficultés causées par la dépression.
		</li>
		<li>
			Ne fournissez pas d’effort exagéré pour planifier une foule d’activités auxquelles votre amie ou ami n’aura pas l’énergie de participer. Proposez-lui plutôt des sorties toutes simples, sans lui mettre de pression.
		</li>
		<li>
			N’essayez pas de lui remonter le moral avec des phrases clichées (p. ex. « Ça pourrait être pire si… »).
		</li>
		<li>
			Ne laissez pas entendre que la situation pourrait s’améliorer si elle ou il faisait plus d’efforts ; votre amie, ami est sensible à la critique en ce moment.
		</li>
		<li>
			Soyez patiente, patient.
		</li>
		<li>
			Guidez votre amie ou ami vers la personne-ressource appropriée.
		</li>
	</ul>
</section>
<section id="conclusion">
	<h1>Conclusion</h1>
	<p>
		En somme, la dépression constitue un trouble psychologique répandu dont l’apparition  s’explique par un ensemble de facteurs. Il importe de reconnaître les signaux d’alarme et d’aller chercher de l’aide sans avoir honte. Parlez à une personne attentive qui saura vous écouter et vous comprendre. Souvenez-vous que l’isolement ne constitue pas une solution réelle même s’il semble moins menaçant de prime abord. Il est très important de communiquer afin d’éviter des difficultés interpersonnelles ou de l’incompréhension de la part de votre entourage. N’oubliez pas de vous faire plaisir puisque ces moments de bonheur vous seront nécessaires pour retrouver goût à la vie. De plus, la médication peut être une alliée fort efficace. Enfin, souvenez-vous que vous n’êtes pas seule ou seul à vivre ce genre de difficulté et qu’il est possible de la surmonter. Elle peut constituer un système d’alarme et vous permettre d’identifier et de travailler certains aspects de vous-même. Vous pourrez alors développer vos ressources personnelles, ce qui favorisera davantage le maintien d’un meilleur équilibre, un peu comme une personne qui décide de faire de l’exercice et de changer son alimentation après avoir souffert d’un problème de santé physique.
	</p>
</section>
<section id="references">
	<h1>Références</h1>
	<p><cite>
		Barlow, David H. (1993), Clinical Handbook of Psychological Disorders, 2e édition, New Yord, Guilford Press.
	</cite></p>
	<p><cite>
		Burns, David D. (1980), Être bien dans sa peau, Saint-Lambert, Les éditions Héritage.
	</cite></p>
	<p><cite>
		Cuijpers, P., P.A. Van Lier, A. Van Straten et M. Donker (2005), « Examining differential effects of psychological treatment of depressive disorder : An application of trajectory analyses », Journal of Affective Disorders, vol. 89, p. 137-146.
	</cite></p>
	<p><cite>
		DSM-IV : American Pschiatric Association (1994), Diagnostic and Statistical Manual of Mental Disorders : DSM-IV, 4e édition, Washington, American Psychiatric Association.
	</cite></p>
	<p><cite>
		Gotlib, Ian H., et Constance L. Hammen (2002), Handbook of Depression, New York, Guilford Press.
	</cite></p>
	<p><cite>
		Mirabel-Sarron, Christine (2002), La dépression comment en sortir, Paris, Les éditions Odile Jacob
	</cite></p>
	<p><cite>
		Scott, J. (2001), « Cognitive therapy for depression », British Medical Bulletin, vol. 57, p.101‑113
	</cite></p>
</section>
<hr>
<section id="footnotes">
	<ul>
		<li id="dysthymie">
			<sup><a href="#foot_1" title="Retour au texte">[1]</a></sup> La <strong>dysthymie</strong> se traduit par une humeur dépressive constante présente pendant au moins deux ans. Les symptômes dépressifs sont moins sévères que dans le cas d’une dépression majeure, mais ils sont persistants.
		</li>
		<li id="bipolaire">
			<sup><a href="#foot_2" title="Retour au texte">[2]</a></sup> Le <strong>trouble bipolaire</strong> se caractérise par des variations marquées de l’humeur (une phase d’euphorie extrême peut être suivie d’une phase dépressive importante).
		</li>
		<li id="depression-saisonniere">
			<sup><a href="#foot_3" title="Retour au texte">[3]</a></sup> La <strong>dépression saisonnière</strong> comporte les mêmes symptômes que la dépression majeure, mais elle survient lors du changement de saison. La diminution de la lumière qui survient à l’automne entraîne une augmentation de la mélatonine, qui provoquerait un état dépressif. Il est à noter qu’une telle dépression est plus sévère que le simple <em>blues hivernal</em>.
		</li>
	</ul>
</section>
<footer>
	<h2>Rédiger par :</h2>
	<p>
		Chantal Thibodeau, psychologue
	</p>
</footer>
