<p>
	Parvenus à l’étape des études universitaires, plusieurs jeunes adultes choisissent de vivre en appartement. Certains étudiants le font pour se rapprocher de l’université, d’autres cherchent à vivre une nouvelle expérience et veulent accéder à une plus grande autonomie. En général, les ressources financières des étudiants sont plutôt restreintes et les dépenses reliées aux études sont élevées. Afin de boucler leur budget, une majorité d’étudiants se tourne vers la colocation. Ils emménagent alors avec des amis, un partenaire amoureux ou d’autres étudiants qu’ils ne connaissaient pas avant la cohabitation.
</p>
<p>
	La vie en colocation constitue une expérience riche et singulière. Deux ou plusieurs personnes, qui ne sont pas nécessairement amies, partagent un espace et une intimité au quotidien pour une durée à la fois prolongée et limitée (Pastinelli, 2003). Elles s’engagent l’une face à l’autre sans se projeter dans un avenir commun. Elles mènent des vies en parallèle tout en demeurant libres et indépendantes dans leurs sorties, leurs relations et leurs activités. Cette expérience représente une belle occasion de mieux se connaître et de découvrir d’autres personnes. Elle favorise aussi l’émergence d’aptitudes concrètes, comme savoir cuisiner, gérer un budget ou entretenir un lieu, qui serviront pour le reste de la vie. Elle permet aussi de développer des qualités personnelles utiles pour la vie future comme l’affirmation de soi, la tolérance, le partage de biens et d’espaces, l’autonomie et la responsabilité. En somme, le choix de vivre en colocation présente des avantages, comme le fait d’économiser et d’éviter la solitude, ce qui est tout à fait approprié pour la période des études universitaires. Et comme les études préparent à la vie professionnelle, la colocation est une expérience qui prépare à la vie adulte. 
</p>
<nav>
	<ul>
		<li><a href="#irritants" title="Des irritants possibles">Des irritants possibles</a></li>
		<li><a href="#quoi-faire" title="Quoi faire?">Quoi faire?</a></li>
		<li><a href="#bien-choisir" title="Bien choisir son colocataire">Bien choisir son colocataire</a></li>
		<li><a href="#savoir-communiquer" title="Savoir communiquer">Savoir communiquer</a></li>
		<li><a href="#ententes" title="Établir des ententes écrites">Établir des ententes écrites</a></li>
		<li><a href="#conclusion" title="Conclusion">Conclusion</a></li>
		<li><a href="#annexe" title="Annexes">Annexes</a></li>
		<li><a href="#references" title="Références">Références</a></li>
	</ul>
</nav>
<section id="irritants">
	<h1>Des irritants possibles</h1>
	<p>
		La gestion de la vie en appartement comporte des tâches, des responsabilités, des contraintes et du plaisir. Pour les étudiants qui vivent une première expérience de longue durée hors du foyer familial, c’est un défi stimulant. Le fait d’avoir des colocataires pour partager ces obligations est, le plus souvent, un facteur aidant. Cependant, quand des divergences dans les comportements ou les perceptions sur ces tâches et responsabilités apparaissent, l’harmonie entre colocataires peut être minée et le confort de la vie en appartement, affecté.
	</p>
	<p>
		Certains des problèmes fréquemment rapportés par les étudiants qui vivent en colocation peuvent paraître anodins. Mais le fait de vivre au quotidien dans un conflit, une insatisfaction ou une situation perçue comme injuste peut accaparer les pensées et devenir significativement incommodant. En voici quelques exemples :
	</p>
	<ul>
		<li>
			Tous les matins avant de déjeuner, vous découvrez une table et un comptoir remplis de résidus alimentaires laissés par vos colocataires qui les ont utilisés la veille et vous devez ramasser et nettoyer la vaisselle des autres avant de pouvoir préparer votre propre repas.
		</li>
		<li>
			En début de bail, vous aviez prévu une vie à deux dans l’appartement soigneusement choisi avec votre amie de longue date. Mais, graduellement, vous avez eu à partager l’espace et la nourriture ou à attendre pour utiliser la salle de bain en raison de la présence imprévue et envahissante du copain de votre amie, alors que ce scénario n’avait pas été prévu au départ.
		</li>
		<li>
			Vous devez composer avec les petites habitudes d’un nouveau colocataire qui regarde ses émissions de télévision tard en soirée ou écoute sa musique dans le salon voisin de votre chambre, alors que vous avez étudié toute la journée en vue d’un examen prochain. Vous tentez de trouver le sommeil alors que votre lit vibre au son du heavy metal.
		</li>
		<li>
			Fatigué et affamé, vous ouvrez la porte du frigo et n’y trouvez pas vos œufs, votre reste de pâté chinois a disparu ou votre bière a été bue. Vous cherchez votre raquette de squash et la retrouvez dans le placard de votre colocataire, hors de son étui protecteur. Ces emprunts non autorisés sont fréquents et vous déplaisent, mais vous ne voulez pas paraître capricieux auprès de vos colocataires en vous en plaignant.
		</li>
		<li>
			Vous avez remarqué que les factures ont tendance à traîner. Alors, pour éviter des frais de retard, vous prenez en charge le paiement du compte de téléphone et de la connexion internet. Chaque mois, vous devez rappeler à répétition à vos colocataires le montant qu’ils vous doivent. Ils sont pourtant bien enclins à profiter de la téléphonie. Coincé, vous voulez à la fois éviter de payer des frais de retard aux fournisseurs de service, éviter de récompenser le laxisme de vos colocataires et éviter de paraître pointilleux.
		</li>
	</ul>
</section>
<section id="quoi-faire">
	<h1>Quoi faire?</h1>
	<p>
		Pour les problèmes d’appartement qui concernent le propriétaire de l’immeuble (bris et réparations, paiement du loyer) ou les voisins (bruit excessif), les locataires ont des recours, comme la Régie du logement. De plus, ils ont comme base d’entente un contrat : le bail. Mais pour ce qui concerne la vie à l’intérieur de l’appartement et les rapports entre colocataires, il n’y a pas de ressource officielle. De plus, les colocataires n’ont pas tendance à établir des contrats ou des ententes explicites. En effet, selon Pastinelli (2003), un élément à la base de la vie en colocation serait le respect des libertés individuelles. Les colocataires préfèrent souvent éviter les contraintes et les règles qui pourraient restreindre leur latitude. Il préfère maintenir une indépendance jugée nécessaire pour une vie en groupe sans accrochage. Ils ont en début de bail quelques brèves discussions sur le partage des frais, mais les autres aspects de la vie en colocation sont le plus souvent laissés en plan. En cours de route, quand il y a des insatisfactions, celles-ci ne sont pas évoquées explicitement et les protagonistes attendent la fin du bail dans le silence et la frustration. Est-ce la meilleure façon de réagir? Serait-il possible de gérer ces situations plus efficacement, ou même de les prévenir?
	</p>
</section>
<section id="bien-choisir">
	<h1>Bien choisir son colocataire</h1>
	<p>
		Faut-il emménager avec un ami ou avec un étranger? Souvent, il est rapporté qu’il est préférable de choisir une personne qui ne fait pas partie de son cercle d’amis intimes. Des personnes qui ont établi un lien depuis des années peuvent être surprises de découvrir, en habitant ensemble, d’autres aspects de la personnalité de leur ami qu’ils n’avaient jamais perçus auparavant. Certains sont plus à l’aise de partager une critique avec un ami proche, d’autres avec un étranger. L’important en colocation est la communication et le respect de l’autre, qu’il s’agisse d’un vieil ami, d’un membre de sa famille ou d’un nouveau camarade. Les liens peuvent s’enrichir ou s’effriter devant de nouvelles expériences. Ce qui fait la différence, ce sont les attitudes personnelles et non la nature du lien. Être capable d’émettre une demande ou une critique de façon respectueuse, d’en recevoir une et d’ajuster ses comportements par la suite sont des attitudes qui favorisent la bonne entente ou la résolution de conflit en appartement.
	</p>
</section>
<section id="savoir-communiquer">
	<h1>Savoir communiquer</h1>
	<p>
		Vivre en colocation requiert donc la qualité de savoir penser aux autres, ce qui consiste à prévoir l’impact de ses comportements sur leur bien-être. Il s’agit de trouver un équilibre entre la préservation de son espace personnel et de sa liberté, tout en favorisant l’harmonie dans la collectivité. Des outils essentiels pour y arriver sont la tolérance, la communication et la prévention. Concrètement, quand des étudiants envisagent de vivre ensemble en colocation, leur discussion devrait dépasser le choix d’un quartier, du type d’appartement ou d’une date d’emménagement. Ils pourraient discuter aussi de la façon de vivre ensemble afin d’ajuster leurs visions et de voir si elles sont compatibles. L’idéal est donc de prévenir d’éventuelles divergences en discutant avant d’emménager ensemble. Il peut aussi être pertinent de le faire au début du bail ou à tout moment en cours de route.
	</p>
	<p>
		Un exercice de discussion moins courant, mais très efficace peut être effectué lors d’une rencontre de colocataires. Il s’agit de l’exercice de « la zone non négociable ». Chacun des colocataires est invité à identifier des éléments concernant la vie à l’appartement qui lui sont impératifs et qu’il n’a pas à justifier auprès des autres. Par exemple, un colocataire signale sa fermeture quant à l’adoption d’un chat et les autres s’engagent à respecter cette limite. Chacun des colocataires peut ainsi identifier un ou quelques éléments qui composent sa « zone non négociable ». Des idées pour cet exercice : la cigarette et la fumée dans l’appartement, les emprunts de vêtements ou d’autres objets, les visiteurs occasionnels ou réguliers ou la gestion des tâches ménagères.
	</p>
</section>
<section id="ententes">
	<h1>Établir des ententes écrites</h1>
	<p>
		« Les paroles s’envolent, mais les écrits restent! » Les discussions ouvertes sont un bon point de départ, mais parfois elles ne sont pas suffisantes et des mois après une entente, des oublis ou autres changements peuvent survenir. Afin d’ouvrir la discussion sur différents thèmes inhérents à la vie en colocation, l’utilisation des documents « <a href="wp-content/uploads/2014/11/Gestion_Depenses.docx" title="grille de gestion des dépenses" target="_blank">grille de gestion des dépenses</a> », « <a href="wp-content/uploads/2014/11/Convention_Colocataires.docx" title="répartition des tâches" target="_blank">répartition des tâches</a> » et « <a href="wp-content/uploads/2014/11/Convention_Colocataires.docx" title="convention de colocataires" target="_blank">convention de colocataires</a> » est tout indiquée. Ces documents peuvent être utilisés comme des contrats entre colocataires.
	</p>
</section>
<section id="conclusion">
	<h1>Conclusion</h1>
	<p>
		Se loger convenablement est l’un des besoins fondamentaux de l’être humain. Que ce soit une maison ou un appartement, l’endroit où une personne habite doit constituer un refuge pour elle, un espace où elle peut se protéger du monde extérieur, se reposer et se ressourcer. Quand le milieu même du logement, censé être un havre de paix, devient une source d’insécurité, le bien-être général d’une personne peut être compromis et son fonctionnement général peut en être affecté. Il est donc important de prévenir d’éventuels problèmes ou de traiter ceux qui sont en cours afin de préserver l’équilibre de la personne et réussir le projet qui l’a conduite à emménager en appartement : son projet d’étude.
	</p>
</section>
<section id="annexe">
	<h1>Annexes</h1>
	<p>
		Grille : <a href="wp-content/uploads/2014/11/Gestion_Depenses.docx" title="Gestion des dépenses de l’appartement" target="_blank">Gestion des dépenses de l’appartement</a>
	</p>
	<p>
		Grille : <a href="wp-content/uploads/2014/11/Convention_Colocataires.docx" title="Répartition des tâches" target="_blank">Répartition des tâches</a>
	</p>
	<p>
		<a href="wp-content/uploads/2014/11/Convention_Colocataires.docx" title="Convention de colocataires" target="_blank">Convention de colocataires</a>
	</p>
</section>
<section id="references">
	<h1>Références</h1>
	<p><cite>
		Pastinelli, Madeleine, Seul et avec l’autre, PUL, 2003.
	</cite></p>
	<p><cite>
		Régie du logement : www.rdl.gouv.qc.ca
	</cite></p>
</section>
<footer>
	<h2>Rédiger par :</h2>
	<p>
		Marcel Bernier, psychologue<br>
		<i>(Merci à Mylène Bussières, psychologue, pour la suggestion de l’exercice « la zone non négociable ».)</i>
	</p>
</footer>
