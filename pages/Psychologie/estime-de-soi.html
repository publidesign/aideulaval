<p>
	On définit globalement l'estime de soi comme une évaluation positive, c'est-à-dire lorsque quelqu'un est satisfait des aspects de sa personne impliqués dans une situation. Notre représentation de nous-mêmes correspond à nos attentes et au rôle que nous souhaiterions jouer dans une situation précise.
</p>
<p>
	L'estime de soi semble avant tout une question d'attitude. Nous le sentons lorsque nous adoptons la bonne attitude : c'est valorisant. C'est une question émotionnelle. C'est aussi une question universelle. Même les gens qui semblent avoir eu la vie facile ont cette tâche d'apprendre à travailler pour développer une meilleure attitude. La documentation à ce sujet rappelle fréquemment que notre valeur ne dépend pas de ce qu'on fait ou de ce qu'on a, mais bien d'aimer ce qu'on est.
</p>
<p>
	Une telle démarche devient plus concrète si on se donne comme but d'améliorer son sentiment d'être une personne valable. Une personne dont l'estime de soi est élevée investit dans elle-même. Vous pouvez y arriver en approfondissant votre conscience de vos besoins et de vos aspirations, ainsi que de vos forces et de vos limites. En vous les appropriant. En vous accommodant, en toute honnêteté, de vos limites et de ce que vous ne pouvez changer. En ne vous prenant pas trop au sérieux. En admettant vos erreurs dans votre vision de vous-même, des autres et du monde. Cela favorise un choix de comportements qui entraîneront de meilleures conséquences lorsque que vous devrez affronter des situations difficiles. Si on vous demande une faveur, vous ne vous sentirez pas obligé ou obligée de répondre systématiquement oui ou systématiquement non. Vous choisirez librement une réponse en fonction de vos goûts, besoins, désirs et limites du moment. Il en découle une projection positive et réaliste de soi face à l'avenir.
</p>
<p>
	Ce texte vise à vous fournir ou à rafraîchir des notions utiles pour vous aider à travailler par vous-même au développement de votre estime.
</p>
<nav>
	<ul>
		<li><a href="#extreme" title="Éviter les évaluations extrêmes">Éviter les évaluations extrêmes</a></li>
		<li><a href="#pensees-automatiques" title="Reconnaître ses pensées automatiques...">Reconnaître ses pensées automatiques...</a></li>
		<li><a href="#affronter" title="S'affronter dans le changement plutôt que de s'éviter">S'affronter dans le changement plutôt que de s'éviter</a></li>
		<li><a href="#communication" title="Développer ses compétences de communication">Développer ses compétences de communication</a></li>
		<li><a href="#democratie" title="Faire montre de constance et de démocratie...">Faire montre de constance et de démocratie...</a></li>
		<li><a href="#conclusion" title="Conclusion">Conclusion</a></li>
		<li><a href="#references" title="Références">Références</a></li>
	</ul>
</nav>
<section id="extreme">
	<h1>Éviter les évaluations extrêmes (sous-évaluation, surévaluation de soi) et l'inertie.</h1>
	<p>
		On observe des conséquences néfastes liées au fait de se fixer dans une attitude extrême. Une personne trop sûre d'elle, sans marge de doute, risque la rigidité. Les autres n'aiment pas ce comportement et s'éloigneront. Une personne trop anxieuse déploie une telle énergie à se soucier du regard des autres, qu'il pourrait ne plus lui en rester pour se comporter de manière intéressante et stimulante. En outre, elle risque de se laisser facilement impressionner par ceux qui se donnent de l'importance, et risque aussi de perdre sa concentration et son efficacité à s'adapter.
	</p>
	<p>
		Comportements reliés à une surévaluation de soi :
	</p>
	<ul>
		<li>
			se considérer supérieur, supérieure aux autres ; 
		</li>
		<li>
			recevoir la critique de manière catastrophique ;
		</li>
		<li>
			avoir peu de façons de tenir compte des autres ; 
		</li>
		<li>
			refuser d'être comme tout le monde ; 
		</li>
		<li>
			confondre « avoir plus », « être plus », avec « valoir plus ».
		</li>
	</ul>
	<p>
		Comportements reliés à une sous-évaluation de soi :
	</p>
	<ul>
		<li>
			filtrer négativement ce que les autres vous disent ou vous font  ;
		</li>
		<li>
			s'attendre systématiquement à ne pas être aimé ou aimée ; 
		</li>
		<li>
			rester sur la défensive avec les autres ;
		</li>
		<li>
			résister à l'idée de partager le contrôle des choses avec son entourage.
		</li>
	</ul>
</section>
<section id="pensees-automatiques">
	<h1>Reconnaître ses pensées automatiques et gérer son « feed-back » interne et son « feed-back » externe</h1>
	<p>
		Les processus autoévaluatifs sont une réalité pour la plupart des gens qui se disent des choses face à ce qui leur arrive. Ces autoévaluations (le « feed-back » interne) constituent un flot continu de pensées évaluant l'adéquation du soi à l'égard de soi-même. Dans ce phénomène, la subjectivité entre en jeu. Face à une situation, une personne peut s'évaluer spontanément comme adéquate vis-à-vis d'elle-même, sans souci du regard des autres. Une autre personne, tout en pratiquant la recherche de l'approbation de soi par soi-même, inclura le respect des autres comme un repère essentiel pour se sentir adéquate face à la même situation. Pour éviter d'engendrer des conflits avec les autres, la deuxième option est à privilégier. Il faut savoir identifier et corriger un biais négatif excessif dans sa vision de soi, des autres et du monde.
	</p>
	<p>
		En ce qui a trait au commentaire venant des autres (le « feed-back » externe), il peut être positif ou négatif. Malheureusement, la plupart des gens reçoivent beaucoup de « feed-back  » favorable authentique, mais ont tendance à ne pas y croire. Par ailleurs, tout le monde devrait s'attendre à recevoir régulièrement du « feed-back » négatif de son environnement social, dont une bonne partie est probablement valide.
	</p>
	<p>
		Bien prendre la critique et savoir bien la décoder est un signe de bonne estime de soi. En effet, les gens qui ont une bonne estime d'eux-mêmes possèdent cette habileté de bien gérer leur « feed-back » externe et leur « feed-back » interne. Elle leur permet une attitude efficace face à leurs problèmes et face à ce qui les dérange. Un sentiment de cohésion entre la personne et ses valeurs en découle.
	</p>
	<p>
		Les gens qui ont une perception positive d'eux-mêmes ont aussi une capacité d'emprise sur la réalité de leurs émotions. Ils ont développé une capacité à nuancer leur dialogue interne, ce qui leur permet d'accéder, face à une situation problématique, à un registre d'émotions modérées, à des motivations et des actions plus adaptatives. Ce phénomène s'appelle l'autocontrôle. Vous pouvez combler un manque d'autocontrôle en appliquant les trois étapes qui suivent.
	</p>
	<ul>
		<li>
			La première étape est l'auto-observation. Elle consiste à observer non seulement les effets immédiats de vos efforts mais aussi leurs effets à long terme. 
		</li>
		<li>
			La seconde étape est l'autoévaluation. Il est recommandé de ne pas être trop sévère dans vos jugements envers vous-même. Cela signifie qu'il faut savoir apprécier ce qu'il y a de valable dans votre performance et même dans votre évaluation de ce que vous êtes. On suggère d'être conséquent, conséquente plutôt que d'adopter une attitude trop punitive envers de faibles performances. 
		</li>
		<li>
			La troisième étape est l'autorenforcement. Elle consiste à vous encourager avec ce qui fonctionne et à ignorer ce qui ne fonctionne pas.
		</li>
	</ul>
	<p>
		Développer de l'autocontrôle est un procédé par essais et erreurs. Dépendamment des personnes, de leurs ressources, des habitudes acquises et des liens entretenus, cet apprentissage peut être plus ou moins long.
	</p>
</section>
<section id="affronter">
	<h1>S'affronter dans le changement plutôt que de s'éviter</h1>
	<p>
		Des recherches ont démontré que l'affrontement produit des autoévaluations positives. À travers leurs expériences, tous et toutes procèdent par essais et erreurs. Il importe de vous servir de vos erreurs pour vous améliorer et de vos succès pour vous encourager. Si vous ne prenez aucun risque, vous n'apprendrez rien. Rappelez-vous que tout le monde fait des erreurs, l'important est de trouver des nouvelles façons d'être une personne valable.
	</p>
	<p>
		Dans ce domaine, la notion de changement est de première importance. Être prêt ou prête à changer est un phénomène subjectif. Vous pourrez constater par vous-même le principe psychologique suivant : on fait des changements et il y a une croissance conséquente. La motivation vient de l'action et non le contraire. En commençant à faire l'expérience de petits changements dans le bon sens, on peut prédire une amélioration. C'est pourquoi l'on dira que la confiance en soi est un facteur basé sur des prédictions positives.

	</p>
</section>
<section id="communication">
	<h1>Développer ses compétences de communication : être empathique et adopter des sentiments positifs face aux autres</h1>
	<p>
		On peut difficilement concevoir le développement de l'estime de soi sans considérer ses habiletés de communication. Prendre conscience de votre communication efficace avec les autres est une source de valorisation. Cela consiste à vous exprimer de façon claire, nette et directe. Il faut aussi savoir communiquer au bon niveau, soit celui du problème, plutôt que de viser la personne ou la relation. Apporter l'information de façon précise et positive dans vos messages aux autres et reformuler occasionnellement les messages imprécis contribueront à augmenter votre efficacité.
	</p>
	<p>
		Les compétences de communication incluent la capacité à savoir faire une demande et à lui répondre, résoudre un conflit, savoir complimenter l'autre ou gérer la critique. La littérature à ce sujet souligne l'appréciation de l'empathie et l'adoption de sentiments positifs comme aspects prédominants, précédant les habiletés relationnelles, pour la durabilité d'une relation.
	</p>
	<p>
		Les difficultés de communication surviennent quand une personne oublie que les différences existent et tient pour acquis que ses valeurs sont universelles et vraies. Même s'il s'agit d'un cliché, on ne répétera jamais assez que chaque personne a le droit d'être différente. Être vraiment attentif ou attentive à l'existence et au respect des différences est un défi de taille en matière de relations durables.
	</p>
	<p>
		Pour améliorer la qualité de vos relations, il importe que vous preniez vos théories explicatives moins au sérieux et que vous en génériez de nouvelles qui supportent mieux vos relations avec vos amis, amies et collègues, ainsi que vos relations de couple. Vous réaliserez que vos relations peuvent changer si vous changez vos actions, vos perceptions et vos interprétations. Tout le monde a des côtés positifs, négatifs et neutres. En vous permettant d'interagir plutôt que de fuir, vous vous redonnerez une capacité d'osciller entre confiance et incertitude, une base de référence pour des perceptions plus réalistes.
	</p>
	<p>
		À long terme, vous découvrirez que si votre éducation et votre formation ont une influence sur ce que vous êtes, le choix de vos actions et vos réactions ont tout autant d'influence pour vous définir.
	</p>
</section>
<section id="democratie">
	<h1>Faire montre de constance et de démocratie, sans imposer ni à soi ni à l'autre la perfection</h1>
	<p>
		Le développement de l'estime de soi s'appuie sur le sens de l'adaptation, l'ouverture, la souplesse et le respect affirmé. Comme dans un entraînement, la tâche consiste à appliquer l'effort nécessaire à son propre équilibre et à le répéter le plus fréquemment possible. Composer avec un côté de soi que l'on n'aime pas peut parfois compliquer ce travail, mais cette autoconfrontation risque fort de permettre à un aspect de vous inconnu d'émerger.
	</p>
	<p>
		En raison de certaines circonstances affaiblissantes, votre capacité d'adaptation peut être réduite, voire presque éteinte. C'est là que la volonté de ne pas vous laisser abattre fait toute la différence. Mettre un peu d'effort (« se positiver ») vous donne plus de chances d'aboutir à des résultats valables, même si vous ne visez qu'à obtenir un résultat minimal. Vous apprendrez alors à vous encadrer et deviendrez ainsi votre meilleur entraîneur ou votre meilleure entraîneuse ! 
	</p>
</section>
<section id="references">
	<h1>Références</h1>
	<p><cite>
		André, C. et Lelord, F. (1999), L'estime de soi : s'aimer pour mieux vivre avec les autres, Paris, Éditions Odile Jacob. 
	</cite></p>
	<p><cite>
		Burns, D. (1992), Être bien dans sa peau, 3e éd., Saint-Lambert (QC), Éditions Héritage.
	</cite></p>
	<p><cite>
		Cottraux, J. (1998), Les thérapies comportementales et cognitives, 3e éd., Paris, Masson.
	</cite></p>
	<p><cite>
		Langis, P. (2003), L'amour et le couple : psychologie des relations intimes, Montréal, Méridien.
	</cite></p>
</section>
<footer>
	<h2>Rédiger par :</h2>
	<p>
		Denis Garneau, psychologue
	</p>
</footer>
