<p>
	David est à sa table de travail depuis plus d’une heure. Ses livres sont ouverts devant lui, et ses yeux parcourent le texte sans y porter attention. Il prend soudain conscience de ce fait et constate un malaise. Depuis bientôt trente minutes, son esprit erre dans des scénarios inquiétants qui s’enchaînent et tournent en boucle dans sa tête : « Et si je n’arrivais pas à terminer mon étude à temps… Je risque d’échouer mon cours et de mettre en péril ma session… ma carrière… mes idéaux… Comment pourrais-je supporter le regard de ma famille, que j’aurai tellement déçue ? Qui voudra de quelqu’un qui n’arrive même pas à terminer ses projets? ». Il tente d’occuper son esprit à autre chose pour faire baisser son anxiété, mais en vain. Il trouve que ses pensées récurrentes occupent trop de place; elles l’épuisent. Si au moins l’étude était son seul souci. Mais non! Il appréhende d’autres situations, comme de tomber malade. Il s’imagine le pire au moindre symptôme. Un mal de ventre devient le premier symptôme d’une tumeur aux intestins... David reconnaît que ses scénarios sont généralement démesurés, mais son anxiété est aussi paralysante que si le pire était réellement en train de se produire. Il se dit que la vie serait tellement plus simple sans ces moments d’incertitude… Il est tout à fait normal qu’une personne ayant connu plusieurs mauvais résultats consécutifs s’inquiète de sa note au prochain examen. Il serait aussi compréhensible qu’un individu qui a déjà surmonté une maladie grave soit davantage préoccupé par des symptômes qui s’apparentent à ceux qu’il a déjà connus. Mais si vous passez beaucoup de temps à vous inquiéter de vos études, malgré des résultats scolaires objectivement acceptables, ou que vous vous préoccupez de votre santé, en dépit d’une bonne forme physique confirmée par votre médecin, il se peut alors que vous souffriez d’un trouble d’anxiété généralisée. 
</p>
<nav>
	<ul>
		<li><a href="#definition" title="Qu'est-ce que l'anxiété généralisée?">Qu'est-ce que l'anxiété généralisée?</a></li>
		<li><a href="#expliquer" title="Comment expliquer cette forme d'anxiété?">Comment expliquer cette forme d'anxiété?</a></li>
		<li><a href="#gerer" title="Apprendre à mieux gérer les inquiétudes et l'anxiété">Apprendre à mieux gérer les inquiétudes et l'anxiété</a></li>
		<li><a href="#conclusion" title="Conclusion">Conclusion</a></li>
		<li><a href="#references" title="Références">Références</a></li>
	</ul>
</nav>
<section id="definition">
	<h1>Qu'est-ce que l'anxiété généralisée?</h1>
	<p>
		La personne aux prises avec une anxiété généralisée se préoccupe intensément, sur une longue période, de situations ordinaires de la vie courante. Elle peut s’inquiéter de son budget, de sa réussite scolaire ou professionnelle, de sa relation amoureuse, de sa santé ou de celle de son entourage. Il s’agit là de situations qui pourraient ponctuellement mobiliser l’attention de quiconque. Ce qui différencie la personne qui souffre d’anxiété généralisée des autres, c’est la ténacité et la récurrence de ses préoccupations; c’est aussi l’importance disproportionnée qu’elle y accorde, compte tenu de la probabilité réelle de l’événement redouté. En fait, la personne souffrant d’anxiété généralisée passe une grande partie de son temps à s’inquiéter de scénarios qui ne se produiront pas. Elle anticipe souvent le pire pour chacune des situations qui l’inquiètent. Cette façon de faire devient habituelle, au point où la personne finit par croire qu’il ne peut en être autrement. Cette forme incessante d’inquiétude et d’anxiété entraîne des conséquences nuisibles sur le fonctionnement et la qualité de vie. La personne ayant une anxiété généralisée deviendra souvent agitée ou continuellement fatiguée. Elle éprouvera des difficultés de concentration et pourra montrer des signes d’irritabilité. Elle ressentira fréquemment des tensions musculaires et son sommeil sera perturbé. À la longue, toutes ces réactions finiront souvent par influencer son humeur, qui deviendra dépressive. 
	</p>
</section>
<section id="expliquer">
	<h1>Comment expliquer cette forme d'anxiété?</h1>
	<p>
		 Il n’existe pas une seule façon d’expliquer comment se développe cette forme d’anxiété. Puisqu’on la retrouve davantage dans certaines familles, on considère souvent l’hypothèse d’une transmission génétique. Cependant, d’autres facteurs, comme l’environnement familial, l’apprentissage ou la présence d’événements traumatisants, contribueraient aussi au développement et au maintien de l’anxiété généralisée. Les recherches démontrent que les individus ayant une anxiété généralisée sont moins tolérants à l’incertitude que la plupart des gens. Ils réagissent plus fortement à l’incertitude parce qu’ils sont portés à surestimer la probabilité que les événements appréhendés se produisent. Comme l’incertitude est omniprésente dans nos vies, toute situation a le potentiel de devenir le point de départ d’une inquiétude. En continuant de s’inquiéter, l’individu développe souvent l’impression qu’il se prémunit contre l’incertitude en imaginant à l’avance ce qui, autrement, aurait pu le surprendre. Ainsi, son attention est davantage portée sur les conséquences redoutées que sur les solutions. Sa vie se remplit de scénarios redoutables qui déclenchent son anxiété et l’entretiennent. Pour éviter ce malaise, il tentera alors de penser le moins possible à ces scénarios et se privera ainsi de la possibilité d’en évaluer la réelle probabilité et d’en mesurer le réalisme. De la même façon, l’individu évitera des situations concrètes qui le préoccupent au lieu de chercher des solutions et de les appliquer pour surmonter le problème. 
	</p>
</section>
<section id="gerer">
	<h1>Apprendre à mieux gérer les inquiétudes et l'anxiété</h1>
	<p>
		Il est fréquent d’avoir des inquiétudes et de se sentir anxieux. Toutefois, en modifiant nos attitudes et nos comportements, nous pouvons parvenir à mieux maîtriser nos inquiétudes lorsqu’elles deviennent démesurées. Voici quelques stratégies.
	</p>
	<h2>
		1) Examinez d’abord les facteurs associés à votre réaction d’anxiété. Plus vous arriverez à cerner les facteurs qui la déclenchent et ceux qui contribuent à la maintenir, mieux vous pourrez la gérer. 
	</h2>
	<ul>
		<li>
			Identifiez l’élément déclencheur : quelle situation a déclenché votre malaise?
		</li>
		<li>
			Précisez les inquiétudes : quelles pensées ou images vous viennent à l’esprit et sont à l’origine de sentiments de peur? Quelle est la menace perçue? Quels scénarios défilent dans votre tête? 
		</li>
		<li>
			Reconnaissez les signes d’anxiété : quels malaises ressentez-vous (tensions musculaires, tremblements, difficulté de concentration, problèmes de sommeil, etc.)? 
		</li>
		<li>
			Constatez l’effet de votre réaction ou de votre inaction : vos peurs vous paralysent-elles? Vous poussent-elles à fuir ou à éviter d’agir pour résoudre la situation? Ou encore, cherchez-vous à vous distraire pour éviter de penser à ce qui vous préoccupe?
		</li>
	</ul>
	<h2>2) Parlez-vous de façon réaliste pour désamorcer l’anxiété.</h2>
	<ul>
		<li>
			Rappelez-vous que l’anxiété, bien qu’inconfortable, est une réaction normale de votre corps. Votre anxiété a été déclenchée parce que vous avez perçu un danger. Elle agit comme un système d’alarme en vous avisant d’une menace. Elle joue un rôle de protection, car elle vous prévient de vous occuper de quelque chose qui vous dérange.
		</li>
		<li>
			Souvenez-vous que l’anxiété finit toujours par se dissiper. Telle une vague qui s’élève, atteint son sommet et s’estompe peu à peu, le corps dispose d’un mécanisme normal de régulation qui fait baisser le niveau d’anxiété après un certain temps.
		</li>
		<li>
			Gardez à l’esprit que la durée de la réaction d’anxiété est attribuable à notre façon d’y répondre soit par nos pensées, soit par nos actions (ou notre inaction).
		</li>
	</ul>
	<h2>3) Faites la guerre à l’évitement</h2>
	<p>
		a) Si vous évitez des situations :
	</p>
	<ul>
		<li>
			Exposez-vous à ce qui vous rend inconfortable! Lorsque nous ressentons de l’anxiété, nous avons souvent tendance à éviter les situations qui déclenchent cette anxiété afin de faire diminuer ou cesser le malaise. Mais l’évitement ne nous permet pas de comprendre et surtout de maîtriser ce qui nous dérange. L’étudiant qui redoute de parler en public ressentira un bref soulagement s’il réussit à éviter de prendre la parole devant un groupe. Toutefois, son malaise restera entier chaque fois que la situation se présentera. Pour votre bien-être à long terme, il vaut mieux vous exposer à la situation. En vous exposant, vous ressentirez peut-être un certain malaise à le faire, mais il est démontré que plus on s’expose à une situation qui nous fait peur ou plus on l’affronte, mieux on parvient à la surmonter et, en conséquence, mieux on se sent.
		</li>
		<li>
			Suivez les étapes de <a href="{{LIEN}}resolution-problemes"></a>la résolution de problèmes pour mieux composer avec une situation problématique. 
		</li>
	</ul>
	<p>
		b) Si vous évitez des pensées :
	</p>
	<ul>
		<li>
			Résistez à la tentation de chasser vos pensées. En présence d’une situation redoutée dont vous craignez les répercussions dans un avenir plus ou moins rapproché, vous pourriez être tenté de chasser vos pensées en vous distrayant ou en tentant de vous rassurer dès qu’elles vous viennent à l’esprit. À court terme, vous auriez l’impression d’en être débarrassé, mais sachez que cela ne fera que maintenir les inquiétudes et l’anxiété, qui reviendront sans crier gare. Un moyen de vous aider à y arriver pourrait être d’écrire en détail le scénario que vous redoutez. Observez alors vos inquiétudes et laissez-les passer dans votre esprit, telle l’eau d’une rivière qui suit son cours, en vous rappelant que ce ne sont que des pensées (et non des faits). Rappelez-vous qu’il vaut mieux laisser « couler » ces pensées. Vous ne pourrez jamais être certain du dénouement d’une situation (dans le présent ou dans l’avenir), mais vous pourrez toujours agir au moment opportun.
		</li>
		<li>
			Ne vous jugez pas d’avoir ces pensées, mais félicitez-vous plutôt d’en avoir pris conscience. Ramenez-vous au moment présent et permettez au calme de revenir. 
		</li>
	</ul>
	<h2>4) Respirez et relaxez</h2>
	<p>
		Si votre niveau de stress est élevé, vous avez besoin de rétablir l’équilibre. La respiration diaphragmatique est un moyen simple et efficace d’atteindre cet objectif. 
	</p>
	<ul>
		<li>
			Trouvez d’abord un lieu propice au calme. 
		</li>
		<li>
			Pour vous mettre dans la bonne disposition d’esprit, fermez les yeux et répétez-vous mentalement « je suis calme et tranquille » à quelques reprises. 
		</li>
		<li>
			Inspirez normalement par le nez en vous assurant de faire passer l’air au niveau de votre abdomen, et non de votre thorax. Pour vous aider, placez une main sur le ventre et l’autre sur la poitrine : seule la main déposée sur votre ventre devrait se soulever durant votre inspiration.
		</li>
		<li>
			Expirez longuement par la bouche pour laisser sortir tout l’air de vos poumons.
		</li>
		<li>
			Faites cet exercice durant au moins cinq minutes et recommencez chaque fois que vous le pouvez dans la journée. 
		</li>
		<li>
			Idéalement, entrainez-vous à toujours respirer de cette façon. 
		</li>
	</ul>
	<p>
		Pour en savoir davantage sur les différentes techniques de relaxation, consultez le texte sur <a href="{{LIEN}}relaxation" title="la relaxation" target="_blank">la relaxation</a>.
	</p>
</section>
<section id="conclusion">
	<h1>Conclusion</h1>
	<p>
		L’anxiété généralisée touche environ 5 % de la population. De nombreuses personnes arrivent à surmonter par elle-même cette forme d’anxiété en adoptant des stratégies qui s’apparentent à celles que nous avons décrites ici. Dans la mesure où vos efforts pour venir à bout de ces difficultés n’auraient pas suffi, n’hésitez pas à faire appel aux services de psychologie du Centre d'aide aux étudiants, qui pourra vous accompagner dans cette démarche. Vous pouvez obtenir plus de renseignements ou prendre un rendez-vous en composant le 418-656-7987. Bon succès dans votre recherche d’équilibre et de bien-être.

	</p>
</section>
<section id="references">
	<h1>Références</h1>
	<p><cite>
		LADOUCEUR, R., L. BÉLANGER et É. LÉGER. Arrêtez de vous faire du souci pour tout et pour rien, Paris, Éditions Odile Jacob, 2003. 
	</cite></p>
</section>
<footer>
	<h2>Rédiger par :</h2>
	<p>
		Caroline Sylvain, psychologue<br>
		Michel Dumont, psychologue
	</p>
</footer>
