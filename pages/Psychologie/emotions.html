<p>
	De tout temps, les émotions ont eu ce pouvoir, chez l'être humain, de le transporter des plus hauts sommets aux abîmes les plus profonds. Issues de régions profondes et archaïques du cerveau, les émotions nous imprègnent et nous influencent, souvent à notre insu. Selon le tempérament de chacun, elles vont du petit frisson, à peine perceptible à la surface de l'eau, au raz-de-marée qui inonde tout, en passant par la vague qui vient balayer le rivage de la conscience. Ces fluctuations sont généralement normales et contribuent au charme et à l'intérêt de nos rapports avec les autres. Elles peuvent toutefois devenir une source d'inquiétude et d'inconfort marqué.
</p>
<p>
	Il n'est donc pas étonnant que les situations liées à la vie émotionnelle incitent beaucoup de gens à consulter. Leurs demandes pourraient souvent se résumer ainsi : « Aidez-moi à ne plus ressentir ce que je ressens », comme s'il fallait à tout prix éliminer tout mouvement de l'eau, de peur de s'y noyer. Bien qu'il soit naturel de vouloir conserver ou retrouver vite notre équilibre interne, il ne serait toutefois pas souhaitable d'immuniser quelqu'un contre ses émotions. Ce serait comme de vouloir isoler la terre de l'océan. Sans l'eau qui la fertilise, la terre ne serait que sécheresse et désolation à perte de vue. Dans cet article, nous vous proposons donc d'apprivoiser le flot de vos émotions afin de vous en nourrir et d'apprendre à y nager en toute sécurité.
</p>
<nav>
	<ul>
		<li><a href="#grandeurs-miseres" title="Les émotions : grandeurs et misères">Les émotions : grandeurs et misères</a></li>
		<li><a href="#double-fonction" title="Les émotions : un système de sécurité à double fonction">Les émotions : un système de sécurité à double fonction</a></li>
		<li><a href="#que-faire" title="Que faire avec ses émotions ?">Que faire avec ses émotions ?</a></li>
		<li><a href="#blocage" title="Deux causes de blocage émotionnel...">Deux causes de blocage émotionnel...</a></li>
		<li><a href="#respect-mutuel" title="Le respect mutuel : un point de repère essentiel">Le respect mutuel : un point de repère essentiel</a></li>
		<li><a href="#conclusion" title="Conclusion">Conclusion</a></li>
		<li><a href="#references" title="Références">Références</a></li>
		<li><a href="#annexe-maslow" title="Annexe: La pyramide de Maslow">Annexe: La pyramide de Maslow</a></li>
	</ul>
</nav>
<section id="grandeurs-miseres">
	<h1>Les émotions : grandeurs et misères</h1>
	<p>
		La plupart du temps, nous n'apprécions nos émotions que lorsqu'elles tombent bien et nous rendent heureux. Nous souhaiterions pouvoir les contrôler, tant dans leur fréquence que dans leur intensité, leur durée ou le moment de leur apparition. Il faut dire que dans notre courant social actuel, les émotions sont souvent perçues comme une menace pour la productivité, ou comme des enfantillages déraisonnables, signes d'immaturité honteuse qu'il nous faut apprendre à camoufler et à éliminer.
	</p>
	<p>
		Mais, en y regardant de plus près, il existe des émotions qui échappent à cette règle, des émotions qui sont courtisées et accueillies à bras ouverts. C'est le cas, notamment, de la joie, de l'amour, du plaisir ou de la satisfaction. Bien que ces émotions altèrent notre précieux équilibre interne, nous ne nous en rendons pas compte parce qu'elles nous procurent un état agréable que nous sommes heureux de retrouver. Il ne nous viendrait pas à l'idée d'éliminer tout sentiment de joie ou de bonheur ! Et puis il y a ces autres émotions, des parias dont nous tentons d'ignorer l'existence, à défaut de les éliminer. Telles des parasites, ces émotions dérangent, troublent notre quiétude et notre emploi du temps si soigneusement planifié. C'est le cas, notamment, de la tristesse, de la colère et de la peur. Malgré nos efforts pour les circonscrire et les museler, elles s'accrochent, refusent de disparaître ou de se faire oublier, insistent même jusqu'à altérer notre fonctionnement.
	</p>
	<p>
		Notre résistance répétée envers les émotions désagréables peut effectivement donner lieu à une panoplie de conséquences indésirables, un peu comme un courant d'eau que l'on tente de bloquer. Ces eaux ne disparaissent pas, simplement parce que nous le voulons et que nous tentons d'en stopper la progression ; elles se détournent plutôt docilement vers une faille, ailleurs, où elles se déversent et vont envahir un terrain où elles n'ont pas lieu d'être, causant parfois des dégâts considérables. C'est ainsi qu'il peut nous arriver d'être surpris par un envahissement émotionnel déplacé et incompréhensible dans une situation particulière, le sens de cette expérience nous échappant complètement, nous laissant gênés et confus. À d'autres moments, nos émotions bloquées s'infiltrent profondément en nous à notre insu, minant alors nos fondements. C'est alors que nous voyons apparaître des maux physiques, de l'anxiété, de la dépression, une perte de concentration, une fatigue inexpliquée, etc.
	</p>
	<p>
		Notre ignorance du fonctionnement des émotions nous amènent parfois à les maltraiter au point qu'elles finissent par nous envahir et nous causer bien des ennuis, bien que leur but soit de favoriser notre bien-être. En fait, les émotions ont assuré la survie de notre espèce depuis des millénaires. C'est grâce à elles, entre autres, que nos ancêtres ont fui certains dangers et recherché les situations les plus favorables au maintien de leur bien-être. Nous vous proposons ici de découvrir le secret de nos ancêtres à l'aide d'une analogie décrivant bien le rôle vital que jouent nos émotions.
	</p>
</section>
<section id="double-fonction">
	<h1>Les émotions : un système de sécurité à double fonction</h1>
	<p>
		Examinons d'abord ce qui fait apparaître les émotions. En en comprenant mieux la cause, il pourra être plus facile d'apprendre à les utiliser. Les émotions sont en fait un système de sécurité ultra-sophistiqué dont la nature nous a généreusement dotés. Tel un radar doublé d'un GPS, elles remplissent deux fonctions :
	</p>
	<ul>
		<li>
			sonder l'environnement à la recherche d'obstacles et d'éléments favorables ;
		</li>
		<li>
			indiquer la route à suivre pour maintenir ou retrouver notre satisfaction en toute sécurité.
		</li>
	</ul>
	<p>
		Le message des émotions est donc double :
	</p>
	<ul>
		<li>
			Première information : « Il y a un événement significatif en vue ». L'intensité (forte ou faible) et la qualité (positive ou négative) de l'émotion signalent la présence d'une situation ou d'un événement qui peut avoir un impact fort ou faible, positif ou négatif, sur notre bien-être.
		</li>
		<li>
			Deuxième information : « Voici l'action à accomplir ». La nature de l'émotion (peur, joie, colère, tristesse, paix, etc.) contient en elle-même une indication de l'ajustement à faire, de l'action à poser pour retrouver ou maintenir notre bien-être.
		</li>
	</ul>
	<p>
		Si l'impact de la situation détectée est perçu comme non significatif, la situation a toutes les chances de passer inaperçue et ne générera pas d'émotion notable. Si l'impact de la situation est perçu comme positif, nous éprouvons une émotion agréable qui nous incite à retenir et, si possible, à renouveler ce genre d'expérience. Si la situation contrevient à un besoin important pour notre bien-être, l'impact perçu est négatif. L'émotion générée par notre cerveau est alors désagréable, dérangeante, de sorte qu'il est difficile de l'ignorer, tel un signal d'alarme. Tout se passe comme si notre cerveau voulait s'assurer que notre conscience s'arrêtera à la situation et verra à y apporter la correction nécessaire.
	</p>
	<img src="wp-content/uploads/2014/11/Emotion2.jpg" alt="Évaluation cognitive: Une signification est attribuée selon l'impact que peut avoir le stimulus sur un besoin personnel.">
	<p>
		L'intensité de l'émotion, quant à elle, est influencée par l'importance du besoin en cause ou l'importance de l'impact de la situation sur ce besoin. Ainsi, un léger impact sur un besoin vital générera tout de même une bonne intensité émotionnelle et il en va de même pour un énorme impact sur un besoin secondaire.
	</p>
	<p>
		Par exemple, si vous gagnez 50 $ à la loterie, vous serez heureux. Vous aurez tendance à vouloir renouveler l'expérience. Si, en plus, vous n'aviez plus un rond pour vivre durant la semaine, l'intensité de votre satisfaction risque d'être beaucoup plus grande et sera agrémentée de soulagement. Cette dernière émotion vous indiquera que vous étiez dans le pétrin et que vous viviez une certaine inquiétude. Si vous gagnez 500 000 $, votre joie sera à son comble et vous vivrez beaucoup d'excitation à l'idée de tous les besoins que vous pourrez vraisemblablement combler pour longtemps.
	</p>
	<p>
		Si, par inadvertance ou par négligence, nous faisons fi du signal d'alarme émis par ce système de sécurité, il s'amplifiera, allant même jusqu'à prendre une autre forme pour attirer notre attention ou à nous assaillir littéralement dans les moments et les endroits les plus embarrassants. N'oublions pas que ce système a pu assurer la survie de notre espèce depuis la nuit des temps justement parce qu'il se montre intraitable, qu'il ne peut être réduit au silence ou ignoré longtemps et qu'il trouve toujours le moyen de se faire entendre. C'est comme un équipement de base intrinsèque de l'être humain ; nous vous suggérons donc d'en accepter la présence et d'apprendre à l'utiliser plutôt que de tenter de l'éliminer !
	</p>
</section>
<section id="que-faire">
	<h1>Que faire avec ses émotions ?</h1>
	<p>
		Comme pour l'information qu'il véhicule, il y a deux étapes principales dans l'utilisation de ce système de sécurité : identifier le besoin visé et déterminer l'action à poser.
	</p>
	<h2>1) Identifier le besoin visé</h2>
	<p>
		La première chose à faire, lorsqu'une émotion nous envahit, c'est de découvrir quel besoin personnel est mis en cause par la situation. Certains besoins généraux sont reconnus comme universels chez l'être humain. <a href="#annexe-maslow" title="La pyramide de Maslow">La pyramide de Maslow</a> nous présente une liste de ces besoins, selon une certaine hiérarchie. En résumé, il s'agit des besoins physiologiques et de survie, des besoins de sécurité, des besoins d'amour et d'appartenance, des besoins d'estime de soi et des besoins de réalisation. Notre préoccupation va toujours aux besoins les plus près de la base de cette pyramide, car ils ont plus d'impact sur notre survie. Je vous invite à consulter cette liste plus en détail en cliquant sur le lien ci-dessus.
	</p>
	<p>
		Pour identifier le besoin en cause, il faut accepter de s'arrêter et de se questionner. Qu'est-ce qui me dérange tant dans cette situation ? En quoi est-ce que ça m'atteint ? Qu'est-ce que j'aurais plutôt voulu et en quoi ça m'aurait fait plus de bien ? Lequel de mes besoins serait alors comblé ? Plusieurs objecteront qu'ils n'ont pas le temps de s'arrêter ou laissent tomber la démarche parce qu'ils ne trouvent pas la réponse immédiatement. Toutefois, l'inconfort et les conséquences indésirables qui découlent des choix faits sans tenir compte de vos besoins personnels vous feront peut-être regretter le temps économisé sur le coup. On parle parfois ici d'années perdues, de malaises chronicisés affectant de plus en plus le fonctionnement, de réseau social inadéquat qu'on devra prendre le temps de renouveler, etc. Rassurez-vous : si au départ il faut un peu de patience pour développer cette habileté, le temps nécessaire pour identifier le besoin visé devient relativement court une fois qu'est développée l'habitude de se consulter soi-même. 
	</p>
	<p>
		Voici un exemple de la façon dont l'émotion peut aider à identifier ses besoins.
	</p>
	<p>
		En général, la colère est une réaction à quelqu'un ou quelque chose qui fait obstacle à la satisfaction d'un besoin, alors que la tristesse surgit quand nous sommes au contact du manque qui en résulte. Par exemple, si l'absence imprévue de votre partenaire vous empêche de vous rendre à une soirée de couple dont vous aviez vraiment envie, vous éprouverez probablement de la colère envers votre partenaire (la cause de votre empêchement) et de la tristesse en pensant à ce que vous avez manqué. La tristesse vous indique que vous avez besoin de contacts sociaux et de détente et votre colère vous indique que vous avez besoin que votre partenaire vous respecte en se montrant responsable et fiable dans les engagements pris envers vous. Après avoir identifié ces besoins, vous pouvez choisir, par exemple, de clarifier vos besoins et limites avec votre partenaire à son retour et d'aller seule, seul à cette soirée, à moins que vous ne fassiez autre chose pour tout de même vous distraire. Vos émotions s'atténueront, surtout si vous pouvez prendre plaisir à la nouvelle activité choisie et que vous pensez pouvoir négocier une entente avec votre partenaire pour éviter que cela ne se reproduise.
	</p>
	<h2>2) Déterminer l'action à accomplir</h2>
	<p>
		La deuxième information contenue dans l'émotion est, en fait, une tendance à l'action qui pousse à agir sur la situation ou à s'y ajuster. Mais avant tout, il faut prendre soin de déterminer qui est responsable de répondre au besoin identifié. Ce n'est qu'ensuite qu'il convient de s'ajuster ou de tenter d'influencer la situation de façon à répondre au besoin identifié. 
	</p>
	<h2>Premièrement : déterminez qui est responsable de quoi</h2>
	<p>
		Avant de tirer, il convient de bien choisir sa cible et d'ajuster le tir afin qu'il soit réellement efficace et atteigne son but, sans pour autant briser ce qui entoure la cible. Il nous faut donc, au préalable, déterminer qui est responsable de veiller à la satisfaction du besoin en cause. Si nous sommes la première personne responsable de notre besoin et de la situation, il ne nous sert à rien de tenter de faire pression sur autrui. 
	</p>
	<p>
		Par exemple, ce n'est pas à notre partenaire de briser notre solitude si nous nous ennuyons. En tant qu'adulte, nous sommes responsables de nos besoins de détente et de stimulation. Si nous sommes persuadés du contraire, il se peut que nous exigions que notre partenaire soit toujours présent quand nous voulons sortir et qu'alors, nous ressentions de la colère face à cette personne parce qu'elle n'est pas disponible pour nos besoins. Se morfondre en attendant son retour pour, ensuite, lui chercher querelle à cause de notre frustration n'aura malheureusement pas satisfait notre besoin de compagnie ; et, en prime, nous aurons aussi réussi à brimer un autre de nos besoins, celui d'une relation harmonieuse et paisible. S'il est vrai que nous pouvons choisir notre partenaire comme source de satisfaction principale pour notre besoin, il en existe d'autres et il nous incombera toujours d'y avoir recours si notre partenaire n'est pas disponible. À nous de déterminer dans quelle proportion nous désirons qu'il ou elle contribue à nos activités et à faire notre choix en conséquence...
	</p>
	<h2>Deuxièmement, identifiez vos zones de pouvoir dans cette situation</h2>
	<p>
		Nous avons principalement deux choix dans notre action : nous ajuster ou agir sur la situation. Il s'agit ici d'identifier en quoi nous contribuons ou pouvons contribuer à la situation. Nous avons toujours un pouvoir quelconque sur la situation et nous devons nous efforcer de découvrir en quoi. Nous pouvons soit la modifier, soit nous y ajuster en modifiant nos croyances, nos objectifs, ou en ciblant une autre source de satisfaction. Nous pouvons aussi nous ajuster à la situation en l'acceptant, mais en faisant aussi en sorte qu'elle ne se répète pas ou qu'elle ne soit pas nuisible si elle se répète.
	</p>
	<p>
		Si la situation est perçue comme bénéfique pour nous, nous tiendrons à en rechercher le maintien ou la répétition (agir sur). Si elle est perçue comme nuisible, nous devons évaluer en quoi et déterminer s'il nous est possible de la modifier pour la rendre conforme à notre besoin, ou s'il nous faut plutôt nous y ajuster.
	</p>
	<p>
		Par exemple, l'ajustement personnel, quant à la responsabilité de notre besoin de compagnie mentionné plus haut, modifiera notre émotion. La colère se changera probablement en tristesse et en regret de ne pouvoir voir les personnes prévues, mais cela laissera aussi place à de l'espoir. En effet, en nous accordant la responsabilité principale de ce besoin, nous nous redonnons automatiquement du pouvoir car nous nous permettons de nous ouvrir à d'autres moyens et actions pour satisfaire notre besoin (influence active sur la situation). Cela engendrera par la suite du contentement et de la satisfaction.
	</p>
	<p>
		Par contre, si notre partenaire s'était engagée ou engagé à nous accompagner mais s'est désistée ou désisté à la dernière minute, nous nous sentirons probablement blessée, blessé dans notre besoin de considération et de respect. Il ne nous sert à rien de minimiser la situation dans notre esprit (ajustement psychologique inadéquat) afin qu'elle ne nous dérange plus. Même si, sur le coup, cela nous permettrait d'éviter un désaccord et de résoudre la situation pacifiquement en abaissant l'inconfort émotionnel de notre couple, cet ajustement serait, en fait, un déni de nos besoins et serait susceptible de nous nuire à moyen et long terme. En agissant sans considération pour nos besoins, nous nous traitons comme étant sans importance (action inadéquate) ; du coup, nous nous sentons dévalorisée, dévalorisé, indigne d'intérêt et notre estime de soi vient d'en prendre pour son rhume !
	</p>
	<p>
		La responsabilité du besoin est ici partagée : si je suis fidèle à ma part de l'engagement, mon ou ma partenaire est aussi responsable de me démontrer une réciprocité sur ce plan. Si la personne choisie ne se montre pas digne de confiance dans ses engagements, je suis alors responsable de mieux choisir de qui je m'entoure (agir sur la situation). J'aurai sans doute d'abord à réviser la perception que j'avais de cette personne et de la satisfaction réelle que cette relation peut apporter à mon besoin de considération et de respect (ajustement psychologique adéquat et réaliste).
	</p>
</section>
<section id="blocage">
	<h1>Deux causes de blocage émotionnel...</h1>
	<p>
		En théorie, tout cela fonctionne à merveille. En pratique, direz-vous, c'est parfois plus compliqué. Effectivement, nous avons tous plus ou moins appris à nous suradapter à des situations qui ne nous offraient pas beaucoup de possibilités d'agir. C'est, par exemple, le propre de l'enfant qui vit dans un rapport hiérarchique et de dépendance. N'ayant pas la capacité de jugement des adultes qui en prennent soin, il est normal qu'il ne décide bien souvent pas de sa vie et que cela l'amène à vivre des contrariétés ou des peines. Cela lui permet d'ailleurs de se préparer adéquatement à sa vie d'adulte, dans laquelle il aura à vivre et à tolérer des situations similaires. Si, toutefois, il ne reçoit pas l'accueil et le support adéquat pour vivre ces expériences émotionnelles, il peut développer des façons d'agir et de réagir inadéquates qui, une fois devenu adulte, persisteront. Les deux suivantes sont assez fréquentes. 
	</p>
	<h2>a. Modifier ses besoins plutôt que trouver les moyens d'y répondre</h2>
	<p>
		Une des erreurs fréquemment commises consiste à tenter de modifier nos besoins pour qu'ils correspondent à la situation, plutôt que de prendre les moyens nécessaires pour répondre à nos besoins compte tenu de la situation. Il arrive qu'une personne aie le sentiment de ne pas avoir de pouvoir sur une situation négative. Cela entraîne nécessairement un sentiment d'impuissance, de découragement et, si cette situation est appelée à se répéter, peut-être même de désespoir, voire de dépression. La personne pense alors que son seul pouvoir est de modifier son besoin. C'est le cas quand, par exemple, une personne ne veut pas déplaire aux autres.
	</p>
	<p>
		Un examen plus poussé de la situation indique souvent que la personne ne se permet pas de prendre son pouvoir et d'agir en fonction de ses besoins. Les raisons peuvent en être nombreuses : peur des représailles, peur de paraître égoïste, peur du rejet, système de valeur interdisant de prendre action, de s'affirmer, de décevoir, etc. Vaut mieux alors s'attarder à ces raisons et les questionner que de nier nos besoins. Selon quel système de valeurs les motifs de l'inaction ont-ils un sens ? Dans quel contexte ces valeurs et ces façons de faire ont-elles été développées et adoptées ? Sont-elles encore valables et adaptées en tant qu'adulte et compte tenu du contexte actuel ?
	</p>
	<p>
		Nos besoins, tout comme les goûts et contrairement aux souhaits, ne sont pas à discuter. Ils sont légitimes au départ car ils sont générés par les conditions nécessaires à notre bien-être. Les moyens à utiliser pour y répondre, eux, peuvent varier et s'adapter aux circonstances. Si, pour nous ajuster, nous nions notre peine, nous répondons certes à notre besoin de relation pacifique et d'harmonie, mais nous contrevenons sérieusement à notre besoin de dignité et de respect qui est plus bas sur la pyramide des besoins, donc plus important. Nous en ressortons perdant... et plus anxieux que jamais! La recette pour s'y retrouver, c'est que quelle que soit l'alternative que nous choisissons (nous ajuster ou agir), celle-ci doit être cohérente avec l'importance de nos besoins et la responsabilité qui en découle, le tout dans le respect les uns des autres. 
	</p>
	<h2>b. Des croyances ou règles de conduite inadéquates et irréalistes</h2>
	<p>
		Comme mentionné précédemment, certaines croyances génèrent des règles de conduite qui s'opposent à notre sécurité et à notre bien-être. Par exemple, une cause fréquente d'anxiété sociale est le sentiment de ne pas pouvoir se défendre contre les brimades et agressions verbales ou morales, ou de devoir systématiquement satisfaire les autres au détriment de soi. Dans un tel contexte, comment avoir envie de côtoyer les gens ?! Certaines croyances relationnelles nous interdisent donc de nous protéger et de tenir compte de nos besoins quand nous sommes en présence des autres. En voici quelques exemples : 
	</p>
	<ul>
		<li>
			<q>Se défendre, c'est agresser l'autre</q>
		</li>
		<li>
			<q>Prendre soin de ses propres besoins est égoïste</q>
		</li>
		<li>
			<q>Aimer, c'est veiller au bien-être de l'autre</q>
		</li>
		<li>
			<q>Mettre ses limites, c'est contrôler l'autre de manière abusive</q>
		</li>
		<li>
			etc.
		</li>
	</ul>
	<p>
		Ce sont là des croyances qui invalident la légitimité et l'importance de nos besoins personnels. Il ne nous reste plus qu'à subir la situation désagréable et à nous en vouloir, par la suite, d'en être aussi affecté. Puis, pour nous permettre d'accepter la situation, nous nous déclarerons fautif d'avoir autant besoin d'être respecté... et nous éviterons un peu plus la présence des autres, car nous ne pouvons nous sentir à l'aise de toujours nous nier. 
	</p>
	<p>
		Dans cette situation, il sera d'abord nécessaire que nous nous ajustions intérieurement en révisant les croyances qui nous interdisent d'agir en considérant nos besoins. Nous pourrons ensuite trouver comment nous pouvons influencer la situation et assurer un peu mieux notre sécurité et notre bien-être, tout en demeurant respectueux.
	</p>
</section>
<section id="respect-mutuel">
	<h1>Le respect mutuel : un point de repère essentiel</h1>
	<p>
		À ce point, certains d'entre vous se demandent peut-être où est la limite raisonnable entre l'égoïsme et un amour de soi sain. La limite est simple et claire : c'est l'accueil et le respect de chacun dans la différence de ses besoins qui prime. L'empathie est cette faculté qui permet d'être sensible à ce que vit l'autre ; la tolérance et l'ouverture sont ces qualités qui permettent de respecter l'autre dans sa différence et ses besoins. Mais attention ! Respecter et accepter ne veulent pas dire satisfaire ou prendre en charge. L'ouverture et le respect nous dictent de ne pas juger et condamner l'autre dans ses besoins ou ses différences. Elles nous dictent aussi de ne pas forcer l'autre à adopter nos besoins, par quelques moyens que ce soit et vice versa.
	</p>
	<p>
		Ce respect et cette ouverture, nous nous les devons d'abord à nous-mêmes. Notre responsabilité première, en tant qu'adulte, est de veiller à accueillir nos besoins et à y répondre d'une manière qui, lorsque cela est possible, ne contrevient pas au besoin de l'autre. Cela n'est pourtant pas toujours possible, surtout quand l'autre a un besoin diamétralement opposé au nôtre. Si cet autre agit aussi dans le respect, il y aura tolérance et ouverture pour les choix et besoins de chacun. Sinon, il risque d'être inutile d'insister pour que notre besoin soit respecté. Il nous faudra peut-être plutôt agir en fonction de notre besoin, que cela plaise ou non. Mais pour agir ainsi, nous aurons peut-être d'abord à nous ajuster en révisant peut-être une autre croyance, celle qui dicte que « pour être une bonne personne, tous doivent nous apprécier et nous approuver ».
	</p>
	<h2>Quand la persuasion devient abus</h2>
	<p>
		Dans nos efforts pour veiller à la satisfaction de nos besoins, il est normal de tenter de persuader l'autre de tenir compte de notre besoin, si cette personne nous apparaît être impliquée dans la satisfaction de ce besoin. La tentation peut être grande, alors, d'user de stratagèmes visant à l'amener, malgré elle, à consentir à ce que nous voulons. Mais attention! Ceci constituerait un abus.
	</p>
	<p>
		L'usage de la force pour imposer nos besoins ou désirs à l'autre peut prendre plusieurs formes. Nous sommes tous conscients que la force physique est à proscrire. Toutefois, il existe d'autres formes d'abus plus subtiles et tout aussi inadmissibles. Il s'agit des abus verbaux et psychologiques. Toute intervention visant, de façon volontaire ou non, à avoir sur l'autre un impact tel qu'il l'amènera à faire, dire ou penser, malgré lui ou elle, ce que nous voulons qu'il fasse, dise ou pense constitue une forme d'abus. Si nous avons la responsabilité de communiquer ouvertement et clairement nos besoins aux personnes concernées, nous avons aussi le devoir d'accepter et de tolérer que ces personnes puissent être en désaccord avec nous. 
	</p>
	<p>
		L'usage de la force psychologique, que l'on nomme aussi manipulation, peut prendre des formes variées. En voici quelques exemples.
	</p>
	<ul>
		<li>
			Insulte, dénigrement, critique méprisante, blâme incessant.
		</li>
		<li>
			Parole ou geste ayant pour but de susciter des émotions négatives chez notre interlocuteur, telles la culpabilité, la honte, la peur, la frustration, l'appréhension, l'angoisse, l'inquiétude et la souffrance.
		</li>
		<li>
			Parole ou geste portant atteinte à l'intégrité psychologique de l'interlocuteur : attaquer son image de soi, le dévaloriser, le ridiculiser, l'infantiliser, minimiser ou invalider son opinion ou son besoin.
		</li>
		<li>
			Menace de rejet, d'abandon, de privation économique, sexuelle ou affective Intimidation, chantage, discrimination sur la différence.
		</li>
		<li>
			Mensonge, omission faisant en sorte de priver l'autre d'une information nécessaire à un choix libre et éclairé.
		</li>
	</ul>
	<p>
		L'usage de ces procédés et de tous ceux qui y sont apparentés constitue une limite que le respect d'autrui nous dicte de ne pas franchir. C'est aussi une indication claire qu'il est temps de cesser de vouloir agir sur l'autre. S'il ne nous est pas possible de tolérer davantage la frustration du besoin que nous tentons de faire valoir, un ajustement personnel s'impose.
	</p>
	<p>
		Par exemple, si notre partenaire nous fait régulièrement défaut dans les engagements pris envers nous, il sera abusif de le ou la rabaisser en attaquant sa valeur personnelle ou de tenter de l'amener à se plier à notre exigence en le ou la menaçant de quelque façon que ce soit. Ce serait tenter de l'amener, malgré lui ou elle, à faire ce que nous désirons et ainsi contrevenir à sa liberté de choix. Si notre partenaire ne désire pas tenir compte de notre besoin et que cela porte atteinte à notre propre sentiment de valeur personnelle, il nous faudra peut-être, alors, envisager d'y renoncer pour demeurer solidaire de nos besoins.
	</p>
	<p>
		User de force pour amener l'autre à consentir, contre son gré, à ce que nous voulons signifie que nous le rendons responsable de ce besoin. N'oublions pas qu'en tant qu'adultes, nous sommes les premiers responsables de nos besoins.
	</p>
</section>
<section id="conclusion">
	<h1>Conclusion</h1>
	<p>
		En résumé, nos émotions sont de précieuses alliées pour nous guider vers notre bien-être. Elles ont un langage qu'il faut apprendre à décoder si nous voulons vivre en harmonie avec elles et évoluer vers un mieux-être continuel. En apprenant à utiliser adéquatement nos émotions, nous nous assurons de toujours évoluer en direction de nos besoins et de nos buts.
	</p>
	<p>
		Cet article présente brièvement des outils de base pour vous aider à utiliser adéquatement vos émotions et à les apprivoiser. Certaines expériences émotionnelles sont toutefois particulièrement intenses, lourdes ou chargées, comme lors du deuil d'un proche par exemple. Dans ces situations, la personne peut devenir submergée et ne plus avoir le recul nécessaire pour y voir clair. L'aide d'une personne-ressource appropriée peut alors s'avérer très utile. Il ne faut pas hésiter à s'en prévaloir, car il y va de votre bien-être à long terme. 
	</p>
</section>
<section id="references">
	<h1>Références</h1>
	<p><cite>
		CORNELIUS, Randolph R.(1996), The Science of Emotion : Research and Tradition in the Psychology of Emotions, Upper Saddle River (NJ), Prentice Hall.
	</cite></p>
	<p><cite>
		D'ANSEMBOURG, Thomas (2001), Cessez d'être gentil, soyez vrai ! Être avec les autres en restant soi-même, Montréal, Éditions de l'Homme.
	</cite></p>
	<p><cite>
		GARNEAU, Jean, et Michelle LARIVEY (2000), Les émotions, source de vie, Montréal, ReD Éditeur.
	</cite></p>
	<p><cite>
		LARIVEY, Michelle (2002), La puissance des émotions, Montréal, Éditions de l'Homme.
	</cite></p>
	<p><cite>
		LELORD, François, et Christophe ANDRÉ (2001), La force des émotions, Paris, Éditions Odile Jacob.
	</cite></p>
	<p><cite>
		REDPSY (1997-2007), Auto-développement. Le site officiel de ressources en dévelopement. Les psychologues humanistes, [en ligne]. [www.redpsy.com] (août 2007)
	</cite></p>
	<p><cite>
		ROSENBERG, Marshall B. (1999), Les mots sont des fenêtres (ou des murs) : introduction à la communication non violente, Genève, Jouvence Éditions.
	</cite></p>
	<p><cite>
		SAMSON, Alain (2004). La vie est injuste (et alors ?), Montréal, Les Éditions Transcontinental.
	</cite></p>
</section>
<section id="annexe-maslow">
	<h1>Annexe: La pyramide de Maslow</h1>
	<p>
		Abraham Maslow propose, sous forme de pyramide, un schéma simple et utile pour comprendre la hiérarchie des besoins de l'être humain. Vous y trouverez une liste de base des besoins de l'être humain. Selon Maslow, une personne ne peut accéder à la satisfaction d'un besoin si ceux qui sont à la base, donc plus fondamentaux, ne sont pas raisonnablement satisfaits. Par exemple, un individu ne pourrait se sentir en sécurité (niveau 2) si sa première préoccupation est de trouver à boire et à manger (niveau 1).  (retour à la section « Que faire avec ses émotions ? »)
	</p>
	<img src="wp-content/uploads/2014/11/P_Maslow.jpg" alt="Pyramide de Maslow : A. Besoins physiologiques - B. Besoin de sécurité (physique et psychologique) - C. Besoin d'amour et d'appartenance - D. Besoin d'estime de soi - E. Besoin de réalisation de soi">
</section>
<footer>
	<h2>Rédiger par :</h2>
	<p>
		Josée Touchette, psychologue
	</p>
</footer>
