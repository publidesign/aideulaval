<nav>
	<ul>
		<li><a href="#autonomie" title="L'importance d'assumer son autonomie malgré la résistance de ses parents">L'importance d'assumer son autonomie malgré la résistance de ses parents</a></li>
		<li><a href="#exemples" title="Des exemples">Des exemples</a></li>
		<li><a href="#consequences" title="Conséquences à court et à long terme">Conséquences à court et à long terme</a></li>
		<li><a href="#expliquer" title="Comment expliquer ce type de relation ?">Comment expliquer ce type de relation ?</a></li>
		<li><a href="#que-faire" title="Que faire ? Changer la dynamique">Que faire ? Changer la dynamique</a></li>
		<li><a href="#references" title="Références">Références</a></li>
	</ul>
</nav>
<section id="autonomie">
	<h1>L'importance d'assumer son autonomie malgré la résistance de ses parents</h1>
	<p>
		Si vous avez l'impression de commettre un crime envers vos parents en devenant indépendant, indépendante ou en vous éloignant d'eux, ce qui suit s'adresse à vous.
	</p>
	<p>
		Dans chaque famille il y a parfois des conflits, des petites luttes de pouvoir ou des tensions. Ce sont des signes que les liens entre parents et enfants sont forts, riches et significatifs. D'une famille à l'autre, le type de lien et le mode relationnel est différent : chez certaines, les parents occupent une place très active dans la vie de leurs enfants en exerçant plus d'autorité et de contrôle, chez d'autres, les parents laissent davantage leurs enfants vivre leurs propres expériences et venir à eux quand ils en ont besoin. Le ou la jeune, en devenant adulte, change et tend à vouloir plus d'autonomie, tout en cherchant à protéger davantage son intimité, ce qui fait partie d'un processus normal de développement. Habituellement, les parents collaborent et comprennent ces changements, mais certains refusent de reconnaître le besoin d'autonomie grandissant de leur enfant. Ceci peut freiner l'émancipation du ou de la jeune adulte ou provoquer des conflits familiaux.
	</p>
	<p>
		L'attitude de parents indiscrets ou surprotecteurs peut paraître paradoxale et contradictoire : après avoir investi généreusement temps et argent dans l'éducation de leur enfant afin qu'il ou elle soit en mesure de réussir sa vie, ils semblent vouloir lui couper les ailes quand il ou elle veut assumer son autonomie à l'âge adulte. Dans cette situation, le ou la jeune doit combattre pour s'émanciper du nid familial où on le retient. Les médias ont beaucoup parlé du « phénomène Tanguy » pour désigner ces enfants devenus grands qui collent à la maison de leurs parents. L'inverse existe aussi et il arrive que des parents collent dans la vie de leurs enfants.
	</p>
	<p>
		La différence significative entre un lien familial sain et un lien malsain, c'est le degré de liberté qu'ont les membres d'une famille de s'exprimer comme individus. Une famille saine encourage la responsabilité personnelle, l'indépendance et l'individualité, tout en maintenant un lien solide. Elle aide l'enfant à cultiver et à assumer ses capacités et le respect de soi. Par contre, dans une famille malsaine et fusionnelle, les enfants doivent se conformer aux pensées, besoins et actions des parents, souvent sous peine de menaces financières, affectives ou émotives (Forward, 1991).
	</p>
</section>
<section id="exemples">
	<h1>Des exemples</h1>
	<p>
		La plupart des enfants subissent l'une ou l'autre de ces situations avec leurs parents. Mais selon leur fréquence et leur intensité, elles peuvent indiquer une situation malsaine et suggérer la nécessité d'apporter des ajustements. Il n'est pas toujours aisé de départager la limite entre ce qui appartient au territoire d'un ou d'une jeune et ce qui concerne ses parents. Des parents qui posent des questions à leur jeune et s'intéressent à sa vie expriment un amour dégagé. Mais quand ils cherchent à meubler leur vie par celle de leur enfant en voulant toujours tout savoir, cela lui impose une inconfortable pression qu'il ou elle n'a pas à subir. Voici quelques exemples :
	</p>
	<ul>
		<li>
			Une mère qui choisit les vêtements de sa fille et critique systématiquement ceux qu'elle a choisis sans sa participation. 
		</li>
		<li>
			Un père qui impose ses critères pour le choix d'un appartement ou pour sa décoration (sous prétexte qu'il paie le loyer). 
		</li>
		<li>
			Une mère qui fouine dans les affaires personnelles de son fils sous prétexte qu'elle doit y faire le ménage. 
		</li>
		<li>
			Des parents qui exigent que leur enfant, qui ne vit pas avec eux, les appelle régulièrement et le ou la font sentir coupable après trois jours de silence. 
		</li>
		<li>
			Des parents qui ouvrent le courrier de leur enfant sans sa permission. 
		</li>
		<li>
			Des parents qui s'insèrent excessivement dans les finances de leur fille et gèrent son budget. 
		</li>
		<li>
			Une mère ou un père qui dénigre systématiquement les choix amoureux de leur enfant. 
		</li>
		<li>
			Un des parents qui demande à son fils ou à sa fille de s'occuper des besoins émotionnels ou affectifs de l'autre parent. 
		</li>
		<li>
			Des parents qui imposent leurs idées dans le choix d'un programme d'étude ou d'une université et qui exercent des pressions financières ou émotives pour y soumettre leur fils ou leur fille. 
		</li>
		<li>
			Des parents qui trouvent des prétextes pour que leur enfant renonce à des projets (voyage, travail à l'extérieur) ou à des personnes (en amitié, en amour) qui pourraient l'éloigner d'eux, même si ces projets ou personnes semblent sains et adaptés à son âge. 
		</li>
		<li>
			Des parents qui profitent de leur apport financier au budget de leur enfant devenu ou devenue adulte pour s'immiscer dans ses affaires personnelles malgré sa résistance. 	
		</li>
		<li>
			Des parents qui se plaignent de vivre trop de tristesse ou de vide si leur enfant n'est pas suffisamment en contact avec eux, et ce, malgré des contacts réguliers. 
		</li>
		<li>
			Un ou une jeune adulte qui a le sentiment d'abandonner ses parents lorsqu'il ou elle les quitte temporairement.
		</li>
	</ul>
</section>
<section id="consequences">
	<h1>Conséquences à court et à long terme</h1>
	<p>
		Une telle dynamique relationnelle, installée depuis longtemps, peut avoir des effets négatifs sur votre bien-être psychologique et même avoir un impact sur votre santé physique. Une tension constante dans les rapports avec ses parents est souvent cachée et ce malaise peut entraîner de l'anxiété ou un état dépressif et des émotions comme la colère ou la tristesse. Un enfant qui doit mentir à propos de ses actions ou de ses sentiments pour éviter de déplaire à ses parents développe souvent un grand sentiment de culpabilité. Surtout s'il lui semble interdit de se garder un jardin secret. Cela peut aussi contribuer à forger des traits de personnalité qui bloquent le développement, tels le manque de confiance en soi, la difficulté à s'affirmer, la tendance à s'occuper des besoins des autres et la difficulté à identifier les siens ou une impression chronique de faiblesse et d'incompétence. Ces traits peuvent caractériser, entre autres, vos choix amoureux ou vos choix de carrière.
	</p>
	<p>
		Lorsque ces symptômes sont d'une intensité ou d'une fréquence telle qu'ils nuisent à votre réussite personnelle, la consultation professionnelle peut être envisagée.
	</p>
</section>
<section id="expliquer">
	<h1>Comment expliquer ce type de relation ?</h1>
	<p>
		Pour comprendre comment ce type de relation peut s'établir entre parents et enfants, il existe plusieurs hypothèses. Certaines concernent les parents, d'autres les enfants, sans oublier les raisons inhérentes au contexte social actuel.
	</p>
	<h2>&#9632; Le défi de la vingtaine : de l'identité à l'intimité</h2>
	<p>
		Dans le cadre de son développement personnel, après avoir intégré son identité durant son adolescence, le jeune adulte fait face au défi de vivre une intimité dans une relation amoureuse. Pour que cette relation soit intime, une exclusivité est nécessaire et implique un certain éloignement des autres personnes de son entourage, dont ses parents. Une crise (période de conflits intérieurs durant laquelle la personne examine ses propres valeurs et prend des décisions au sujet des rôles à adopter dans sa vie) est parfois nécessaire pour mieux définir son identité et adapter ses liens familiaux en conséquence. Le résultat de ces changements est l'expérience de l'intimité.
	</p>
	<h2>&#9632; Les baby-boomers devenus parents : confusion entre les rôles de parents et d'amis</h2>
	<p>
		Cette confusion est un phénomène remarqué par des sociologues (Ricard, 1992 ; Grand'Maison, 1992) concernant la génération des baby-boomers devenus parents. Beaucoup de parents de jeunes dans la vingtaine entretiennent des relations plutôt amicales avec leurs enfants. Dans une saine conquête de l'autonomie, un lien surtout axé sur l'amitié ou la réciprocité avec les parents crée un noeud. Quitter le giron familial quand un mur autoritaire parental est établi est plus facile que s'il n'y a aucun mur à traverser et que la relation avec les parents-amis ne semble pas avoir de limite ou de cadre défini. Aussi, les liens qui sont plus profonds et étendus entre les parents et leurs enfants sont plus difficiles à transformer. Pour une jeune de 20 ans par exemple, dire à sa mère, qui est aussi sa meilleure amie, qu'elle veut prendre ses distances et faire des choix seule est difficile car elle aura le sentiment, souvent fondé, de la blesser, voire de la trahir.
	</p>
	<h2>&#9632; Parents dominateurs</h2>
	<p>
		Cette confusion est un phénomène remarqué par des sociologues (Ricard, 1992 ; Grand'Maison, 1992) concernant la génération des baby-boomers devenus parents. Beaucoup de parents de jeunes dans la vingtaine entretiennent des relations plutôt amicales avec leurs enfants. Dans une saine conquête de l'autonomie, un lien surtout axé sur l'amitié ou la réciprocité avec les parents crée un noeud. Quitter le giron familial quand un mur autoritaire parental est établi est plus facile que s'il n'y a aucun mur à traverser et que la relation avec les parents-amis ne semble pas avoir de limite ou de cadre défini. Aussi, les liens qui sont plus profonds et étendus entre les parents et leurs enfants sont plus difficiles à transformer. Pour une jeune de 20 ans par exemple, dire à sa mère, qui est aussi sa meilleure amie, qu'elle veut prendre ses distances et faire des choix seule est difficile car elle aura le sentiment, souvent fondé, de la blesser, voire de la trahir.
	</p>
	<h2>&#9632; Syndrome du nid vide</h2>
	<p>
		Les parents qui voient partir ou s'éloigner leur enfant, surtout si c'est le ou la plus jeune, doivent faire face immédiatement à une nouvelle étape de leur propre vie. Si une grande part de leur identité est liée à leur rôle parental, ils se sentent abandonnés ou inutiles lorsque leur enfant devient indépendant ou indépendante (Forward, 1991). Après avoir consacré les vingt dernières années principalement à leurs enfants, ils doivent réapprendre à penser à eux. Bien que cela soit normal et dans l'ordre des choses, ce passage est une crise pour eux qui se voient vieillir et doivent s'adapter à de nouveaux rôles.
	</p>
	<h2>&#9632; Famille monoparentale (ou absence de communication dans un couple de parents)</h2>
	<p>
		Il arrive que les besoins affectifs d'un parent qui vit seul ou qui communique peu avec son partenaire ne soient pas comblés. Afin de combler ce manque, il peut avoir tendance à surinvestir son enfant. Une fois adulte, l'enfant se sent responsable de la solitude qu'il ou elle engendre en s'éloignant de ce parent. Son sentiment de culpabilité sera plus grand s'il ou elle subit un chantage affectif. 
	</p>
	<h2>&#9632; Maladie ou handicap chez l'un des parents</h2>
	<p>
		Les besoins particuliers d'un parent qui souffre d'une maladie chronique ou qui vit avec un handicap peuvent contribuer à créer une relation de dépendance envers l'enfant. Dans ce cas, l'enfant doit se demander à quel point elle ou il est prêt à assumer une responsabilité par rapport aux besoins particuliers de son parent et quelle aide il peut fournir.
	</p>
	<h2>&#9632; Mère ou père souffrant d'un problème d'anxiété</h2>
	<p>
		Souvent, les troubles d'anxiété ne sont pas diagnostiqués et, avec le temps, ils s'intègrent aux traits de personnalité. Par exemple, une mère qui a une tendance à l'obsession-compulsion peut devenir particulièrement contrôlante avec son fils ou sa fille. Elle acceptera plus difficilement l'éloignement de son enfant qui est maintenant adulte. Autre exemple, un père qui souffre d'anxiété généralisée peut transmettre implicitement le message à son fils ou à sa fille que la vie est dangereuse, qu'il faut faire attention et que ce qui est nouveau, le changement, est très menaçant. Une telle attitude provoque une double barrière à un sain départ de l'enfant qui veut voler de ses propres ailes car en plus de devoir faire fi des résistances de son père, il ou elle devra affronter ses propres craintes, issues d'une éducation marquée par la peur.
	</p>
	<h2>&#9632; Différences culturelles</h2>
	<p>
		Quand les parents sont originaires d'une autre culture ou s'ils ont des valeurs traditionnelles très différentes de celles propres à l'environnement social dans laquelle le ou la jeune adulte a grandi, un décalage important peut se faire entre le monde de l'enfant-adulte et les idéaux de ses parents.
	</p>
	<h2>&#9632; Manque d'affirmation de soi</h2>
	<p>
		Certaines personnes sont très tolérantes face aux besoins des autres et ont développé d'excellentes capacités d'adaptation. Ces qualités peuvent aussi provoquer une faible affirmation de soi. Une personne tolérante est sensible au bien-être de l'autre et à sa souffrance, au point parfois de négliger l'établissement de ses limites et de son autonomie par rapport aux gens de son réseau relationnel. Dans ce réseau, les parents sont souvent ceux avec qui il est le plus difficile de s'affirmer à cause d'habitudes de soumission bien ancrées.
	</p>
</section>
<section id="que-faire">
	<h1>Que faire ? Changer la dynamique</h1>
	<p>
		Si votre situation ressemble aux exemples ci-dessus, votre impression de malaise est justifiée et des changements sont nécessaires. Mais ces changements commencent par vous. Vous êtes les premiers à vous accorder le droit de vivre votre vie. Si vous attendez que vos parents se transforment et vous donnent la permission de vivre votre autonomie, vous risquez d'attendre longtemps et vous garderez le même type de comportement. Adulte, vous avez la possibilité de délimiter votre territoire.
	</p>
	<ul>
		<li>
			Établissez des limites claires. Par exemple, si vous ne prévoyez pas visiter vos parents une fin de semaine, dites « non » et évitez les « peut-être » qui vous feront sentir coupable de décevoir les attentes que l'on vous a fait sentir. 
		</li>
		<li>
			Si vous habitez chez vos parents, vous procurer un téléphone cellulaire pourrait vous permettre de gérer vous-mêmes vos appels sans risque d'ingérence de la part de vos parents.
		</li>
		<li>
			Affirmez ce que vous voulez faire, ne le demandez pas. Par exemple, dites : « je serai absent, absente en fin de semaine » plutôt que : « ça vous dérange si je pars toute la fin de semaine avec ma blonde, mon chum ? ».
		</li>
		<li>
			Apprenez à percevoir vos parents comme des adultes autonomes. À 50 ou 60 ans, en santé, ce ne sont ni des enfants ni des personnes malades. Ils sont capables de subvenir à leurs besoins.
		</li>
		<li>
			Évitez de répondre au chantage affectif. Votre indifférence à des propos tels que « tu préfères ta blonde, ton chum à ta mère... » ou « si tu n'as plus besoin de moi, je ne paierai plus tes études » rendra ces menaces inefficaces. Vous pouvez simplement expliquer que ce sont des choix importants pour vous. 
		</li>
		<li>
			Voyez les avantages que votre émancipation peut entraîner pour vous et pour eux. Vos parents devront réorganiser leur vie et leurs activités tôt ou tard. Ils ont la responsabilité de se divertir, de se faire des amis, de se trouver des occupations. 
		</li>
		<li>
			Tolérez votre impuissance vis-à-vis de vos parents. Votre sentiment de culpabilité ne leur rapporte rien. Vous devez vous habituer à tolérer votre impuissance devant le vide suscité par la distance que vous voulez établir. 
		</li>
		<li>
			Partez vivre une expérience à l'étranger. Cela vous aidera à développer votre confiance en vous. Votre absence prolongée - et temporaire - forcera vos parents à faire face à leur anxiété de vous savoir loin et ils ne pourront recourir à vous directement pour se rassurer. 
		</li>
		<li>
			La famille est le premier système à travers lequel une personne apprend à percevoir le monde. Identifiez les idées irrationnelles intégrées à votre famille afin de vous en différencier. Par exemple : 
			<ul>
				<li>
					les enfants sont responsables de leurs parents ;
				</li>
				<li>
					la présence des enfants est nécessaire et vous êtes mauvais si vous êtes absent ou absente ; 
				</li>
				<li>
					les seuls liens vraiment importants sont les liens familiaux ; 
				</li>
				<li>
					telle religion est la seule valable. 
				</li>
				<li>
					telle profession ou telle voiture est nécessaire pour prouver sa réussite.
				</li>
			</ul>
		</li>
		<li>
			Souvenez-vous de votre rôle. Vous n'êtes pas le parent de vos parents. Vous n'êtes pas responsable d'eux : ils sont autonomes et ont la responsabilité de leur propre vie.
		</li>
	</ul>
	<p>
		Ne vous découragez pas si les changements voulus ne surviennent pas immédiatement. Ce type de transformations demande beaucoup de temps car il vise des comportements qui sont installés depuis plusieurs années. Rappelez-vous que selon l'ordre naturel de la vie, c'est principalement à ses propres enfants que l'on redonne ce qu'on a reçu de ses parents. Après une transition mature et un respect réciproque du territoire, de l'autonomie et des choix de l'autre, un rapprochement est possible. Un lien où l'on respire bien peut devenir très agréable. 
	</p>
</section>
<section id="references">
	<h1>Références</h1>
	<p><cite>
		Forward, S. (1991), Parents toxiques : comment échapper à leur emprise, Paris, Éditions Stock. 
	</cite></p>
	<p><cite>
		Grand'Maison, J. (dir.) (1992), Vers un nouveau conflit de générations : profils sociaux et religieux des 20-35 ans : recherche-action, deuxième dossier, Montréal, Fides. 
	</cite></p>
	<p><cite>
		Osterkamp, L. (1992), How to deal with your parents when they still treat you like a child, New York, Berkley Books. 
	</cite></p>
	<p><cite>
		Ricard, F. (1992), La génération lyrique : essai sur la vie et l'oeuvre des premiers-nés du baby-boom, Montréal, Boréal. 
	</cite></p>
	<p><cite>
		Sheehy, G. (1982), Passages : les crises prévisibles de l'âge adulte, Boucherville (Qc), Éditions de Mortagne.
	</cite></p>
</section>
<footer>
	<h2>Rédiger par :</h2>
	<p>
		Marcel Bernier, psychologue
	</p>
</footer>
