<blockquote>
	Le passage dans l'inconnu est un acte de création.
	<cite>Michelle Tocher</cite>
</blockquote>
<p>
	Vous venez d'arriver à l'université, vous êtes là depuis quelques jours, quelques semaines ou quelques mois et déjà vous avez grande envie d'en repartir. Chaque jour qui passe vous fournit, vous semble-t-il, de nouvelles raisons pour faire votre valise... Vous vous sentez étranger, étrangère à ce nouveau milieu auquel vous ne parvenez pas à vous adapter. Vous regardez autour de vous et vous avez l'impression d'être seul ou seule dans cette situation... Pourtant, il n'en est pas ainsi. Chaque année, plusieurs sont tentés de quitter l'université pour ces mêmes motifs. Parmi eux, certains décident effectivement de quitter au cours de la première session. Que se passe-t-il ? Pourquoi en êtes-vous là ?
</p>
<nav>
	<ul>
		<li><a href="#suprises" title="Surprises et déceptions">Surprises et déceptions</a></li>
		<li><a href="#reactions" title="Des réactions à décoder">Des réactions à décoder</a></li>
		<li><a href="#moyens" title="Des moyens à prendre">Des moyens à prendre</a></li>
		<li><a href="#references" title="Références">Références</a></li>
	</ul>
</nav>
<section id="suprises">
	<h1>Surprises et déceptions</h1>
	<p>
		Pour plusieurs d'entre vous, l'arrivée à l'université était porteuse de grandes attentes et de beaux rêves à réaliser : étudier enfin des matières qui vous intéressent, vivre dans un univers stimulant, faire de belles rencontres, etc. Et voilà que la réalité dans laquelle vous vous retrouvez ressemble peu à ce que vous aviez imaginé :
	</p>
	<ul>
		<li>
			Certains cours diffèrent grandement de ce à quoi vous vous attendiez, soit par la nature de leurs contenus ou encore par la complexité de ceux-ci ;
		</li>
		<li>
			La lourdeur bureaucratique de l'appareil universitaire vous surprend. Vous êtes déconcerté ou déconcertée de vous voir refuser l'accès à certains cours auxquels vous teniez particulièrement ;
		</li>
		<li>
			L'organisation matérielle de votre nouvelle vie (habiter en appartement, trouver ou occuper un nouvel emploi, réorganiser vos finances, etc.) vous prend plus de temps et d'énergie que ce que vous aviez prévu, ne vous laissant pas le loisir de socialiser alors que vous en auriez grand besoin ; 
		</li>
		<li>
			Tout ce monde sur le campus et toutes ces idées nouvelles auxquelles vous vous heurtez ne vous donnent pas du tout envie de vous faire de nouveaux amis, à votre grand étonnement.
		</li>
	</ul>
	<p>
		Vous devenez alors bien nostalgique du temps de vos études collégiales et peu encline ou enclin à vous faire une place dans l'univers universitaire.
	</p>
	<p>
		Pour ces raisons ou pour bien d'autres, vous éprouvez un désappointement ou un découragement auquel vous ne vous attendiez pas. Dépendant de sa provenance, votre déception prendra diverses couleurs : vous verrez vos projets comme des rêves devenus irréalisables, vous vous sentirez bousculée ou bousculé dans vos valeurs, vous aurez l'impression d'avoir perdu beaucoup d'illusions, vous constaterez que vos désirs d'autonomie et vos idéaux de liberté en prennent un coup ou encore vous aurez le sentiment de devoir gravir une montagne sans y être vraiment préparé ou préparée.
	</p>
	<p>
		Bien entendu, vos frustrations, vos inquiétudes, vos nostalgies peuvent s'estomper rapidement, mais elles peuvent aussi se prolonger et vous risquez de réagir inadéquatement et de prendre, par la suite, des décisions inappropriées.
	</p>
</section>
<section id="reactions">
	<h1>Des réactions à décoder</h1>
	<p>
		Quand il est question d'adaptation, réagir inadéquatement peut prendre l'une des formes suivantes :
	</p>
	<h2>&#9632; Vous vous lancez à corps perdu dans le travail.</h2>
	<p>
		À court terme, ce comportement ne présentera pas trop d'inconvénients et aura même certains avantages, mais après quelques mois, il peut vous conduire à l'épuisement. 
	</p>
	<h2>&#9632; Vous reculez face aux efforts exigés.</h2>
	<p>
		Ce qui signifie : rater des cours, arriver en retard, se sentir fatigué ou fatiguée, étudier peu ou pas du tout. Ce faisant, vous prendrez rapidement un retard important, susceptible de faire naître en vous un sentiment d'incompétence et de découragement. 
	</p>
	<h2>&#9632; Vous vous isolez.</h2>
	<p>
		En agissant de cette façon, vous vous privez du support et de la collaboration que vos collègues peuvent vous apporter. Vous passez également à côté de plusieurs informations souvent essentielles pour comprendre votre nouveau milieu et décoder les règles qui le régissent. 
	</p>
	<h2>&#9632; Vous vous retirez intérieurement.</h2>
	<p>
		Ce retrait vous permet de fonctionner mais en apparence seulement. Vous devenez maîtres dans l'art de « faire semblant ». Comme cette position requiert beaucoup d'énergie pour être maintenue, il vous sera de plus en plus pénible de demeurer fonctionnelle ou fonctionnel. 
	</p>
	<h2>&#9632; Vous doutez de vos capacités.</h2>
	<p>
		Évidemment, ces doutes entraînent la peur de ne pas réussir et la tentation d'abandonner qui l'accompagne souvent. Cette réaction est davantage probable si vous faites partie de la catégorie des gens performants ou perfectionnistes. 
	</p>
	<h2>&#9632; Vous doutez de votre choix ou de votre place à l'université.</h2>
	<p>
		Les exigences reliées à l'adaptation peuvent devenir des prismes déformants qui embrouillent vos perceptions, comme si vous regardiez un paysage à travers un brouillard. Cette vision risque évidemment de fausser votre jugement quant à la pertinence du programme choisi ou de votre présence à l'université.
	</p>
	<p>
		Si vous vous reconnaissez dans l'une ou l'autre des réactions précédentes, c'est probablement que vous vivez une « surcharge » au plan des changements à effectuer et que votre façon habituelle de composer avec ceux-ci est cette fois mise à plus rude épreuve. Divers facteurs personnels et contextuels peuvent faire en sorte qu'il en soit ainsi (vous avez quitté votre ville pour la première fois, vous ne connaissez personne de votre programme, vous n'êtes pas familier ou familière avec la culture québécoise, vous avez facilement tendance à vous ennuyer, vous éprouviez des difficultés scolaires au cégep, etc.). Quelle que soit votre situation, la première chose à faire est d'éviter de paniquer. Même si la mer est un peu plus ou même fortement plus houleuse pour vous que pour les autres, cela ne signifie surtout pas que vous n'êtes pas un bon capitaine et que vous n'avez pas pris le bon bateau.
	</p>
</section>
<section id="moyens">
	<h1>Des moyens à prendre</h1>
	<p>
		Pour éviter de paniquer et de prendre des décisions non fondées telles changer de programme ou tout abandonner trop rapidement, voici quelques conseils susceptibles de vous aider à y voir plus clair :
	</p>
	<h2>&#9632; Donnez-vous du temps.</h2>
	<p>
		En premier lieu, il est grandement souhaitable de comprendre ce qui vous arrive vraiment. Bien sûr, comprendre votre situation ne sera pas suffisant, mais cela vous rendra plus disponible pour une recherche efficace de solutions aux problèmes que vous aurez identifiés. Accordez-vous aussi du temps pour prendre de l'expérience ; ce temps est vraiment essentiel quand on réalise tout ce qu'implique l'adaptation, ne serait-ce qu'au plan scolaire. En effet, seulement à ce point de vue, il faut : répondre à de nouvelles exigences d'études ; s'habituer à une organisation scolaire différente et à des approches pédagogiques variées ; composer avec des charges de travail plus grandes et moins délimitées, de même qu'avec des contenus d'apprentissage plus complexes ; fonctionner dans un climat souvent plus compétitif et plus impersonnel ; établir une bonne gestion de son temps. Comme vous pouvez le constater, à cause de l'importance des changements que vous devez effectuer, retrouver votre aisance antérieure ne pourra pas se faire rapidement. Toutefois, vous serez progressivement en mesure de distinguer l'essentiel, d'établir vos priorités, de diriger vos efforts, d'installer des routines et d'apporter les correctifs nécessaires aux problèmes que vous rencontrez. Une session entière peut s'avérer nécessaire pour évaluer votre situation adéquatement.
	</p>
	<h2>&#9632; Gardez un lien continu avec la réalité universitaire.</h2>
	<p>
		Cette continuité est de nature à renforcer le lien encore ténu qui vous rattache à votre nouvelle vie étudiante à l'université. Ainsi, conserver le « rythme » en évitant, au début surtout, les coupures trop fréquentes ou trop longues vous permettra d'être plus rapidement à l'aise avec vos nouveaux points de repère et de tourner plus facilement la page. Concrètement, cela veut dire qu'au cours de la première session surtout, il est important de fréquenter le campus le plus régulièrement possible et de ne pas prolonger indûment les congés, les retours à votre nouvelle réalité pouvant s'avérer encore plus difficiles.
	</p>
	<h2>&#9632; Ouvrez-vous.</h2>
	<p>
		Dans cette transition comme dans d'autres, partagez ce que vous ressentez avec une ou quelques personnes en qui vous avez confiance. Cela vous aidera à dédramatiser votre situation et à reprendre du pouvoir sur celle-ci. En prime, vous expérimenterez sans doute, une fois de plus, les bienfaits de l'entraide et de la collaboration.
	</p>
	<p>
		En fin de compte, pour contrer la tentation d'abandonner vos études, l'essentiel est de bien décoder ce qui vous arrive, d'agir en conséquence et de donner un sens exact aux difficultés rencontrées. Ces difficultés demeureront ainsi ce qu'elles sont la plupart du temps, c'est-à-dire passagères.
	</p>
	<p>
		Vous pouvez consulter aussi:
	</p>
	<ul>
		<li>
			<a href="{{LIEN}}motifs-de-choix" title="Approfondir les motifs de son choix" target="_blank">Approfondir les motifs de son choix</a>
		</li>
		<li>
			<a href="{{LIEN}}strategies" title="La section sur les stratégies d'apprentissage" target="_blank">La section sur les stratégies d'apprentissage</a>
		</li>
		<li>
			Un texte sur la confiance en soi.
		</li>
	</ul>
</section>
<section id="references">
	<h1>Références</h1>
	<p><cite>
		COULON, Alain. « Le métier d'étudiant », Presses universitaires de France, Paris, 1997. 
	</cite></p>
	<p><cite>
		DUPONT, Pol et OSSANDO, Marcelo. « La pédagogie universitaire », Presses universitaires de France, Paris, 1994. 
	</cite></p>
	<p><cite>
		ELLIS, Dave. « La clé du savoir », Houghton Miffin Company, Ontario, 1994. 
	</cite></p>
	<p><cite>
		HOFMANN-NEMIROF, Greta. « Transitions », Harcourt, Brace & Company, Canada, 1994. 
	</cite></p>
</section>
<footer>
	<h2>Rédiger par :</h2>
	<p>
		Louise Turgeon, conseillère d'orientation
	</p>
</footer>