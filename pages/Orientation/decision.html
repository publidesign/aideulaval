<blockquote>
	Rien ne commence vraiment que si on se le permet à soi-même.
	<cite>Claude Mauriac</cite>
</blockquote>
<p>
	Après avoir choisi votre programme, vous avez probablement ressenti un grand soulagement. Cette décision a mis fin à une période d'incertitude (ou l'a réduite grandement) et vous a donné l'énergie nécessaire pour entreprendre cette nouvelle aventure.
</p>
<p>
	Toutefois, ce choix est loin d'être le dernier en ce qui concerne votre carrière. En cours de formation, vous aurez d'autres décisions à prendre pour préciser ou corriger votre trajectoire : choix d'une spécialisation à l'intérieur de votre programme, choix entre deux milieux de stage ou deux emplois, choix de prolonger ses études au 2e cycle, sans compter la remise en question possible de votre programme, etc. Vous sentez-vous bien outillés pour faire face à ces carrefours ?
</p>
<p>
	Plusieurs décisions se prennent au quotidien sans nécessiter une grande réflexion. Certaines, par contre, exigent une analyse plus poussée, parce que les enjeux personnels sont plus importants, que la situation est plus complexe ou que les options qui s'offrent à vous sont toutes aussi attrayantes les unes que les autres. Notre façon habituelle de décider peut alors s'avérer insuffisante et nous exposer à des déboires plutôt qu'à la satisfaction escomptée. Pour mieux vous outiller face à ce deuxième type de décisions, voici une méthode de résolution de problèmes éprouvée , pouvant s'appliquer à des décisions de toutes sortes. À partir de la pratique de cette méthode, vous pourrez développer votre confiance et parvenir à des décisions constructives pour vous.
</p>
<nav>
	<ul>
		<li><a href="#quatre-points" title="$Une méthode en quatre points">Une méthode en quatre points</a></li>
		<li><a href="#methode" title="Utilisation de la méthode">Utilisation de la méthode</a></li>
		<li><a href="#decision" title="Indices d'une bonne décision">Indices d'une bonne décision</a></li>
		<li><a href="#incertitude" title="Si l'incertitude persiste...">Si l'incertitude persiste...</a></li>
		<li><a href="#references" title="Références">Références</a></li>
		<li><a href="#complementaire" title="Contenu complémentaire">Contenu complémentaire</a></li>
	</ul>
</nav>
<section id="quatre-points">
	<h1>Une méthode en quatre points</h1>
	<p>
		La méthode présentée comporte quatre étapes essentielles :
	</p>
	<h2>1. Préciser le problème et recueillir tous les faits.</h2>
	<p>
		Il s'agit d'abord de bien cerner le problème en identifiant les divers éléments de la situation et de votre réalité dans cette situation (insatisfactions, malaises, aspirations, priorités, besoins, etc.). Cette étape est importante parce qu'elle aide à préciser avec plus de clarté ce sur quoi doit porter la décision à prendre. Elle est préalable à la recherche de solutions. Selon la nature de la décision à prendre, la cueillette peut concerner autant des faits extérieurs que des éléments d'information ou encore de connaissance de soi qui sont significatifs pour la compréhension et la résolution du problème. Par exemple, y a-t-il une date de tombée pour votre décision ? Quels sont vos critères prioritaires ou les objectifs que vous souhaitez atteindre ? Avez-vous des contraintes fermes dont vous devez tenir compte ?
	</p>
	<h2>2. Identifier toutes les solutions possibles.</h2>
	<p>
		Il s'agit ensuite d'utiliser votre imagination pour élaborer des solutions. Vous devez essayer de dépasser le connu pour vous ouvrir à de nouvelles avenues et élargir le champ de vos possibilités. Il faut faire attention de ne pas éliminer tout de suite des options qui vous paraissent osées ; elles peuvent contenir des éléments intéressants. Vous devez vous permettre de vivre une certaine désorganisation dans votre perception de vous-même. Un travail insuffisant à cette étape risque de vous enfermer dans votre problème, surtout si vous vous en tenez aux seules solutions connues, mais insatisfaisantes. Pour chacune des solutions envisagées, vous appliquez ensuite les étapes 3 et 4.
	</p>
	<h2>3. Analyser les conséquences de chaque option.</h2>
	<p>
		À cette étape, vous établissez les avantages et inconvénients que comporte chaque scénario. Vous identifiez également les difficultés que vous prévoyez rencontrer dans leur mise en oeuvre. Seront-elles surmontables, si oui, comment ? Sinon, pouvez-vous prévoir un plan de rechange ? Cette étape fait appel à votre sens logique et à votre capacité d'évaluation. Passer outre à cette étape peut vous laisser bien démuni ou démunie devant les imprévus ou les obstacles qui surgiront dans la réalisation de votre choix.
	</p>
	<h2>4. Mesurer l'impact sur vous et sur les personnes importantes pour vous.</h2>
	<p>
		Cette solution est-elle en accord avec vos besoins, vos valeurs, vos critères personnels ? À quoi vous oblige-t-elle à renoncer ? Ces renoncements représentent-ils un compromis trop important ? Quel impact cette solution risque-t-elle d'avoir sur votre conjoint, votre réseau d'amis, vos proches ? Vous sentez-vous la force de vivre cette décision ? Cette étape requiert d'être ouverte et ouvert à ce que vous ressentez. Si vous la négligez, vous courez le risque de prendre des décisions qui ne seront pas assez enracinées en vous, qui pourront être difficiles à assumer ou encore qui susciteront des conflits inattendus avec vos proches.
	</p>
</section>
<section id="methode">
	<h1>Utilisation de la méthode</h1>
	<h2>Processus circulaire</h2>
	<p>
		Ces quatre tâches sont présentées de façon séquentielle, mais en réalité, ce processus est plutôt circulaire. Il est possible en tout temps de revenir à une étape antérieure pour compléter l'analyse. Par exemple, une personne qui n'a généré que peu de solutions pourra revenir à la deuxième étape si les solutions qu'elle a analysées se révèlent insatisfaisantes. L'important est de s'assurer de traverser toutes les étapes. 
	</p>
	<img src="wp-content/uploads/2014/11/processus.gif" alt="Processus circulaire">
	<h2>Faire attention à sa zone aveugle</h2>
	<p>
		Selon vos caractéristiques personnelles, vous aurez naturellement tendance à vous attarder plus longuement sur une phase et à en négliger inconsciemment une autre. 
	</p>
	<p>
		Ainsi, une personne rationnelle pourra privilégier l'analyse des conséquences logiques et éluder la phase consistant à mesurer l'impact de sa décision sur elle-même et sur les autres. Pour illustrer cela, on peut penser à une personne qui est tentée d'accepter un transfert dans une autre ville pour les avantages de la promotion, du salaire, de la progression de sa carrière, sans s'arrêter suffisamment aux effets qu'aura ce déracinement sur sa famille. Une personne intuitive et imaginative, stimulée à la perspective de générer des possibilités, pourra quant à elle avoir tendance à négliger la collecte des faits : pensez par exemple à la personne qui s'emballe devant un beau projet sans se demander quel est le budget disponible ou si elle peut effectivement ajouter ce projet à son horaire.
	</p>
	<p>
		Il est donc souhaitable de s'exercer à pratiquer systématiquement ces étapes dans les décisions qui constituent des enjeux importants pour vous. 
	</p>
</section>
<section id="decision">
	<h1>Indices d'une bonne décision</h1>
	<p>
		S'il est impossible d'être certain à 100 % de vos choix, il est néanmoins possible d'obtenir un résultat satisfaisant, même s'il peut subsister une certaine anxiété face à l'inconnu.
	</p>
	<p>
		Une bonne décision vous apparaîtra comme un bon ajustement au problème rencontré, en fonction des éléments dont vous disposiez à ce moment-là. Elle vous laissera dans un état d'harmonie face à la situation. Cette décision ouvrira un espace d'action et vous vous sentirez en mesure de vous engager dans sa réalisation et d'assumer les difficultés qui pourraient surgir. Vous aurez le sentiment qu'elle vous construit.
	</p>
	<p>
		Il ne faut pas oublier qu'une décision se valide au contact de la réalité. En la mettant en oeuvre, vous aurez de nouvelles informations sur vous, sur la situation. Vos décisions peuvent toujours être réajustées en cours de route.
	</p>
</section>
<section id="incertitude">
	<h1>Si l'incertitude persiste...</h1>
	<p>
		Si vous ne parvenez pas à une solution satisfaisante, y en a-t-il tout de même une vers laquelle vous inclinez, même légèrement ? Quels risques courez-vous à la prendre ? N'oubliez pas que le fait de ne pas prendre de décision est aussi une décision. Il se pourrait également que certains éléments en vous ou dans la situation ne soient pas suffisamment mûrs pour que vous puissiez trouver une solution. Il faut parfois savoir attendre, tout en se demandant s'il y aurait des actions à tenter durant cette période pour faire évoluer la situation. Dans les cas urgents qui requièrent une décision rapide, soupesez les risques de chaque solution et choisissez celle qui vous apparaît la moins mauvaise et pour laquelle vous vous sentez en mesure d'assumer les conséquences.
	</p>
	<p>
		Dans les situations difficiles, il peut aussi être utile de faire appel aux autres. Ils n'ont pas à décider à votre place, mais ils peuvent vous écouter, vous rappeler vos forces, vous donner des pistes de solutions, vous refléter vos attitudes ou vous faire voir le problème autrement. Il demeure que la décision vous appartient. Vous êtes la personne la mieux placée pour sentir ce qui est le meilleur pour vous.
	</p>
	<p>
		Cela dit, il demeure que prendre des décisions est difficile, car choisir implique toujours de renoncer à quelque chose, surtout lorsqu'il s'agit d'un choix professionnel. Vous ne pourrez jamais savoir comment aurait pu être la suite des choses si vous aviez agi autrement. Il faut aussi savoir faire votre deuil du choix parfait, les compromis étant parfois nécessaires pour composer avec certaines de nos limites ou avec les éléments de la réalité sur lesquels nous avons moins le contrôle. De plus, la méthode présentée n'empêche en rien qu'une part d'irrationalité puisse entrer dans nos décisions. Prendre le risque de vos aspirations n'y fait pas exception.
	</p>
	<p>
		Finalement, comme les décisions importantes succèdent souvent à des moments plus ou moins longs de remise en question et d'indécision, il peut être utile de consulter également les textes suivants :
	</p>
	<ol>
		<li><a href="{{LIEN}}insouciance" title="Sortir de l'insouciance" target="_blank">Sortir de l'insouciance</a></li>
		<li><a href="{{LIEN}}remise-en-question" title="Sortir des remises en question" target="_blank">Sortir des remises en question</a></li>
		<li><a href="{{LIEN}}indecision-chronique" title="Sortir de l'indécision chronique" target="_blank">Sortir de l'indécision chronique</a></li>
	</ol>
	<p>Bonnes décisions !</p>
</section>
<section id="references">
	<h1>Références</h1>
	<p><cite>
		Fournier, Geneviève. Interagir, une stratégie efficace d'orientation et d'insertion professionnelle. Cahier d'intégration no 1 : la prise de décision. Sainte-Foy, Éditions Septembre, Presses de l'Université Laval, 1995. 
	</cite></p>
	<p><cite>
		Personnalité et relations humaines (PRH). Règles pour un discernement - Note d'observations. Poitiers, 1981 (document non publié).
	</cite></p>
</section>
<section id="complementaire">
	<h1>Contenu complémentaire</h1>
	<p><cite>
		Falardeau, Isabelle. Sortir de l'indécision. Québec, Septembre Éditeur, 2007.
	</cite></p>
	<p><cite>
		Johnson, Spencer. Oui ou non?; l'art de prendre de bonnes décisions. Neuilly-sur-Seine, Michel Lafon, 2004.
	</cite></p>
</section>
<footer>
	<h2>Rédiger par :</h2>
	<p>
		France Lehoux, conseillère d'orientation
	</p>
</footer>
