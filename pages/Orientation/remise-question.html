<blockquote>
	Décider... c'est passer du conditionnel à l'indicatif, de l'imaginaire au réel, de la délibération à l'action. Moment de crise et de résolution.
	<cite>André Comte-Sponville</cite>
</blockquote>
<p>
	Vous en êtes à votre première, deuxième, troisième session ou plus dans votre programme et vos résultats scolaires sont satisfaisants. Pourtant, il vous arrive de vous demander, avec une certaine inquiétude, si vous avez fait le bon choix...
</p>
<p>
	Si ce questionnement s'avère passager, c'est qu'il était probablement lié à une période de plus grande fatigue ou encore à une situation personnelle difficile qui vous a fait douter de tout, comme par exemple une rupture amoureuse. Mais si, au contraire, votre remise en question persiste ou refait régulièrement surface, alors il est important de s'y arrêter et de prendre le temps de bien comprendre d'abord de quoi il en retourne.
</p>
<nav>
	<ul>
		<li><a href="#malaise" title="Clarifier son malaise">Clarifier son malaise</a></li>
		<li><a href="#attitudes" title="Adopter des attitudes aidantes">Adopter des attitudes aidantes</a></li>
		<li><a href="#references" title="Références">Références</a></li>
		<li><a href="#complementaire" title="Contenu complémentaire">Contenu complémentaire</a></li>
	</ul>
</nav>
<section id="malaise">
	<h1>Clarifier son malaise</h1>
	<p>
		Plusieurs éléments peuvent entrer en ligne de compte dans votre remise en question. Il est probable que vous vous reconnaîtrez dans l'un ou l'autre des énoncés suivants :
	</p>
	<ul>
		<li>
			Vous avez le sentiment d'avoir effectué un choix insuffisamment réfléchi, d'avoir plutôt agi à la dernière minute. Cette manière de procéder vous laisse maintenant avec l'impression que vous n'avez pas tenu compte suffisamment de ce que vous êtes, ni de ce qui vous est accessible. Bref, vous n'avez pas fait le tour du jardin. 
		</li>
		<li>
			Vous êtes déçue ou déçu du programme choisi. Vous trouvez les cours inintéressants, trop théoriques et ne voyez pas bien où tout cela vous mènera. En somme, il y a un écart important entre vos attentes et la réalité. Un ajustement s'impose, ajustement qui passera inévitablement par une remise en question à la fois de vos objectifs et de ceux de votre programme. 
		</li>
		<li>
			Vous n'arrivez pas à identifier clairement les buts que vous poursuivez. Vos perspectives professionnelles demeurent vagues. Alors, dans les périodes plus exigeantes comme les périodes d'examens, le doute quant à la pertinence de votre choix peut s'installer rapidement, étant donné que vous ne savez pas vraiment pourquoi vous êtes là. 
		</li>
		<li>
			Vous n'avez pas la certitude que vos capacités intellectuelles ou vos acquis antérieurs vous permettront de rencontrer les exigences scolaires du programme choisi. Ces doutes, puisqu'ils affectent négativement votre motivation aux études, peuvent faire naître en vous l'idée de changer de programme ou de quitter l'université, et ce, même quand il n'y a probablement pas lieu d'envisager ces hypothèses. Il est parfois plus tentant de changer de bicyclette que d'apprendre à bien en faire; seulement vous risquez de faire face au même problème éventuellement. 
		</li>
		<li>
			Vous avez peur de vous être trompée ou trompé parce que vous n'êtes pas sûr ou sûre que votre choix actuel vous conviendra toujours... Évidemment, se donner comme critère d'être à l'aise toute sa vie dans la profession choisie devient très propice aux remises en question, alors que le changement est inhérent à l'existence et que la réalité professionnelle est faite d'incertitudes. En somme, cela revient à exiger de vous l'impossible... 
		</li>
		<li>
			Vous avez des potentialités multiples et vous saviez que des rôles professionnels parfois très différents étaient possibles pour vous. Par exemple, vous avez choisi d'être artiste plutôt que scientifique (ou l'inverse), mais peut-être vous est-il encore pénible de vivre avec les renoncements importants que ce choix représente pour vous. 
		</li>
		<li>
			Vous êtes à l'aise avec le choix que vous avez fait, mais comme celui-ci comporte à la fois des aspects attrayants et d'autres moins désirables ou même limitatifs, il vous arrive de vous arrêter surtout à ces derniers aspects. En ces moments-là, d'autres voies peuvent vous sembler plus souhaitables. 
		</li>
		<li>
			Vous n'êtes pas encore convaincu ou convaincue que votre choix est vraiment le vôtre. Vous avez peut-être l'impression d'avoir été trop influencée ou influencé, dans un sens ou dans l'autre, par vos proches. À cet égard, il peut vous arriver de vous demander si vous auriez choisi d'être avocate, enseignant, ingénieure ou médecin si plusieurs membres de votre famille n'exerçaient pas déjà la même profession. Au contraire, avez-vous parfois l'impression d'avoir renoncé à une profession « de famille » parce que cela vous semblait trop facile ou trop difficile d'emprunter le même chemin ? Peut-être aussi étiez-vous en désaccord avec l'image que vos proches vous ont donnée de telle ou telle profession. En ce cas, vous pouvez maintenant vous inquiéter à savoir si vous n'avez pas jeté le bébé avec l'eau du bain. Il se peut aussi que votre entourage ait exercé de fortes pressions sur vous en faveur de tel ou tel choix. Que vous ayez ou non accepté ces contraintes, votre sentiment de ne pas avoir décidé librement peut être le même aujourd'hui. 
		</li>
		<li>
			Vous avez l'impression depuis le début d'être différente ou différent des autres étudiants et étudiantes de votre programme. Vous n'avez pas les mêmes intérêts ni les mêmes objectifs que vos collègues. Vous vous sentez souvent isolé ou isolée... À mesure que le temps passe, vos différences vous inquiètent davantage et votre isolement s'accroît. Ce malaise peut alors devenir porteur de doutes. 
		</li>
		<li>
			Vous êtes en conflit avec vos pairs ou avec vos professeurs ou professeures. Si cette situation se prolonge, il n'y aura plus de qualité de vie possible pour vous. Remettre votre choix en cause deviendra presque inévitable et quitter vous semblera alors la seule solution.
		</li>
	</ul>
</section>
<section id="attitudes">
	<h1>Adopter des attitudes aidantes</h1>
	<p>
		Quels que soient vos motifs pour remettre votre choix en question, vous pouvez trouver des réponses satisfaisantes à vos interrogations. Pour ce faire, il est essentiel d'adopter des attitudes et des comportements susceptibles de transformer ces périodes d'inquiétude en occasions de croissance personnelle et professionnelle. Voici quelques conseils en ce sens :
	</p>
	<ul>
		<li>
			Tenez compte des doutes qui vous habitent. Faire « comme si de rien n'était » ne vous servirait à rien. Tôt ou tard, ces doutes réapparaîtront et il vous sera alors peut-être plus difficile d'y faire face.
		</li>
		<li>
			Identifiez clairement ce qui cause votre remise en question avant d'agir. Il peut être tout aussi approprié de poursuivre dans la même voie que de choisir de changer. 
		</li>
		<li>
			Ayez confiance en vos ressources intérieures. Elles vous permettront de trouver les moyens d'action nécessaires pour résoudre vos difficultés. 
		</li>
		<li>
			Accordez-vous du temps. S'il est utile d'agir, il ne l'est pas d'agir précipitamment. Développez plutôt votre tolérance à l'incertitude (c'est-à-dire à ne pas trouver rapidement de réponses à vos interrogations) pour vous offrir la possibilité d'une réflexion sérieuse et enrichissante.
		</li>
		<li>
			Investissez-vous. Que ce soit pour préciser ou améliorer la connaissance que vous avez de vous-même, pour déterminer vos objectifs ou pour régler un conflit intra ou interpersonnel, le travail à faire demande du temps, de l'énergie et de l'implication. 
		</li>
		<li>
			Référez-vous à vos critères personnels pour évaluer votre situation et faire un autre choix s'il y a lieu. L'heure est à l'affirmation. Il est primordial de vous respecter, surtout si vous avez le sentiment que vous n'avez pas réussi à le faire jusqu'à présent. Bien sûr, vous pouvez discuter de votre situation avec des personnes de confiance, mais à la condition d'utiliser leurs points de vue pour mieux cerner vos besoins et vos aspirations. 
		</li>
		<li>
			Soyez vigilante ou vigilant. C'est-à-dire demeurez en éveil face à votre situation, sans pour autant vous alarmer. Cette attitude vous permettra de rester calme, de vous sentir en contrôle de ce qui vous arrive et de bien saisir les enjeux de vos questionnements.
		</li>
	</ul>
	<p>
		Comme vous pourrez le constater, agir ainsi vous facilitera grandement la tâche. De plus, vous pourrez sans risque en prendre l'habitude car cela vous sera utile en de nombreuses circonstances.
	</p>
</section>
<section id="references">
	<h1>Références</h1>
	<p><cite>
		HAMEL, Henri. « L'indécision vocationnelle chez les étudiants(es) de niveau collégial et universitaire : éléments de diagnostic », Connat, no 8, 1985, p. 187-209. 
	</cite></p>
	<p><cite>
		DOSNON, Odile. « L'indécision face au choix scolaire et professionnel : concepts et mesures », L'orientation scolaire et professionnelle, no 1, 1996, 25 p. 129-168.
	</cite></p>
</section>
<section id="complementaire">
	<h1>Contenu complémentaire</h1>
	<p><cite>
		FALARDEAU, Isabelle. « Sortir de l'indécision », Québec, Septembre Éditeur, 2007.
	</cite></p>
	<p><cite>
		FALARDEAU, Isabelle et ROY, Roland. « S'orienter malgré l'indécision », Éditions Septembre, Sainte-Foy, 1999. 
	</cite></p>
	<p><cite>
		ROBERGE, Michelle. « Tant d'hiver au coeur du changement », Éditions Septembre, Sainte-Foy, 1998. 
	</cite></p>
	<p><cite>
		PROULX, Suzanne. « Changer sans tout casser », Éditions du Méridien, Montréal, 1986.
	</cite></p>
</section>
<footer>
	<h2>Rédiger par :</h2>
	<p>
		Louise Turgeon, conseillère d'orientation
	</p>
</footer>