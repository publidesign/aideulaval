<blockquote>
	Avoir du talent, c'est avoir foi en soi-même, en ses propres forces.
	<cite>Maxime Gorki</cite>
</blockquote>
<p>
	Vous souhaitez fort probablement, au terme de votre formation, pouvoir exercer avec professionnalisme votre futur métier et affronter avec succès les divers défis que vous rencontrerez. Hélas, le sentiment de compétence n'accompagne pas automatiquement l'obtention d'un diplôme.
</p>
<p>
	Bien sûr, l'intégration des connaissances propres à votre champ d'études contribue au sentiment de compétence, mais ce dernier repose aussi sur la combinaison d'autres facteurs, à la fois subjectifs et objectifs. On peut nommer entre autres la confiance en soi, des attentes réalistes face à vos accomplissements, des réussites répétées dans un domaine particulier et les opportunités de faire vos preuves que vous offrent les milieux où vous évoluez. L'interaction de ces ingrédients favorise le développement d'un sentiment d'efficacité personnelle devant des tâches déterminées et donne envie d'aller plus loin dans le développement de vos potentialités.
</p>
<p>
	L'université est un lieu de transformation personnelle vous permettant de cultiver une multitude de compétences. Vous tirerez profit à cumuler plusieurs d'entre elles pour augmenter votre confiance, en cours d'études et sur le marché du travail. Voici quatre grandes catégories de compétences susceptibles d'être développées ou améliorées à l'université.
</p>
<nav>
	<ul>
		<li><a href="#types" title="Types de compétences exercées à l'université">Types de compétences exercées à l'université</a></li>
		<li><a href="#accentuer" title="Que pouvez-vous faire pour accentuer votre sentiment de compétence ?">Que pouvez-vous faire pour accentuer votre sentiment de compétence ?</a></li>
		<li><a href="#echec" title="Que faire si vous rencontrez l'échec en cours de route ?">Que faire si vous rencontrez l'échec en cours de route ?</a></li>
		<li><a href="#references" title="Références">Références</a></li>
		<li><a href="#complementaire" title="Contenu complémentaire">Contenu complémentaire</a></li>
	</ul>
</nav>
<section id="types">
	<h1>Types de compétences exercées à l'université</h1>
	<h2>&#9632; Scolaires</h2>
	<p>
		Au cours de vos études antérieures, vous devriez avoir acquis certaines méthodes de travail et stratégies d'études. À l'université, la maîtrise de ces habiletés est cruciale, car vous devez fonctionner de façon beaucoup plus autonome. Savoir gérer son temps, prendre des notes, lire efficacement, étudier, rédiger des rapports, utiliser les bibliothèques sont quelques-unes de ces habiletés essentielles. Beaucoup d'étudiantes et d'étudiants n'ont pas pris la peine de développer de bonnes méthodes de travail au cégep et font le constat douloureux de leurs lacunes à l'université. Il n'est jamais trop tard pour corriger ces lacunes, car c'est sur ces habiletés méthodologiques de base que peuvent s'épanouir les habiletés intellectuelles. Notre secteur « <a href="{{LIEN}}reussite-universitaire" title="Réussite universitaire" target="_blank">Réussite universitaire</a> » peut vous aider à en faire l'évaluation et à y rémédier, s'il y a lieu.
	</p>
	<h2>&#9632; Coginitives</h2>
	<p>
		Il s'agit d'un éventail de capacités mentales qui vous permettent de tendre de plus en plus vers l'indépendance intellectuelle. Une bonne part des compétences acquises à l'université sont de cette nature. Penser, catégoriser, analyser, émettre des hypothèses, faire des liens, évaluer, synthétiser, vulgariser sont les principales activités de toute démarche scientifique et intellectuelle. Le développement de vos compétences cognitives se poursuit sur une longue période dépassant le temps de la scolarisation. Les étudiantes et étudiants négligent parfois l'importance de ces habiletés intellectuelles. Souvent très attirés par les applications pratiques dans leur domaine, ils ont hâte aux stages et autres occasions de faire des liens entre la théorie et la pratique. Certes ces réalisations concrètes sont essentielles pour gagner confiance. Mais étant donné qu'aucune situation n'est jamais tout à fait semblable à celles vécues précédemment, c'est souvent la confiance en votre capacité de réfléchir qui devient le pivot de votre sentiment de compétence dans nombre de situations professionnelles. Le sociologue <cite>Edgar Morin</cite> a appelé cela <q>une tête bien faite</q>.
	</p>
	<h2>&#9632; Spécialisées</h2>
	<p>
		Ces compétences concernent la compréhension des connaissances fondamentales et des problématiques de votre discipline, ainsi que l'intégration des méthodes de travail qui lui sont propres. Votre engagement dans vos études et dans des stages contribue à cette intégration graduelle. Votre diplôme fait foi de ces compétences et les employeurs et employeuses s'attendent à ce que vous puissiez vous référer assez aisément à ce bagage. Chaque programme favorise le développement de compétences spécifiques essentielles à l'exercice d'une profession. Pour les identifier, vous pouvez consulter les objectifs des programmes ou vous informer auprès de votre directeur ou directrice ou auprès de diplômées ou diplômés en contexte professionnel.
	</p>
	<h2>&#9632; Personnelles ou génériques</h2>
	<p>
		Ces compétences reflètent votre habileté à gérer votre vie et à entretenir des rapports avec les autres. Elles sont acquises dans une variété de situations de vie (emplois, bénévolat, loisirs, contexte familial, milieu scolaire, voyages, etc.). Parfois, elles s'apparentent à des caractéristiques personnelles lorsque vous avez une facilité presque innée dans un domaine. On peut relever l'entregent, l'esprit d'équipe, l'empathie, les habiletés de communication orale et écrite, la capacité d'adaptation, le leadership, le sens des responsabilités, la créativité, etc. Ces compétences sont fort appréciées autant dans les contextes personnels que socioprofessionnels.
	</p>
	<p>
		Ces catégories ne dressent pas un inventaire exhaustif. Elles sont présentées pour vous permettre de développer une conscience plus juste de l'éventail des compétences qui font ou feront votre richesse
	</p>
</section>
<section id="accentuer">
	<h1>Que pouvez-vous faire pour accentuer votre sentiment de compétence ?</h1>
	<h2>On apprend à marcher en marchant : développez vos compétences en les exerçant</h2>
	<p>
		Une compétence s'acquiert et s'exerce toujours à travers une action. Pour ce faire, il faut des défis juste assez difficiles mais réalisables pour augmenter votre confiance en vos capacités. Les exigences universitaires constituent déjà un bon défi. Goûtez vos réussites et prenez conscience de vos progrès. Si vous voulez faire un pas de plus, vous pouvez affronter des situations un peu plus exigeantes (par exemple un emploi d'été axé sur la carrière).
	</p>
	<h2>Soyez partie prenante à vos études</h2>
	<p>
		Certaines personnes arrivent au terme de leur formation universitaire avec le constat d'avoir fait leurs études « en amateurs » et ont le sentiment d'aborder le marché du travail en « imposteurs », leur diplôme leur semblant une coquille vide. Cela ne signifie pas qu'elles n'ont pas développé de compétences, mais le manque d'investissement dans leurs études affecte l'intégration de ces bases et rend la confiance en soi plus fragile. Pour éviter de vous retrouver dans cette position inconfortable, fixez-vous des objectifs personnels face aux apprentissages que vous souhaitez faire ou face aux points faibles que vous voulez améliorer. Mettez en place autant que vous le pouvez les conditions qui vous permettront d'être satisfaits de vos accomplissements. C'est dans la mesure où vous vous investirez dans vos études que vous constaterez après coup combien elles vous ont apporté.
	</p>
	<p>
		Cela dit, certaines personnes peuvent aussi avoir un sentiment d'incompétence pour d'autres raisons. Des tendances perfectionnistes peuvent contribuer au fait de ne pas se sentir adéquat ou adéquate malgré un grand engagement dans ses études. Il y aura alors lieu de réajuster ses attentes avant de penser à en faire encore plus. À ce sujet, vous pouvez consulter le texte sur le <a href="{{LIEN}}perfectionnisme" title="perfectionnisme" target="_blank">perfectionnisme</a>.
	</p>
</section>
<section id="echec">
	<h1>Que faire si vous rencontrez l'échec en cours de route ?</h1>
	<p>
		En situation d'apprentissage, la facilité n'est pas nécessairement au rendez-vous et il est normal d'expérimenter des fluctuations dans l'exercice des habiletés que l'on tente d'acquérir. Le sentiment d'incompétence va nécessairement alterner avec le sentiment de compétence, au fil des variations dans les performances, et ce, jusqu'à ce qu'une certaine aisance et une confiance s'installent.
	</p>
	<p>
		Il faut se rappeler que ces variations font partie de tout nouvel apprentissage et que la somme des efforts investis ne vous en met pas à l'abri. En ce sens, votre façon d'interpréter les difficultés jouera sur votre motivation et sur votre désir de plonger dans l'action à nouveau. Bien sûr, personne n'aime être exposé à l'échec. On peut se heurter à certaines limites personnelles, ce qui peut mettre en évidence des erreurs de jugement ou un manque de préparation. Néanmoins, si vous faites le point et que vous en tirez des leçons, cela devient source de transformation. Afin de ne pas affecter outre mesure le regard que vous portez sur vous-même lorsque vous traversez des difficultés, il y a donc lieu de développer une philosophie à l'égard de la réussite et de l'échec.
	</p>
	<p>
		Les compétences que vous maîtriserez s'intégreront peu à peu à l'image que vous avez de vous-même et contribueront à votre confiance. Elles feront peu à peu partie de votre identité. C'est un processus qui ne se fait pas du jour au lendemain. Profitez de votre séjour universitaire pour vous jauger. Vous aurez ensuite, au cours de votre vie professionnelle, plusieurs occasions de vous améliorer, de pousser plus loin vos performances ou de développer de nouvelles compétences, si vous le désirez.
	</p>
</section>
<section id="references">
	<h1>Références</h1>
	<p><cite>
		François, P.-H. et Botteman, A. E. « Théorie sociale cognitive de Bandura et bilan de compétences : applications, recherche et perspectives critiques », in Carriérologie, vol. 8, no 3, 2002 (en ligne). <a href="http://www.carrierologie.uqam.ca/volume08_3-4/index.html" title="http://www.carrierologie.uqam.ca/volume08_3-4/index.html" target="_blank">http://www.carrierologie.uqam.ca/volume08_3-4/index.html</a> (page consultée le 2 février 2004) 
	</cite></p>
	<p><cite>
		Amigues, R. « Compétences, capacités et savoirs » (en ligne). <s><a href="http://recherche.aix-mrs.iufm.fr/publ/voc/n1/amigues5/" title="http://recherche.aix-mrs.iufm.fr/publ/voc/n1/amigues5/" target="_blank">http://recherche.aix-mrs.iufm.fr/publ/voc/n1/amigues5/</a></s> (page consultée le 2 février 2004) 
	</cite></p>
</section>
<section id="complementaire">
	<h1>Contenu complémentaire</h1>
	<ul>
		<li>
			<a href="{{LIEN}}estime-de-soi" title="L'estime de soi" target="_blank">L'estime de soi</a>
		</li>
		<li>
			<a href="{{LIEN}}reussite-apprentissage" title="Réussite universitaire" target="_blank">Réussite universitaire</a>
		</li>
	</ul>
</section>
<footer>
	<h2>Rédiger par :</h2>
	<p>
		France Lehoux, conseillère d'orientation
	</p>
</footer>
