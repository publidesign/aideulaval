<blockquote>
	Ce que j'aurai perdu en faisant confiance aux autres peut se calculer, mais ce que j'aurai gagné par ce même moyen est inestimable.
	<cite>Gilbert Cesbron</cite>
</blockquote>
<p>
	Il est possible de réussir ses études en se centrant surtout sur les tâches scolaires, mais vous pouvez aussi les enrichir en créant des liens avec les personnes qui vous entourent. Le campus étant vaste, on s'y sent exister davantage à travers des contacts, des amitiés ou des groupes d'appartenance.
</p>
<p>
	Le sentiment d'appartenance est quelque chose qui se construit peu à peu. Le partage d'une même réalité, de valeurs ou d'objectifs communs crée un terrain favorable. Il nécessite en plus, pour se développer, une qualité d'interactions avec les personnes, ce qui contribue au fait qu'on se sente bien et qu'on ait conscience de sa valeur au sein d'un groupe particulier. Vous sentant reconnu ou reconnue, vous avez alors envie de vous engager davantage, de donner le meilleur de vous-même et de vous identifier avec une certaine fierté à ce groupe dont vous fait partie. Si une part de responsabilité relève du milieu qui vous accueille, une autre part dépend de votre attitude et de vos propres efforts d'intégration.
</p>
<nav>
	<ul>
		<li><a href="#sentiment" title="Comment développer ce sentiment ?">Comment développer ce sentiment ?</a></li>
		<li><a href="#resultats" title="Qu'en résultera-t-il pour vous ?">Qu'en résultera-t-il pour vous ?</a></li>
	</ul>
</nav>
<section id="sentiment">
	<h1>Comment développer ce sentiment ?</h1>
	<p>
		À l'université, le sentiment d'appartenance passe généralement par votre identification à votre programme, votre département ou votre faculté. Il y a plusieurs façons de nourrir cette qualité d'interaction avec votre milieu. La liste suivante reflète l'évolution des opportunités à votre disposition, du début à la fin de vos études :
	</p>
	<ul>
		<li>
			participer aux activités d'intégration en première année (initiation, activités sociales ou sportives) pour rencontrer les gens de son programme ;
		</li>
		<li>
			être un membre actif dans les travaux d'équipe ;
		</li>
		<li>
			fréquenter les lieux où des liens informels peuvent se créer, tels le local étudiant ;
		</li>
		<li>
			intégrer des groupes de discussion ; 
		</li>
		<li>
			coopérer aux projets de son association étudiante ;
		</li>
		<li>
			vous impliquer dans l'organisation d'activités étudiantes dans votre faculté (festivals, galas, semaines thématiques, journées carrière) ou y assister ;
		</li>
		<li>
			approcher les professeures et professeurs que vous admirez ou avec qui vous avez des affinités, connaître leurs réalisations et sujets de recherche, discuter avec eux de vos intentions professionnelles ou des possibilités sur le marché du travail ;
		</li>
		<li>
			devenir tuteur ou tutrice, ou encore assistante ou assistant dans les cours ou dans les laboratoires ;
		</li>
		<li>
			travailler pour un professeur ou une professeure ;
		</li>
		<li>
			assister aux conférences et colloques dans votre discipline ;
		</li>
		<li>
			élargir votre appartenance à votre futur groupe professionnel en rencontrant des étudiantes et étudiants plus avancés ou des diplômées et diplômés pour discuter de leurs expériences dans leurs champs d'activités ; en devenant membres-étudiants de l'ordre professionnel s'il y a lieu.
		</li>
	</ul>
	<p>
		Il est possible aussi d'alimenter ce sentiment par le biais d'activités qui rejoignent d'autres intérêts présents chez vous : socioculturels, sportifs, politiques, humanitaires, écologiques, etc.
	</p>
	<p>
		Vous pouvez être réticent ou réticente à l'idée de vous investir dans votre milieu. Les raisons peuvent être multiples : refus de la conformité, timidité, peur du jugement des autres, crainte de revivre des déceptions, sentiment d'être trop différente ou différent, méfiance à l'endroit du milieu d'accueil, etc. Peut-être ne le désirez-vous pas, votre besoin d'appartenance étant comblé hors de l'université (famille, groupes d'amis). Cette question d'investissement demeure un choix personnel.  Néanmoins, si vous êtes prêt ou prête à affronter quelques risques, vous pourrez retirer plusieurs avantages de votre implication, tant au plan personnel que professionnel.
	</p>
</section>
<section id="resultats">
	<h1>Qu'en résultera-t-il pour vous ?</h1>
	<p>
		Comme ce qu'on investit de soi-même porte souvent en soi sa récompense, votre engagement au sein de votre milieu pourra rejaillir sur votre vie présente et future en y apportant :
	</p>
	<ul>
		<li>
			Une meilleure qualité de vie durant votre séjour universitaire.
		</li>
		<li>
			Une plus grande motivation vis-à-vis de vos études. 
		</li>
		<li>
			Une ouverture à l'influence mutuelle et à l'appréciation des différences. Votre insertion dans un milieu n'empêche nullement de demeurer vous-même. Au contraire, c'est en devenant conscient et consciente de vos affinités que vous prendrez aussi davantage conscience de vos différences et que vous apprendrez à les estimer.
		</li>
		<li>
			L'accès à toutes sortes d'informations pertinentes pendant vos études et la création de liens qui peuvent être utiles pour l'avenir (collaborations éventuelles dans des projets, partage mutuel de vos réseaux de contacts, recommandations, etc.). 
		</li>
		<li>
			Le développement progressif de votre identité professionnelle. L'investissement au sein de votre programme vous permettra de connaître les valeurs, les motivations et les idéaux qui y prévalent. Si vous partagez ces valeurs, cela vous permettra d'adhérer aux objectifs communs. Grâce aux modèles que peuvent représenter vos professeurs et professeures ou des diplômés et diplômées de votre domaine, vous vous forgerez une représentation nuancée de votre future profession et vous prendrez conscience avec satisfaction des qualités, attitudes et compétences que vous êtes en train de développer. Vous commencerez à découvrir ce qui lie entre eux les membres de votre groupe professionnel et vous sentirez peu à peu que vous en faites partie.
		</li>
	</ul>
	<p>
		Il demeure que nous avons des appartenances multiples et certaines sont tissées plus serrées que d'autres... Quoi qu'il en soit, les pas que vous ferez vers les autres, les liens que vous créerez, éphémères ou permanents, vous laisseront assurément des souvenirs plus riches de votre passage à l'université. Vous prendrez conscience de vos valeurs. La place que vous occuperez vous permettra d'entrevoir comment vous risquez de vous insérer en contexte professionnel. Finalement, à travers les initiatives que vous prendrez, d'une façon ou d'une autre, vous apprendrez énormément sur vous et vous aurez l'occasion d'évoluer.
	</p>
</section>
<footer>
	<h2>Rédiger par :</h2>
	<p>
		France Lehoux, conseillère d'orientation
	</p>
</footer>
