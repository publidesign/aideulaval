<blockquote>
	Le génie est fait de un pour cent d'inspiration et de quatre-vingt-dix-neuf pour cent de transpiration.
	<cite>Thomas Edison</cite>
</blockquote>
<p>
	Certaines personnes y songent depuis longtemps, d'autres savent qu'il s'agit d'une étape obligatoire pour l'obtention d'un titre professionnel. Pour plusieurs cependant, la question de faire des études supérieures va se poser, en cours de route, au premier cycle. Elle a tout intérêt à se poser tôt, car il y a des exigences : moyenne cumulative de 3 et plus, (2,8 dans certains programmes), date limite plus rapprochée pour l'admission ou pour l'obtention de bourses, etc. On peut envisager les études supérieures pour de bonnes ou mauvaises raisons, tout comme on peut ne pas y penser, aussi pour de bonnes ou mauvaises raisons.
</p>
<nav>
	<ul>
		<li><a href="#motivations" title="Motivations">Motivations</a></li>
		<li><a href="#attentes" title="Attentes">Attentes</a></li>
		<li><a href="#avantages" title="Avantages">Avantages</a></li>
		<li><a href="#references" title="Références">Références</a></li>
		<li><a href="#complementaire" title="Contenu complémentaire">Contenu complémentaire</a></li>
	</ul>
</nav>
<section id="motivations">
	<h1>Motivations</h1>
	<p>
		La première chose à faire est de situer le genre de motivation qui vous pousse aux études supérieures. Si c'est le désir d'apprendre, l'envie de pousser plus loin ou la stimulation intellectuelle à l'idée de résoudre une question qui vous habite, ce sont les motivations les plus favorables à la recherche et à la créativité parce qu'en général, elles amènent un apprentissage significatif et orienté vers la compréhension en profondeur.
	</p>
	<p>
		Vous pouvez aussi vouloir faire des études supérieures pour le salaire, pour faire plaisir à votre entourage ou par peur de décevoir. Vous pouvez chercher à vous réfugier dans les études supérieures par peur du marché du travail ou pour préserver votre statut d'étudiante ou d'étudiant. Ce qui vous motive est alors la crainte d'une conséquence négative ou la recherche d'une récompense sans rapport avec la tâche. L'apprentissage est alors de moindre qualité parce que trop axé sur les résultats et l'obtention du diplôme. Ce genre de motivation peut être source d'une bonne dose de stress.
	</p>
	<p>
		Vous pouvez également désirer vous accomplir, c'est-à-dire relever des défis personnels et vous dépasser dans la maîtrise d'une matière spécifique. Vouloir exceller dans un domaine aide à maintenir la motivation tout en favorisant un apprentissage de qualité, car on tire son plaisir de la satisfaction anticipée d'atteindre ses buts. Cependant, il faut faire attention aux pièges du <a href="{{LIEN}}perfectionnisme" title="perfectionnisme" target="_blank">perfectionnisme</a>.
	</p>
</section>
<section id="attentes">
	<h1>Attentes</h1>
	<p>
		Vérifiez également si vos attentes correspondent bien à la réalité. Le but principal des études supérieures comprend presque toujours cette intention : vouloir vous initier à la recherche. À des degrés divers, bien entendu, car même une maîtrise professionnelle inclut généralement un minimum de recherche. Prenez le temps de consulter les objectifs de ce genre d'études (voir ceux de l'Université Laval). Dans tous les cas, vous devrez apprendre à utiliser vos connaissances pour résoudre des problèmes ouverts. Vous aurez à identifier les lacunes de votre formation et à les combler par vous-même, particulièrement au doctorat. Vous vous poserez des questions et vous apprendrez à reconnaître leurs difficultés. Comme l'a dit Hubert Reeves, astrophysicien réputé, « le principe des études supérieures, c'est qu'on y apprend à apprendre ». Ces études demandent aux étudiantes et étudiants d'avoir un grand sens des responsabilités et une grande autonomie, d'être méthodiques et productifs, d'avoir de bonnes habiletés d'étude et une bonne capacité de rédaction. Vous devez donc adopter une attitude de prise en charge et d'initiative face à vos études.
	</p>
	<p>
		Certaines attitudes peuvent vous jouer des tours. Dans le livre <cite>« How to get a PhD », Philipps et Pugh</cite> en signalent quelques-unes (qui peuvent aussi s'appliquer à la maîtrise). La première est de ne pas vraiment vouloir le diplôme : ce serait bien, mais on n'est pas vraiment prête ou prêt à y mettre l'effort nécessaire. C'est un peu comme devenir millionnaire : beaucoup de gens le souhaitent, mais très peu font ce qu'il faut pour y parvenir.
	</p>
	<p>
		Un autre piège est de surestimer ce qui est demandé. Les études supérieures sont principalement un entraînement à la recherche et, dans ce sens, l'idée qu'il faille apporter une contribution tout à fait originale doit être nuancée. Tant mieux si vous y arrivez, mais en faire un objectif relève davantage d'une stratégie pour l'obtention d'une reconnaissance honorifique que pour l'obtention d'un diplôme. On vous demande surtout de faire de la science « normalement ». Vous pouvez choisir vous-même votre projet ou vous associer à un projet mis en marche par une professeure ou un professeur.
	</p>
	<p>
		À l'inverse, vous pouvez sous-estimer ce qui est demandé pour l'obtention de ces grades, particulièrement si vous occupez déjà un emploi. Non seulement trouver une réponse demande beaucoup d'énergie, mais formuler correctement la question de recherche en demande tout autant, sinon plus. Il faut généralement passer beaucoup de temps pour justifier la pertinence de notre question avant de pouvoir s'attaquer à la réponse. Il est donc faux de penser que vous pourrez faire vos études supérieures en dilettante sous prétexte que votre question est déjà claire dans votre tête.
	</p>
	<p>
		On sous-estime, en général, le temps et l'effort que la phase de rédaction finale demande alors qu'en fait, c'est celle qui exige le plus de concentration de tout le processus. Il est donc risqué pour les candidates et candidats aux études supérieures d'accepter un emploi avant d'avoir terminé, particulièrement à cette étape.
	</p>
</section>
<section id="id_3">
	<h1>Avantages</h1>
	<p>
		Si elles ont leurs exigences, les études supérieures ont aussi leurs avantages. Le plus important est probablement la réalisation d'un projet qui vous appartient. La majorité des étudiantes et étudiants apprécient les études supérieures parce qu'elles permettent de travailler sur un projet personnel. Elles vous offrent la possibilité de satisfaire votre curiosité scientifique et de contribuer à l'évolution de la société. De plus, elles vous apprendront à vous organiser et à vous structurer davantage.
	</p>
	<p>
		Il est vrai aussi, statistiques à l'appui, que le taux de placement s'améliore au fur et à mesure des cycles. De plus, ceux et celles qui détiennent une maîtrise ou un doctorat rapportent de hauts taux de satisfaction au travail, en particulier parce que la relation avec leur formation y est encore plus grande.
	</p>
</section>
<section id="references">
	<h1>Références</h1>
	<p><cite>
		Phillips, E.M. et Pugh, D.S.. How to get a Ph.D.. Open University Press, 1987 
	</cite></p>
</section>
<section id="complementaire">
	<h1>Contenu complémentaire</h1>
	<p>
		Pour plus d'informations sur les études supérieures (types de programme, procédures, aide financière, etc.), consultez ces sites de l'Université Laval:
	</p>
	<ul>
		<li>
			<s><a href="http://www.ulaval.ca/fes" title="Faculté des études supérieures et postdoctorales" target="_blank">Faculté des études supérieures et postdoctorales</a></s>
		</li>
		<li>
			<s><a href="http://www.ulaval.ca/reg/p4_2.html" title="Bureau du registraire" target="_blank">Bureau du registraire</a></s>
		</li>
		<li>
			<s><a href="http://www.bbaf.ulaval.ca/bbaf/entree.htm" title="Bureau des bourses et de l'aide financière" target="_blank">Bureau des bourses et de l'aide financière</a></s>
		</li>
	</ul>
	<p>
		Voici aussi un guide développé par la Faculté des études supérieures et postdoctorales de l'Université Laval :
	</p>
	<ul>
		<li>
			<s><a href="http://www1.fes.ulaval.ca/cheminement" title="Guide de cheminement aux cycles supérieurs" target="_blank">Guide de cheminement aux cycles supérieurs</a></s>
		</li>
	</ul>
</section>
<footer>
	<h2>Rédiger par :</h2>
	<p>
		Daniel Tremblay, conseiller d'orientation
	</p>
</footer>
